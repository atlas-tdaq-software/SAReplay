#!/bin/bash

# scp ../x86_64-slc6-gcc48-opt/sareplay_osiris ../x86_64-slc6-gcc48-opt/sareplay_osiris.debug /afs/cern.ch/atlas/project/tdaq/cmt/tdaq/nightly/installed/share/lib/esperUtils.jar /afs/cern.ch/atlas/project/tdaq/cmt/tdaq/nightly/installed/share/lib/cassandra.jar /afs/cern.ch/atlas/project/tdaq/cmt/tdaq/nightly/installed/share/lib/sareplay.jar atlasgw:

rsync -avz -e ssh ../../installed/x86_64-slc6-gcc48-opt/bin/sareplay_osiris ../../installed/x86_64-slc6-gcc48-opt/bin/sareplay_osiris.debug \
    ../../installed/x86_64-slc6-gcc48-opt/bin/sareplay_downsamplemetrics ../../installed/x86_64-slc6-gcc48-opt/bin/sareplay_downsamplemetrics.debug \
    ../../installed/share/lib/*.jar \
    ../../installed/share/lib/python/osirispy.py \
    ../../installed/x86_64-slc6-gcc48-opt/lib/*.so \
    atlasgw:bin/

