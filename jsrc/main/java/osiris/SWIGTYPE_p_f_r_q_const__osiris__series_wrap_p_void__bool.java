/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package osiris;

public class SWIGTYPE_p_f_r_q_const__osiris__series_wrap_p_void__bool {
  private transient long swigCPtr;

  protected SWIGTYPE_p_f_r_q_const__osiris__series_wrap_p_void__bool(long cPtr, @SuppressWarnings("unused") boolean futureUse) {
    swigCPtr = cPtr;
  }

  protected SWIGTYPE_p_f_r_q_const__osiris__series_wrap_p_void__bool() {
    swigCPtr = 0;
  }

  protected static long getCPtr(SWIGTYPE_p_f_r_q_const__osiris__series_wrap_p_void__bool obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }
}

