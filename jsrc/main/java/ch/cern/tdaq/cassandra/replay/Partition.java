package ch.cern.tdaq.cassandra.replay;

/** Simple Partition class for Querying the Oracle DB.
 * 
 * @author asantos
 *
 */
public class Partition {
	private String name;
	private Long id;

	public Partition(String name, Long id) {
		setName(name);
		setId(id);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return getName() + " (id: " + getId() + ")";
	}
}
