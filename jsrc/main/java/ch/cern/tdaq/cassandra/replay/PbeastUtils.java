package ch.cern.tdaq.cassandra.replay;

import java.math.BigInteger;
import java.util.List;

/** P-BEAST utility class for data types conversion.
 * 
 * @author asantos
 *
 */
public class PbeastUtils {

	public static final int PBEAST_BOOL   = 0;
	public static final int PBEAST_S8     = 1;
	public static final int PBEAST_U8     = 2;
	public static final int PBEAST_S16    = 3;
	public static final int PBEAST_U16    = 4;
	public static final int PBEAST_S32    = 5;
	public static final int PBEAST_U32    = 6;
	public static final int PBEAST_S64    = 7;
	public static final int PBEAST_U64    = 8;
	public static final int PBEAST_FLOAT  = 9;
	public static final int PBEAST_DOUBLE = 10;
	public static final int PBEAST_ENUM   = 11;
	public static final int PBEAST_DATE   = 12;
	public static final int PBEAST_TIME   = 13;
	public static final int PBEAST_STRING = 14;
	public static final int PBEAST_OBJECT = 15;
	public static final int PBEAST_VOID   = 16;

	public static final int OSIRIS_DOUBLE_AS_LONG = 256;
	public static final int OSIRIS_FLOAT_AS_INT = 257;
	
	public static final BigInteger MAX_ULONG_VALUE = BigInteger.ONE.shiftLeft(64).subtract(BigInteger.ONE);
	
	public static Object convertSingle(int pbeastType, String vs) {
		Object v = null;
		
		switch(pbeastType) {
		case PBEAST_BOOL:
			v = Boolean.valueOf(!vs.equals("0"));
			break;
		
		case PBEAST_DOUBLE:
			v = Double.parseDouble(vs);
			break;
			
		case PBEAST_FLOAT:
			v = Float.parseFloat(vs);
			break;
			
		case PBEAST_ENUM:
		case PBEAST_S32:
			v = Integer.parseInt(vs);
			break;
			
		case PBEAST_U32:
			{
				long tmp = Long.parseLong(vs);
				if (tmp > Integer.MAX_VALUE)
					v = (int)(-((tmp ^ 0xffffffff) + 1));
				else
					v = (int)tmp;
			}
			break;
		
		case PBEAST_S16:
			v = Short.parseShort(vs);
			break;
			
		case PBEAST_U16:
			{
				int tmp = Integer.parseInt(vs);
				if (tmp > Short.MAX_VALUE)
					v = (short)(-((tmp ^ 0xffff) + 1));
				else
					v = (short)tmp;
			}
			break;
			
		case PBEAST_S64:
			v = Long.parseLong(vs);
			break;
			
		case PBEAST_U64:
			{
				BigInteger tmp = new BigInteger(vs);
				if (tmp.compareTo(BigInteger.valueOf(Long.MAX_VALUE)) > 0) {
					v = tmp.xor(MAX_ULONG_VALUE).add(BigInteger.ONE).negate().longValue();
				} else {
					v = tmp.longValue();
				}
			}
			break;
			
		case PBEAST_S8:
			v = Byte.parseByte(vs);
			break;
			
		case PBEAST_U8: 
			{
				int tmp = Integer.parseInt(vs);
				if (tmp > Byte.MAX_VALUE)
					v = (byte)(-((tmp ^ 0xff) + 1));
				else
					v = (byte)tmp;
			}
			break;
			
		case PBEAST_STRING:
			v = vs;
			break;
			
		case PBEAST_DATE:
			v = new java.sql.Date(Long.parseLong(vs));
			break;
			
		case PBEAST_TIME:
			v = new java.sql.Time(Long.parseLong(vs));
			break;
			
		case OSIRIS_DOUBLE_AS_LONG:
			v = Double.longBitsToDouble(Long.parseLong(vs));
			break;
			
		case OSIRIS_FLOAT_AS_INT:
			v = Float.intBitsToFloat(Integer.parseInt(vs));
			break;
			
		default:
			// This should not happen... anyway...
			throw new RuntimeException("NOT IMPLEMENTED: PBEAST DATA TYPE " + pbeastType);
		}
		
		return v;
	}

	public static Object convertArray(int pbeastType, List<String> vsarr) {
		Object v = null;
		
		switch(pbeastType) {
		case PBEAST_BOOL:
			// v = (!vs.equals("0"));
			{
				boolean[] q = new boolean[vsarr.size()];
				int i = 0;
				for (String w : vsarr) {
					q[i] = (Boolean)convertSingle(pbeastType, w);
					++i;
				}
				v = q;
			}
			break;
		
		case OSIRIS_DOUBLE_AS_LONG:
		case PBEAST_DOUBLE:
			//v = Double.parseDouble(vs);
			//v = new double[vsarr.size()];
			{
				double[] q = new double[vsarr.size()];
				int i = 0;
				for (String w : vsarr) {
					q[i] = (Double)convertSingle(pbeastType, w);
					++i;
				}
				v = q;
			}
			break;
			
		case OSIRIS_FLOAT_AS_INT:
		case PBEAST_FLOAT:
			//v = Float.parseFloat(vs);
			//v = new float[vsarr.size()];
			{
				float[] q = new float[vsarr.size()];
				int i = 0;
				for (String w : vsarr) {
					q[i] = (Float)convertSingle(pbeastType, w);
					++i;
				}
				v = q;
			}

			break;
			
		case PBEAST_ENUM:
		case PBEAST_S32:
		case PBEAST_U32:
			//v = Integer.parseInt(vs);
			//v = new int[vsarr.size()];
			{
				int[] q = new int[vsarr.size()];
				int i = 0;
				for (String w : vsarr) {
					q[i] = (Integer)convertSingle(pbeastType, w);
					++i;
				}
				v = q;
			}
			break;
			
		case PBEAST_S16:
		case PBEAST_U16:
			//v = Short.parseShort(vs);
			//v = new short[vsarr.size()];
			{
				short[] q = new short[vsarr.size()];
				int i = 0;
				for (String w : vsarr) {
					q[i] = (Short)convertSingle(pbeastType, w);
					++i;
				}
				v = q;
			}
			break;
			
		case PBEAST_S64:
		case PBEAST_U64:
			//v = Long.parseLong(vs);
			//v = new long[vsarr.size()];
			{
				long[] q = new long[vsarr.size()];
				int i = 0;
				for (String w : vsarr) {
					q[i] = (Long)convertSingle(pbeastType, w);
					++i;
				}
				v = q;
			}
			break;
			
		case PBEAST_S8:
		case PBEAST_U8:
			//v = Byte.parseByte(vs);
			//v = new byte[vsarr.size()];
			{
				byte[] q = new byte[vsarr.size()];
				int i = 0;
				for (String w : vsarr) {
					q[i] = (Byte)convertSingle(pbeastType, w);
					++i;
				}
				v = q;
			}
			break;
			
		case PBEAST_STRING:
			//v = vs;
			//v = new String[vsarr.size()];
			{
				String[] q = new String[vsarr.size()];
				int i = 0;
				for (String w : vsarr) {
					q[i] = (String)convertSingle(pbeastType, w);
					++i;
				}
				v = q;
			}
			break;
			
		case PBEAST_DATE:
			//v = new java.sql.Date(Long.parseLong(vs));
			//v = new java.sql.Date[vsarr.size()];
			{
				java.sql.Date[] q = new java.sql.Date[vsarr.size()];
				int i = 0;
				for (String w : vsarr) {
					q[i] = (java.sql.Date)convertSingle(pbeastType, w);
					++i;
				}
				v = q;
			}
			break;
			
		case PBEAST_TIME:
			//v = new java.sql.Time(Long.parseLong(vs));
			//v = new java.sql.Time[vsarr.size()];
			{
				java.sql.Time[] q = new java.sql.Time[vsarr.size()];
				int i = 0;
				for (String w : vsarr) {
					q[i] = (java.sql.Time)convertSingle(pbeastType, w);
					++i;
				}
				v = q;
			}
			break;
			
		default:
			// This should not happen... anyway...
			throw new RuntimeException("NOT IMPLEMENTED: PBEAST DATA TYPE " + pbeastType);
		}
		
		/*
		int i = 0;
		for (String w : vsarr) {
			v[i] = convertSingle(pbeastType, w);
			++i;
		}
		*/
		
		return v;
	}
}
