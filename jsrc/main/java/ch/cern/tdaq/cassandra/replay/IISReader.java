package ch.cern.tdaq.cassandra.replay;

import daq.EsperUtils.ISEvent;
import daq.EsperUtils.TypedObject;

/** Utility interface for reading constructed events.
 * 
 * @author asantos
 *
 */
public interface IISReader {

	public TypedObject getInfoByIndex(String partition, String infoName, int parameterIndex);

	public TypedObject getInfoByName(String partition, String infoName, String paramName);
	
	public ISEvent getEventByName(String partition, String infoName);
}
