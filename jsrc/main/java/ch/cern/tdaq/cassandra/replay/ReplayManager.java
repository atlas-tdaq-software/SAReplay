package ch.cern.tdaq.cassandra.replay;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.espertech.esper.common.client.EventBean;
import com.espertech.esper.runtime.client.UpdateListener;
import com.espertech.esper.runtime.client.EPEventService ;

import ch.cern.tdaq.cassandra.CEPProvider;
import ch.cern.tdaq.cassandra.reader.ConfigReader;
import daq.EsperUtils.ERSEvent;
import daq.EsperUtils.ISEvent;
import daq.EsperUtils.TypedObject;

/**
 * Utility functions to replay ERS and IS messages.
 * 
 * This class also manages the EPL metrics.
 * 
 * @author asantos
 *
 */
public class ReplayManager implements UpdateListener {
	private static final Log log = LogFactory.getLog(ReplayManager.class);
	private static final boolean IsLogDebugEnabled = log.isDebugEnabled();
	
	public static void sort(List<MessageCriteria> list) {
		java.util.Collections.sort(list, new Comparator<MessageCriteria>() {
			@Override
			public int compare(MessageCriteria o1, MessageCriteria o2) {
				return Long.valueOf(o1.getRun().getRunNumber()).compareTo(
						o2.getRun().getRunNumber());
			}
		});
	}

	private static final String SA_REPLAY_ENGINE_METRIC = "SA_REPLAY_ENGINE_METRIC";
	private static final String SA_REPLAY_STATEMENT_METRIC = "SA_REPLAY_STATEMENT_METRIC";

	private final AtomicBoolean replayIsRunning = new AtomicBoolean(false);
	private final AtomicLong replayErsMessageCounter = new AtomicLong(0);
	private final AtomicLong replayIsMessageCounter = new AtomicLong(0);

	// private ISOsirisReader osirisReader = null;
	// private ISOsirisEventTracker osirisTracker = null;
	private List<MessageCriteria> replayCriteriaERS;
	private List<IISOsirisReader> replayCriteriaIS;
	private PriorityQueue<EventSeries> allEvents = new PriorityQueue<EventSeries>();

	private CEPProvider esper = CEPProvider.getInstance() ;
	private EPEventService evservice = CEPProvider.getInstance().getEsperRuntime().getEventService() ;
	private ch.cern.tdaq.cassandra.config.Configuration config = ch.cern.tdaq.cassandra.config.Configuration
			.getInstance();

	private long lastEventTime = -1;
	private long lastEsperTime = 0;

	private BufferedWriter engineMetricsWriter = null;
	private BufferedWriter statementMetricsWriter = null;

	public ReplayManager() {
		initializeMetrics();
		initializeExternalTiming();
		
		CEPProvider.getInstance().getEsperRuntime().getConfigurationDeepCopy().getCommon().addEventType(ISEvent.class) ;
		CEPProvider.getInstance().getEsperRuntime().getConfigurationDeepCopy().getCommon().addEventType(ERSEvent.class) ;

		ISReader.setInstance(new DummyISReader());
	}

	private void initializeExternalTiming() {
		
		evservice.clockExternal(); 

		// We need to initialize lastEsperTime to the current system time,
		// otherwise metrics wont work.
		// See: https://gist.github.com/alejolp/4f1cac757f8ab0e6354c
		
		// AK: disabled and metrics also. We want to see 'real' historical times of alerts
		// lastEsperTime = System.currentTimeMillis();
		// esper.sendEvent(new CurrentTimeEvent(lastEsperTime));
	}

	private void initializeMetrics() {
		String filename;

		try {
			filename = config.getResultDirectory() + "/metricsEngine.txt";
			File outDir = new File(config.getResultDirectory());

			outDir.mkdirs();

			engineMetricsWriter = new BufferedWriter(new FileWriter(filename,
					false));
			engineMetricsWriter.write(String.format("%s\t%s\t%s\t%s\n",
					"Timestamp", "InputCount", "InputCountDelta",
					"ScheduleDepth"));
			engineMetricsWriter.flush();

			filename = config.getResultDirectory() + "/metricsStatements.txt";
			statementMetricsWriter = new BufferedWriter(new FileWriter(
					filename, false));
			statementMetricsWriter.write(String.format(
					"%s\t%s\t%s\t%s\t%s\t%s\t%s\n", "Timestamp",
					"StatementName", "CpuTime", "WallTime", "NumInput",
					"NumOutputIStream", "NumOutputRStream"));
			statementMetricsWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
			log.error(e);
			throw new RuntimeException(e);
		}

		UpdateListener statsListener = this;

		// esper.registerStatement("select * from com.espertech.esper.client.metric.EngineMetric",
		// SA_REPLAY_ENGINE_METRIC);
		// esper.registerStatement("select * from com.espertech.esper.client.metric.StatementMetric",
		// SA_REPLAY_STATEMENT_METRIC);
		// esper.getEpAdministrator().getStatement(SA_REPLAY_ENGINE_METRIC).addListener(statsListener);
		// esper.getEpAdministrator().getStatement(SA_REPLAY_STATEMENT_METRIC).addListener(statsListener);

		// AK May2019: no need metrics at the moment

/*		esper.getEpAdministrator()
				.createEPL(
						"select * from com.espertech.esper.client.metric.EngineMetric",
						SA_REPLAY_ENGINE_METRIC).addListener(statsListener);
		esper.getEpAdministrator()
				.createEPL(
						"select * from com.espertech.esper.client.metric.StatementMetric",
						SA_REPLAY_STATEMENT_METRIC).addListener(statsListener);
*/
	}

	@Override
	public void update(EventBean[] arg0, EventBean[] arg1,
	com.espertech.esper.runtime.client.EPStatement stmt, com.espertech.esper.runtime.client.EPRuntime rt) { } 

	
	public void _update(EventBean[] arg0, EventBean[] arg1) {
		/*
		 * For the metrics
		 */

		if (arg0 != null && arg0.length > 0) {

			for (int i = 0; i < arg0.length; ++i) {
				EventBean ev = arg0[i];

				if (ev.getUnderlying() instanceof com.espertech.esper.common.client.metric.RuntimeMetric) {
					com.espertech.esper.common.client.metric.RuntimeMetric em = (com.espertech.esper.common.client.metric.RuntimeMetric) ev
							.getUnderlying();

					try {
						engineMetricsWriter.write(String.format(
								"%d\t%d\t%d\t%d\n", em.getTimestamp(),
								em.getInputCount(), em.getInputCountDelta(),
								em.getScheduleDepth()));
						engineMetricsWriter.flush();
					} catch (IOException e) {
						e.printStackTrace();
						log.error(e);
						throw new RuntimeException(e);
					}
				} else if (ev.getUnderlying() instanceof com.espertech.esper.common.client.metric.StatementMetric) {
					com.espertech.esper.common.client.metric.StatementMetric sm = (com.espertech.esper.common.client.metric.StatementMetric) ev
							.getUnderlying();

					try {
						statementMetricsWriter.write(String.format(
								"%d\t%s\t%d\t%d\t%d\t%d\t%d\n",
								sm.getTimestamp(), sm.getStatementName(),
								sm.getCpuTime(), sm.getWallTime(),
								sm.getNumInput(), sm.getNumOutputIStream(),
								sm.getNumOutputRStream()));
						statementMetricsWriter.flush();
					} catch (IOException e) {
						e.printStackTrace();
						log.error(e);
						throw new RuntimeException(e);
					}
				} else {
					throw new RuntimeException("Unknown metrics event: "
							+ ev.getEventType().getName() + ", "
							+ ev.getUnderlying());
				}
			}
		}

		if (arg1 != null && arg1.length > 0) {
			log.info("arg1" + arg1);
		}
	}

	public void close() {
		log.info("Closing replay metrics") ;
		try {
			engineMetricsWriter.close();
			engineMetricsWriter = null;
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			statementMetricsWriter.close();
			statementMetricsWriter = null;
		} catch (IOException e) {
			e.printStackTrace();
		}
		log.info("Done with metrics") ;
	}

	private void sendExternalTimingEvent(Object event) {
		if (event instanceof ERSEvent || event instanceof ISEvent) {
			final long currentEventTime;

			if (event instanceof ERSEvent) {
				final ERSEvent ersEvent = (ERSEvent) event;
				currentEventTime = EventSeriesERS.getTimingAsMusec(ersEvent) / 1000;
			} else if (event instanceof ISEvent) {
				final ISEvent isEvent = (ISEvent) event;
				currentEventTime = EventSeriesIS.getTimingAsMusec(isEvent) / 1000;
			} else {
				throw new RuntimeException("Unknown event type");
			}

			if ( lastEsperTime == 0 ) // first event
				{ 	lastEsperTime = currentEventTime ; 
					evservice.advanceTime(currentEventTime) ;
					LocalDateTime date = LocalDateTime.ofEpochSecond(currentEventTime/1000, 0, ZoneOffset.UTC) ;
					log.info("First timing event sent: " + currentEventTime + " (ms): " + date) ;
				}
			else
				{	evservice.advanceTimeSpan(currentEventTime) ; } // advance time to the next event

			return ;
		} 
	}

	public static String toString(ISEvent ev) {
		StringBuilder attrsStr = new StringBuilder();
		
		for (String k : ev.getAttributes().keySet()) {
			TypedObject e = ev.getAttributes().get(k);
			attrsStr.append(k);
			attrsStr.append("=");
			attrsStr.append("(" + ((e.getValue() != null) ? e.getValue().getClass().getSimpleName() : "null") + ")");
			attrsStr.append(e);
			attrsStr.append(", \n");
		}
		
		return "ISEvent ["
				+ "\t- operation=" + ev.getOperation() + ", \n"
				+ "\t- partition=" + ev.getPartitionName() + ", \n"
				+ "\t- server=" + ev.getServer() + ", \n"
				+ "\t- name=" + ev.getName() + ", \n"
				+ "\t- type=" + ev.getType() + ", \n"
				+ "\t- is_time=" + ev.getTimeMicro() + ", \n"
				+ "\t- creation_time=" + ev.getTimeCreation() + ", \n"
				+ "\t- attributes=" + attrsStr + "\n"
				+ "]"; 
	}

	private void sendEventBlocking(Object event) {
//		if (false && ((replayIsMessageCounter.get() + replayErsMessageCounter
//				.get()) % 60000) == 0) {
//			log.info(event);
//		}
	    
		if (IsLogDebugEnabled) {
			if (event instanceof ISEvent) {
				log.debug(toString((ISEvent)event));				
			} else if (event instanceof ERSEvent) {
				log.debug(((ERSEvent)event));
			} else {
				log.debug(event);
			}
		}
		if (true) {
			try {
				if (event instanceof ISEvent) {
					esper.sendEvent(event, "ISEvent");
				}
				else if (event instanceof ERSEvent) {
					esper.sendEvent(event, "Message");
				} else {
					throw new RuntimeException("Wrong type of event: " + event.getClass().getName());
				}
			} catch (Throwable e) {
				if (event instanceof ISEvent) {
					log.error("Throwable with ISEvent: " + toString((ISEvent) event), e);
				} else if (event instanceof ERSEvent) {
					log.error("Throwable with ERSEvent: " + ((ERSEvent) event).toString(), e);
				} else {
					log.error("Throwable with event: " + e.toString(), e);
				}
				// throw new RuntimeException("sendEventBlocking", e);
			}
			sendExternalTimingEvent(event);
		}
	}

	public void configure() {

		sort(replayCriteriaERS);

		ERSLogService logService = ERSLogService.getNewInstance(); // RuntimeException

		HashSet<String> configuredPartitions = new HashSet<String>();

		final String dbRepositoryEnv = System.getenv("TDAQ_DB_REPOSITORY");
		final String dbPathEnv = System.getenv("TDAQ_DB_PATH");
		log.debug("env TDAQ_DB_REPOSITORY: " + dbRepositoryEnv);
		log.debug("env TDAQ_DB_PATH: " + dbPathEnv);

		if(dbRepositoryEnv == null) {
			throw new RuntimeException("Missing TDAQ_DB_REPOSITORY for loading run configuration!");
		}
		// if(dbPathEnv == null) {
		// 	throw new RuntimeException("Missing TDAQ_DB_PATH for loading SA configuration!");
		// }

		try {
			// select all the ERS events and store in memory.
			for (MessageCriteria w : replayCriteriaERS) {
				try {
					log.info("Selecting ERS messages on Partition: "
							+ w.getPartition() + " User=" + w.getUserName()
							+ " Run=" + w.getRun() + " Timeframe start="
							+ GMTDateUtils.formatISOGMT(w.getBeginTimeDate())
							+ ", end="
							+ GMTDateUtils.formatISOGMT(w.getEndTimeDate()));
					final Queue<ERSEvent> events = new ArrayDeque<ERSEvent>();

					logService.SelectMessages(w,
							new ResultSetCallback<ERSMessage>() {
								@Override
								public void resultSetCallback(ERSMessage item) {
									List<ERSEvent> e = ERSAdapter
											.convertERSMessageToEvent(item);
									events.addAll(e);
									replayErsMessageCounter.addAndGet(e.size());
								}
							});

					String configDataFile = "combined/partitions/ATLAS.data.xml";
					String runTag = "tag:r" + w.getRun().getRunNumber() + "@ATLAS";
					config.Configuration db = null;
					try {
						log.info("Loading Configuration from repository " + dbRepositoryEnv
							+ " configData" + configDataFile + " tag: " + runTag); ;
						db = new config.Configuration("oksconfig:" + configDataFile + "&version=" + runTag);

						// Inject just the first partition. Different runs may
						// have different configurations

						if (!configuredPartitions.contains(w.getPartition().getName())) {
							configuredPartitions.add(w.getPartition().getName());
							ConfigReader.injectStaticConfiguration(w.getPartition().getName(), db);
							log.info("Configuration for partition " + w.getPartition().getName() + " loaded in Esper") ;
						}
					} catch (config.ConfigException e) {
						log.error(e);
						throw new RuntimeException(e) ;
					} 

					if ( !events.isEmpty() ) {
						allEvents.add(new EventSeriesERS(events, configDataFile, w, db));
					}
				} finally {
					log.info("Done Selecting ERS messages on partition: "
							+ w.getPartition() + " USER: " + w.getUserName()
							+ " RUN: " + w.getRun());
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			log.error("Failed to get data from SQL: ", e);
			throw new RuntimeException(e) ;
		} finally {
			try {
				logService.getConnection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void replay() {

		// This feels like a syncronized should be used... yet we wont get an
		// exception.
		if (!replayIsRunning.compareAndSet(false, true)) {
			throw new RuntimeException("Already running, check your threads");
		}

		log.info("REPLAY started") ;

		// select all the IS readers
		for (IISOsirisReader isr : replayCriteriaIS) {
			ISOsirisEventTracker t = new ISOsirisEventTracker(isr);

			if (!isr.isStarted()) {
				throw new RuntimeException(
						"The IS stream from OSIRIS has not been started.");
			} else if (t.peekNextISEvent() != null) {
				allEvents.add(new EventSeriesIS(t));
			} else {
				log.error("The IS stream from OSIRIS is empty: "
						+ isr.toString());
			}
		}

		Map<String, Object> heldBackISObjects = new HashMap<String, Object>();
		// List<Object> heldBackERSObjects = new LinkedList<Object>();
		long lastObjectTime = -1;

		log.info("REPLAY continued") ;

		// Replay events to the engine.
		while (allEvents.size() > 0) {
			EventSeries e = allEvents.remove();

			if (e.isEmpty()) {
				log.info("EventSeries empty!") ;
				continue;
			}

			e.hit();
			Object nextObject = e.getNextEvent();
			long nextObjectTime = -1;
			
			// We keep the objects of the same timestamp in the map until we receive an event of
			// a greater timestamp.
			// IF the object type is ERS: empty the map and send the current event.
			// IF the object type is IS: IF timestamp == last: keep current, else: empty the map, keep the current.
			
			if (nextObject instanceof ISEvent) {
				ISEvent nextISObject = (ISEvent) nextObject;				
				nextObjectTime = EventSeriesIS.getTimingAsMusec(nextISObject);
			} else if (nextObject instanceof ERSEvent) {
				ERSEvent nextERSEvent = (ERSEvent) nextObject;
				nextObjectTime = EventSeriesERS.getTimingAsMusec(nextERSEvent);
			} else {
				throw new RuntimeException("Unknown object type");
			}
			
//			log.info("Next object time: " + nextObjectTime) ;
//			log.info("Last object time: " + lastObjectTime) ;

			if (nextObjectTime != lastObjectTime) {
				for (Object w : heldBackISObjects.values()) {
					sendEventBlocking(w);
				}
				heldBackISObjects.clear();
			}

			if (nextObject instanceof ISEvent) {
				ISEvent nextISObject = (ISEvent) nextObject;
				String key = nextISObject.getPartitionName() + "\t" + nextISObject.getName();
				heldBackISObjects.put(key, nextObject);
			} else if (nextObject instanceof ERSEvent) {
				sendEventBlocking(nextObject);
			}  
			lastObjectTime = nextObjectTime;
			
			e.removeNextEvent();

			replayIsMessageCounter.incrementAndGet();

			if (!e.isEmpty()) {
				allEvents.add(e);
			}
		}

		for (Object w : heldBackISObjects.values()) {
			sendEventBlocking(w);
		}
		heldBackISObjects.clear();

		replayIsRunning.set(false);
		log.info("REPLAY FINISHED - ERS MSG COUNTER: "
				+ replayErsMessageCounter.get()
				+ " IS MSG COUNTER: "
				+ (replayIsMessageCounter.get() - replayErsMessageCounter.get()));
		
		close();
		log.info("Exiting replay()");
	}

	public Object[][] selectRunStats(MessageCriteria crit) {
		ERSLogService ls = null;
		try {
			ls = ERSLogService.getNewInstance();
			RunStats result = ls.SelectRunStats(crit);
			Map<String, String> resultMap = new HashMap<String, String>();

			resultMap.put("Run number",
					Long.toString(crit.getRun().getRunNumber()));
			resultMap.put("Message count",
					Long.toString(result.getMessageCount()));
			resultMap.put("Begin time", Long.toString(result.getBeginTime()));
			resultMap.put("End time", Long.toString(result.getEndTime()));
			resultMap.put("Begin time (str)", result.getBeginTimeStr());
			resultMap.put("End time (str)", result.getEndTimeStr());

			final Object[][] data = new Object[resultMap.size()][];
			int i = 0;
			for (Entry<String, String> w : resultMap.entrySet()) {
				data[i] = new String[] { w.getKey(), w.getValue() };
				i++;
			}
			Arrays.sort(data, new Comparator<Object[]>() {
				@Override
				public int compare(Object[] o1, Object[] o2) {
					return ((String) o1[0]).compareTo((String) o2[0]);
				}
			});
			return data;
		} catch (SQLException e) {
			e.printStackTrace();
			log.error("", e);
		} finally {
			try {
				ls.getConnection().close();
			} catch (SQLException e) {
				e.printStackTrace();
				log.error("", e);
			}
		}
		return null;
	}

	public Long[] selectMinMax(List<MessageCriteria> runs) {
		long beginTime = Long.MAX_VALUE, endTime = Long.MIN_VALUE;
		ERSLogService ls = null;

		try {
			ls = ERSLogService.getNewInstance();

			for (MessageCriteria w : runs) {
				RunStats stats = ls.SelectRunStats(w.getPartition(),
						w.getUserName(), w.getRun());
				beginTime = Math.min(beginTime, stats.getBeginTime());
				endTime = Math.max(endTime, stats.getEndTime());
			}

			return new Long[] { beginTime, endTime };
		} catch (SQLException e) {
			e.printStackTrace();
			log.error("", e);
		} finally {
			try {
				ls.getConnection().close();
			} catch (SQLException e) {
				e.printStackTrace();
				log.error("", e);
			}
		}
		return null;
	}

	public void setCriteriaERS(List<MessageCriteria> replayCriteriaERS) {
		this.replayCriteriaERS = replayCriteriaERS;
	}

	public void setCriteriaIS(List<IISOsirisReader> replayCriteriaIS) {
		this.replayCriteriaIS = replayCriteriaIS;
	}
}
