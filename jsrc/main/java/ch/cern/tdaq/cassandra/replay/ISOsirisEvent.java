package ch.cern.tdaq.cassandra.replay;

import java.util.List;

/** One row on the P-BEAST OSIRIS tool.
 * 
 * @author asantos
 *
 */
public class ISOsirisEvent {
	
	/* 
	 * std::cout << sp->get_current_created() 
	 * 		<< "\t" << sp->get_current_last_updated() 
	 * 		<< "\t" << sw.part_name 
	 * 		<< "\t" << sw.class_name 
	 * 		<< "\t" << sw.attr_name 
	 * 		<< "\t" << sw.obj_name 
	 * 		<< "\t" << sp->get_current_value_str() << std::endl;
	 */
	
	private long timeStampCreated;
	private long timeStampLastUpdated;
	private String partitionName;
	private String className;
	private List<String> attributeName;
	private List<String> objectName;
	private String rawValue;

	public ISOsirisEvent(long timeStampCreated, long timeStampLastUpdated,
			String partitionName, String className, List<String> attributeName,
			String serverName, List<String> objectName, String rawValue) {
		super();
		this.timeStampCreated = timeStampCreated;
		this.timeStampLastUpdated = timeStampLastUpdated;
		this.partitionName = partitionName;
		this.className = className;
		this.attributeName = attributeName;
		this.objectName = objectName;
		this.rawValue = rawValue;
	}

	public long getTimeStampCreated() {
		return timeStampCreated;
	}

	public void setTimeStampCreated(long timeStampCreated) {
		this.timeStampCreated = timeStampCreated;
	}

	public long getTimeStampLastUpdated() {
		return timeStampLastUpdated;
	}

	public void setTimeStampLastUpdated(long timeStampLastUpdated) {
		this.timeStampLastUpdated = timeStampLastUpdated;
	}

	public String getPartitionName() {
		return partitionName;
	}

	public void setPartitionName(String partitionName) {
		this.partitionName = partitionName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public List<String> getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(List<String> attributeName) {
		this.attributeName = attributeName;
	}

	public List<String> getObjectName() {
		return objectName;
	}

	public void setObjectName(List<String> objectName) {
		this.objectName = objectName;
	}

	public String getRawValue() {
		return rawValue;
	}

	public void setRawValue(String rawValue) {
		this.rawValue = rawValue;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ISOsirisEvent [timeStampCreated=");
		builder.append(timeStampCreated);
		builder.append(", timeStampLastUpdated=");
		builder.append(timeStampLastUpdated);
		builder.append(", partitionName=");
		builder.append(partitionName);
		builder.append(", className=");
		builder.append(className);
		builder.append(", attributeName=");
		builder.append(attributeName);
		builder.append(", objectName=");
		builder.append(objectName);
		builder.append(", rawValue=");
		builder.append(rawValue);
		builder.append("]");
		return builder.toString();
	}

	
}
