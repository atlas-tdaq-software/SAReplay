package ch.cern.tdaq.cassandra.replay;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


/** Utility functions to format GMT dates and times.
 * 
 * @author asantos
 *
 */
public class GMTDateUtils {
	private static DateFormat df = new SimpleDateFormat("dd MMM yyyy HH:mm:ss z");
	private static DateFormat gmtdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss z");
	private static DateFormat gmtisodf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
	
	static {
		gmtdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		gmtisodf.setTimeZone(TimeZone.getTimeZone("GMT"));
	}
	
	public static String format(Long epoch_time) {
		return df.format(new Date(epoch_time));
	}

	public static String formatGMT(Long epoch_time) {
		return gmtdf.format(new Date(epoch_time));
	}

	public static String formatGMT(Date d) {
		return gmtdf.format(d);
	}

	/** ISO 8601: "YYYY-MM-dd HH:mm:ss"
	 * 
	 * @param d
	 * @return
	 */
	public static String formatISOGMT(Date d) {
		return gmtisodf.format(d);
	}

	public static String formatISOGMT(Long epoch_time) {
		return gmtisodf.format(new Date(epoch_time));
	}
	
}
