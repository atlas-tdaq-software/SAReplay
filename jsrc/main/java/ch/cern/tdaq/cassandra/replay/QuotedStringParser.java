package ch.cern.tdaq.cassandra.replay;

import java.util.ArrayList;
import java.util.List;

/** Simple unquoter for strings. Test cases available in the {@link StringQuoteTest} class.
 * 
 * @author asantos
 *
 */
public class QuotedStringParser {

	/** Unquote the basic quote encoding on OSIRIS arrays.
	 * 
	 * See quote_str function at osiris_api.cpp.
	 * 
	 * @param vs
	 * @return
	 */
	public static List<String> unquoteAsList(String vs) {
		if (vs.length() < 2 || vs.charAt(0) != '[' || vs.charAt(vs.length()-1) != ']')
			throw new RuntimeException("unquoteAsList: Invalid format: " + vs);
		int i = 1, start = -1;
		boolean inside = false, comma = true;
		List<String> result = new ArrayList<String>();
		StringBuilder sb = null;
		
		for (; i < vs.length() - 1; ++i) {
			if (inside) {
				switch (vs.charAt(i)) {
				case '\\':
					switch (vs.charAt(i+1)) {
					case '\\':
					case '"':
						sb.append(vs.charAt(i+1));
						i++;
						break;

					case 'n':
						sb.append('\n');
						i++;
						break;
						
					case 't':
						sb.append('\t');
						i++;
						break;
						
					default:
						throw new RuntimeException(
								String.format("unquoteAsList: Invalid char '%c' ($02x) at %d in string %s", 
									vs.charAt(i), (int)vs.charAt(i), i, vs));
					}
					break;
					
				case '"':
					result.add(sb.toString());
					inside = false;
					comma =  false;
					start = -1;
					sb = null;
					break;
					
				default:
					sb.append(vs.charAt(i));
					break;
				}
			} else {
				switch (vs.charAt(i)) {
				case '"':
					if (!comma) {
						throw new RuntimeException(
								String.format("unquoteAsList: Missing comma, Invalid char '%c' ($02x) at %d", 
									vs.charAt(i), (int)vs.charAt(i), i));
					}
					
					inside = true;
					start = i + 1;
					sb = new StringBuilder();
					break;
					
				case ',':
					if (comma) {
						throw new RuntimeException(
								String.format("unquoteAsList: Invalid char '%c' ($02x) at %d in string %s", 
									vs.charAt(i), (int)vs.charAt(i), i, vs));
					}
					comma = true;
					
					/* ignore */
				case ' ':
					break;
					
				default:
					throw new RuntimeException(
						String.format("unquoteAsList: Invalid char '%c' ($02x) at %d in string %s", 
							vs.charAt(i), i, vs));
				}
			}
		}
		
		if (sb != null) {
			throw new RuntimeException("unquoteAsList: Invalid format: " + vs);
		}
		
		return result;
	}
}
