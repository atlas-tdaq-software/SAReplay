package ch.cern.tdaq.cassandra.replay;

import java.util.ArrayList;
import java.util.List;

import daq.EsperUtils.ERSEvent;

/** Converts an ERSMessage from the Oracle Store DB to a Cassandra ERSEvent. 
 * 
 * @author asantos
 *
 */
public class ERSAdapter {

	private static final String[] severityValues = new String[]{ "Debug",  "Log", "Information", "Warning", "Error", "Fatal", "Undefined"};
	
	/** Given a {@link ERSMessage} returns a new {@link ERSEvent} without chaining the messages.
	 * 
	 * @return
	 */
	private static ERSEvent convertERSMessageToEvent_NoChained(final ERSMessage m) {
		ERSEvent result = new ERSEvent();
		
		String severity = m.getSeverity();
		
		if (severity != null) {
			try {
				int idx = Integer.parseInt(severity);
				if (idx >= 0 && idx < severityValues.length) {
					severity = severityValues[idx].toUpperCase();
				}
			} catch (NumberFormatException e) {
				
			}
		}
		
		result.setMessageID(m.getMessageId());
		result.setSeverity(severity);
		result.setMachineName(m.getMachineName());
		result.setApplicationName(m.getApplicationName());
		result.setMessageTxt(m.getMsgText());
		result.setPartitionName(m.getPartitionName());
		result.setFormattedDate(m.getIssuedWhenFormatted());
		result.setIssuedDate(m.getIssuedWhen());
		//result.setParameters(m.getParameters()) ;
		result.setParameters(m.getOptionalParameters());
		//result.setOptionalParameters(m.getOptionalParameters());
		result.setQualifiers(m.getQualifiers());
			
		List<ERSMessage> chainedMsgList = m.getChainedMsgs();		
		result.setChained(chainedMsgList.size() > 0);
		
		/* 
		 * The last one is chained to itself.
		 */
		result.setChainedMessage(result);

		// FIXME: 3 fields remain to be filled!

		//result.setNumberOfMessages(numberOfMessages);
		//result.setOptionalQualifiers(e);
		//result.setUserName(user_name);

		return result;
	}
	
	/**  
	 * 
	 * Given a {@link ERSMessage} returns a list of all the {@link ERSEvent} chained to 
	 * the original {@link ERSMessage} including this one as the first element of the list.
	 * 
	 * @param m
	 * @return
	 */
	public static List<ERSEvent> convertERSMessageToEvent(final ERSMessage m) {
		ERSEvent first = convertERSMessageToEvent_NoChained(m);
		List<ERSMessage> chainedMsgList = m.getChainedMsgs();		
		List<ERSEvent> result = new ArrayList<ERSEvent>();
		
		result.add(first);
		
		if (chainedMsgList.size() > 0) {
			ERSEvent previous = first;
			
			for (ERSMessage current : chainedMsgList) {
				ERSEvent w = convertERSMessageToEvent_NoChained(current);
				result.add(w);
				previous.setChainedMessage(w);
				previous = w;
			}
		}
		
		return result;
	}

}
