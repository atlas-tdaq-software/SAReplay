package ch.cern.tdaq.cassandra.replay;

/** Oracle DB callback.
 * 
 * @author asantos
 *
 * @param <T>
 */
public interface ResultSetCallback<T> {
	public void resultSetCallback(T item);
}
