package ch.cern.tdaq.cassandra.replay;

/** Criteria class for querying the Oracle DB
 * 
 * @author asantos
 *
 */
public class MessageCriteria {
	private long beginTime;
	private long endTime;
	private Partition partition;
	private String userName;
	private Run run;

	public MessageCriteria(Partition partition, String userName, Run run) {
		super();
		this.beginTime = Long.MIN_VALUE;
		this.endTime = Long.MAX_VALUE;
		this.partition = partition;
		this.userName = userName;
		this.run = run;
	}
	
	public MessageCriteria(long beginTime, long endTime, Partition partition, String userName, Run run) {
		super();
		this.beginTime = beginTime;
		this.endTime = endTime;
		this.partition = partition;
		this.userName = userName;
		this.run = run;
	}

	public long getBeginTime() {
		return beginTime;
	}

	public java.util.Date getBeginTimeDate() {
		return new java.util.Date(beginTime * 1000);
	}

	public void setBeginTime(long beginTime) {
		this.beginTime = beginTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public java.util.Date getEndTimeDate() {
		return new java.util.Date(endTime * 1000);
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public Partition getPartition() {
		return partition;
	}

	public void setPartition(Partition partition) {
		this.partition = partition;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Run getRun() {
		return run;
	}

	public void setRun(Run run) {
		this.run = run;
	}

	@Override
	public String toString() {
		return "MessageCriteria [beginTime=" + beginTime + ", endTime=" + endTime + ", partition=" + partition + ", userName=" + userName + ", run=" + run + "]";
	}
}
