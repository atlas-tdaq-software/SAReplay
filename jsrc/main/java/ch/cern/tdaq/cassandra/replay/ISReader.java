package ch.cern.tdaq.cassandra.replay;

import daq.EsperUtils.ISEvent;
import daq.EsperUtils.TypedObject;

/** Replacement for the AAL ISReader class.
 * 
 * @author asantos
 *
 */
public class ISReader {
	private static IISReader instance = new DummyISReader();
	
	public static void setInstance(IISReader x) {
		instance = x;
	}
	
	public static TypedObject getInfoByIndex(String partition, String infoName, int parameterIndex) {
		return instance.getInfoByIndex(partition, infoName, parameterIndex);
	}

	public static TypedObject getInfoByName(String partition, String infoName, String paramName) {
		return instance.getInfoByName(partition, infoName, paramName);
	}
	
	public static ISEvent getEventByName(String partition, String infoName) {
		return instance.getEventByName(partition, infoName);
	}
}
