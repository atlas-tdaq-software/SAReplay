package ch.cern.tdaq.cassandra.replay;

import daq.EsperUtils.ISEvent;
import daq.EsperUtils.TypedObject;

public class DummyISReader implements IISReader {

	@Override
	public TypedObject getInfoByIndex(String partition, String infoName, int parameterIndex) {
		return new TypedObject(null);
	}

	@Override
	public TypedObject getInfoByName(String partition, String infoName, String paramName) {
		return new TypedObject(null);
	}

	@Override
	public ISEvent getEventByName(String partition, String infoName) {
		return null;
	}
}
