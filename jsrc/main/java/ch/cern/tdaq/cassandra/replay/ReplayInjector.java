package ch.cern.tdaq.cassandra.replay;

import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.util.Locale;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ipc.servant;
import ch.cern.tdaq.cassandra.CEPProvider;
import ch.cern.tdaq.cassandra.config.Configuration;
import ch.cern.tdaq.cassandra.injector.Injector;


/** Replay Injector. 
 * 
 * Objects of this class are instantiated by calling Class.forName in the AALEngine class.
 * 
 * @author asantos
 *
 */
public class ReplayInjector implements Injector, Runnable {
	
	public static class TimePair implements Comparable<TimePair> {
		public TimePair(java.util.Date start, java.util.Date end) {
			this.start = start;
			this.end = end;
		}
		
		public java.util.Date start, end;
		
		@Override
		public int compareTo(TimePair o) {
			return start.compareTo(o.start);
		}
	}
	
	private static final Log log = LogFactory.getLog(ReplayInjector.class);

	private AtomicBoolean started = new AtomicBoolean(false);
	private AtomicBoolean stopping = new AtomicBoolean(false);
	private CEPProvider esper = CEPProvider.getInstance();
	
	private ReplayManager replayMgr;
	private List<MessageCriteria> replayCriteriaERS;
	private List<IISOsirisReader> replayCriteriaIS;
	private AtomicBoolean configured = new AtomicBoolean(false);
	
	private Thread replayThread;
	
	public ReplayInjector() {
		configure();  // throw RuntimeException
	}

	synchronized private void configure() {
		Configuration config = Configuration.getInstance();
		
		if (!config.getReplayMode())
			throw new RuntimeException("Replay mode disabled, will not configure Injector");
		
		Properties props = config.getProperties();
		log.debug("env TDAQ_DB_REPOSITORY: " + System.getenv("TDAQ_DB_REPOSITORY"));
		log.debug("env TDAQ_DB_PATH: " + System.getenv("TDAQ_DB_PATH"));
		// log.debug("env TDAQ_CLASSPATH: " + System.getenv("TDAQ_CLASSPATH"));
		// log.debug("env CLASSPATH: " + System.getenv("CLASSPATH"));
		// log.debug("env LD_LIBRARY_PATH: " + System.getenv("LD_LIBRARY_PATH"));
		log.info("Replay properties: " + props.toString()) ;

		ERSLogService ersLogSvc;
		
			// Initialize some containers.
		try {
			ersLogSvc = new ERSLogService(ERSLogService.getNewConnection());
			replayCriteriaERS = new ArrayList<MessageCriteria>();
			replayCriteriaIS = new ArrayList<IISOsirisReader>();
			ArrayList<TimePair> timeSlots = new ArrayList<TimePair>();
			
			// The shell to run the osiris tool easily.
			String userShell = System.getenv("SHELL");
			
			if (userShell == null) {
				userShell = "/bin/sh";
			}

			// Osiris properties.
			String replayIsPartClass = props.getProperty("replay.osiris.partclass", "");
			String replayIsPartClassAttr = props.getProperty("replay.osiris.partclassattr", "");
			
			// List all the ERS runs, and take each time frame.
			for (String k : props.stringPropertyNames()) {
				final String suffix = ".runnumber";
				if (k.startsWith("replay.run") && k.endsWith(suffix)) {
					String prefix = k.substring(0, k.length() - suffix.length());
					
					long replayRunNumber = Long.parseLong(props.getProperty(prefix + ".runnumber", ""));
					long referenceRunNumber = Long.parseLong(props.getProperty(prefix + ".refrn", "0"));
					
					if (log.isDebugEnabled()) {
						//log.debug("Available releases: " + ersLogSvc.SelectTDAQreleases());
					}
					
					MessageCriteria crit = ersLogSvc.messageCriteriaFactory(replayRunNumber);
					
					/* Use another Run Number as a reference timer for this run */
					if (referenceRunNumber > 0) {
						MessageCriteria refc = ersLogSvc.messageCriteriaFactory(referenceRunNumber);
						crit.setBeginTime(refc.getBeginTime());
						crit.setEndTime(refc.getEndTime());
					}
					
					/* Use a time interval as a reference timer for this run */
					String referenceSince = props.getProperty(prefix + ".since", "");
					String referenceTill  = props.getProperty(prefix + ".till", "");
					if (referenceSince.length() > 0 && referenceTill.length() > 0) {
						//DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

						DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

						crit.setBeginTime(LocalDateTime.parse(referenceSince, formatter).toEpochSecond(ZoneOffset.UTC));
						crit.setEndTime(LocalDateTime.parse(referenceTill, formatter).toEpochSecond(ZoneOffset.UTC));
					}
					
					replayCriteriaERS.add(crit);
					log.info("Added ERS criteria: " + crit);
					timeSlots.add(new TimePair(crit.getBeginTimeDate(), crit.getEndTimeDate()));
				}
			}

			log.info("Added ERS criterias: " + replayCriteriaERS.size()) ;

			// Sort time frames.
			Collections.sort(timeSlots);

			// Find and merge intersections on the time frames.
			for (int k = 1; k < timeSlots.size(); ) {
				if (timeSlots.get(k - 1).end.compareTo(timeSlots.get(k).start) >= 0) {
					timeSlots.get(k - 1).end = timeSlots.get(k).end;
					timeSlots.remove(k);
				} else {
					++k;
				}
			}

			if (replayIsPartClass.trim().length() > 0 || replayIsPartClassAttr.trim().length() > 0) {
				// For each time frame start a new Osiris tool.
				for (TimePair p : timeSlots) {
					//RunStats rstat = ersLogSvc.SelectRunStats(crit.getPartition(), replayUserName, crit.getRun());
					//String beginTimeGMT = GMTDateUtils.formatISOGMT(rstat.getBeginTimeDate());
					//String endTimeGMT = GMTDateUtils.formatISOGMT(rstat.getEndTimeDate());
	
					String beginTimeGMT = GMTDateUtils.formatISOGMT(p.start);
					String endTimeGMT = GMTDateUtils.formatISOGMT(p.end);
					
					log.info("OSIRIS swig method selected");
					ISOsirisSwigReader osirisreader = new ISOsirisSwigReader(beginTimeGMT, endTimeGMT);
					    
					if (replayIsPartClass.length() > 0) {
					    osirisreader.addPartClass(replayIsPartClass);
					}
					    
					if (replayIsPartClassAttr.length() > 0) {
					    osirisreader.addPartClassAttr(replayIsPartClassAttr);
					}
						
					replayCriteriaIS.add(osirisreader);
				}
			} else {
				log.info("OSIRIS IS disabled.");
			}
			
			replayMgr = new ReplayManager();
			
			replayMgr.setCriteriaERS(replayCriteriaERS);
			replayMgr.setCriteriaIS(replayCriteriaIS);
			
			replayMgr.configure();
			log.info("Replay Manager configured");
			
			configured.set(true);
		}
		catch (final Exception ex)
			{
			log.error("Failed to configure ReplayInjector: " + ex) ;
			throw new RuntimeException(ex) ;
			}
	}

	/** Replay method. Do not call this directly, use start().
	 * 
	 */
	@Override
	public void run() {
		try { // give it up to 5 minutes to configure?
			for (int i = 0; i < 300 && !configured.get(); ++i) {
				Thread.sleep(1000);
			}
		} catch (InterruptedException e1) {
		}
		
		if (!configured.get()) {
			throw new RuntimeException("ReplayInjector not configured in 5 minutes, won't start!");
		}
		
		try {
			for (IISOsirisReader isr : replayCriteriaIS) {
				isr.start();
			}
			
			log.info("Starting replay");
			replayMgr.replay();
			log.info("Replay finished");
		} catch (final Exception e) {
			e.printStackTrace();
			log.error("Failed in replay: " + e);
			// throw new RuntimeException(e) ;
		}
		log.info("Replay completed, exiting Replay thread, calling System.exit(0)");
		System.exit(0);
	}
	
	@Override
	synchronized public void start() {
		// Cassandra will never call this method directly. We should call it from checkAndRestart.
		
		if (!started.compareAndSet(false, true)) 
			return;
		
		replayThread = new Thread(this);
		replayThread.start();
		log.info("Replay thread spawned");
	}

	@Override
	public void stop(boolean serverStatus) {
		stopping.set(true);
	}

	@Override
	public void checkAndRestart() {
		// This is the method that actually gets called first. We should call start from here. 
		start();
	}

	@Override
	public boolean isRunning() {
		return true;
	}

	@Override
	public ServerState isServerAlive() {
		return ServerState.ACTIVE;
	}

	@Override
	public servant getIpcObject() {
		throw new RuntimeException("Unimplemented ");
	}
}
