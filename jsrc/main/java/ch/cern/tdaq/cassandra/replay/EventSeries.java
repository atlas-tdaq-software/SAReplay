package ch.cern.tdaq.cassandra.replay;

/**
 * 
 * @author asantos
 *
 */
public abstract class EventSeries implements Comparable<EventSeries> {

	public EventSeries() {
		
	}
	
	@Override
	public int compareTo(EventSeries o) {
		// extra protection from empty collections and NoSuchElement exceptions
		// proper solution: in ReplayManager make sure that no empty EventSeries is added to PriorityQueue
		if ( isEmpty() || o.isEmpty() ) return 0 ;
		return (int)(getNextTimestamp() - o.getNextTimestamp());
	}
	
	public abstract boolean isEmpty();
	
	public abstract long getNextTimestamp();
	
	public abstract Object getNextEvent();
	
	public abstract void removeNextEvent();
	
	public abstract void hit();
}
