package ch.cern.tdaq.cassandra.replay;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.management.RuntimeErrorException;

import daq.coral.DBInfoExtractor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.cern.tdaq.cassandra.config.Configuration;


public class ERSLogService {
	private static final Log log = LogFactory.getLog(ERSLogService.class);

	public static Connection getNewConnection()
            throws InstantiationException,
                   IllegalAccessException,
                   ClassNotFoundException,
                   SQLException {

		DBInfoExtractor dbInfo = null;
        try {
            dbInfo = DBInfoExtractor.createFromLogicalName("LOG_MANAGER");
        } catch (DBInfoExtractor.BadInfoException e) {
			log.error("Failed to resolve LOG_MANAGER logical name for ERS archive query: " + e) ;
            throw new RuntimeException(e) ; // application to resolve this
        }

		Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
		DriverManager.setLoginTimeout(10);
		log.info("Connecting to " + dbInfo.getConnectionString()) ;
		return DriverManager.getConnection(dbInfo.getConnectionString(),
                                           dbInfo.getUser(),
                                           dbInfo.getPassword());
	}

	public static ERSLogService getNewInstance() {
		Connection connection = null;
		try {
			connection = getNewConnection();
		} catch (Exception e) {
			throw new RuntimeException(e) ;
		}
				
		return new ERSLogService(connection);
	}
	
	private Connection connection;

	public ERSLogService(Connection connection) {
		this.connection = connection;
	}

	public Connection getConnection() {
		return connection;
	}
	
	public List<String> SelectTDAQreleases() throws SQLException {
		return SelectTDAQreleases(true);
	}
	
	public List<String> SelectTDAQreleases(boolean orderByAsc) throws SQLException {
		String sqlQuery = "SELECT DISTINCT(TDAQ_RELEASE) FROM ATLAS_LOG_MESSG.PARTITION_IDS ORDER BY TDAQ_RELEASE " + (orderByAsc ? " ASC " : " DESC ");
		ResultSet rs = null;
		Statement stmt = null;
		ArrayList<String> result = new ArrayList<String>();

		try {
			// Create the statement and execute the query
			stmt = connection.createStatement(
					ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				result.add(rs.getString("TDAQ_RELEASE"));
				log.info("Selected TDAQ_RELEASE: " + rs.getString("TDAQ_RELEASE")) ;
			}
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		}
		return result;
	}

	public List<Partition> SelectTDAQPartition(String tdaqRelease)
			throws SQLException {
		String sqlQuery = "SELECT * FROM ATLAS_LOG_MESSG.PARTITION_IDS WHERE TDAQ_RELEASE=? ORDER BY PARTITION_NAME ASC";
		ResultSet rs = null;
		PreparedStatement stmt = null;
		ArrayList<Partition> result = new ArrayList<Partition>();

		try {
			// Create the statement and execute the query
			stmt = connection.prepareStatement(sqlQuery,
					ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			stmt.setString(1, tdaqRelease);
			rs = stmt.executeQuery();

			while (rs.next()) {
				result.add(new Partition(rs.getString("PARTITION_NAME"), rs
						.getLong("PARTITION_ID")));				
				log.info("Selected PARTITION_NAME: " + rs.getString("PARTITION_NAME")) ;
			}
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		}
		return result;
	}

	public List<String> SelectTDAQPartitionUsers(long partitionId,
			long startTime) throws SQLException {
		String sqlQuery = "SELECT DISTINCT(user_name) FROM ATLAS_LOG_MESSG.METADATA WHERE partition_id = ?"
				+ " AND TIMESTAMP > ? ORDER BY user_name";
		ResultSet rs = null;
		PreparedStatement stmt = null;
		ArrayList<String> result = new ArrayList<String>();

		try {
			// Create the statement and execute the query
			stmt = connection.prepareStatement(sqlQuery,
					ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			stmt.setLong(1, partitionId);
			stmt.setLong(2, startTime);
			rs = stmt.executeQuery();

			while (rs.next()) {
				result.add(rs.getString("user_name"));
				log.info("Selected user_name: " + rs.getString("user_name")) ;
			}
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		}
		return result;
	}

	public List<Run> SelectRuns(long partitionId, String user, long startTime)
			throws SQLException {
		String sqlQuery = "SELECT DISTINCT(RUN_NUMBER), TIMESTAMP, PARTITION_ID FROM ATLAS_LOG_MESSG.METADATA "
				+ "WHERE partition_id = ? AND user_name = ? AND TIMESTAMP > ? ORDER BY RUN_NUMBER ASC";
		ResultSet rs = null;
		PreparedStatement stmt = null;
		ArrayList<Run> result = new ArrayList<Run>();

		try {
			// Create the statement and execute the query
			stmt = connection.prepareStatement(sqlQuery,
					ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			stmt.setLong(1, partitionId);
			stmt.setString(2, user);
			stmt.setLong(3, startTime);
			rs = stmt.executeQuery();

			while (rs.next()) {
				result.add(new Run(rs.getLong("RUN_NUMBER"), rs
						.getLong("TIMESTAMP"), rs.getLong("PARTITION_ID")));
				log.info("Selected RUN_NUMBER: " + rs.getString("RUN_NUMBER")) ;
				log.info("Selected TIMESTAMP: " + rs.getString("TIMESTAMP")) ;
				log.info("Selected PARTITION_ID: " + rs.getString("PARTITION_ID")) ;
			}
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		}
		
		fillConfigData(result);
		
		return result;
	}

	public void fillConfigData(List<Run> result) throws SQLException {
		/* This is OH SO HORRIBLE. I'm sorry, I really am. */
		
		Connection connection_run = null;
		try {
            DBInfoExtractor dbInfo = null;
            try {
                final String runNumberDB = Configuration.getInstance().getProperties().getProperty("replay.run_number_db", "oracle://atonr/rn_r");
                dbInfo = DBInfoExtractor.createFromServiceName(runNumberDB);
            } catch (DBInfoExtractor.BadInfoException e) {
                e.printStackTrace();
                log.error("", e);
            }

			connection_run = DriverManager.getConnection(dbInfo.getConnectionString(),
                                                         dbInfo.getUser(),
                                                         dbInfo.getPassword());
			
			String sqlQuery = "SELECT CONFIGNAME FROM atlas_run_number.runnumber WHERE RUNNUMBER = ?";
			ResultSet rs = null;
			PreparedStatement stmt = null;
			
			for (Run r : result) {
				
				try {
					// Create the statement and execute the query
					stmt = connection_run.prepareStatement(sqlQuery,
							ResultSet.TYPE_FORWARD_ONLY,
							ResultSet.CONCUR_READ_ONLY);
					stmt.setLong(1, r.getRunNumber());
					rs = stmt.executeQuery();
					if (rs.next()) {
						r.setConfigData(rs.getString("CONFIGNAME"));
					log.info("Selected CONFIGNAME: " + rs.getString("CONFIGNAME")) ;
					}
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (stmt != null) {
						stmt.close();
					}
				}
			}
		} finally {
			if (connection_run != null) {
				connection_run.close();
			}
		}
	}
	
	public void fillConfigData(Run r) throws SQLException {
		List<Run> result = new LinkedList<Run>();
		result.add(r);
		fillConfigData(result);
	}
	
	public void SelectMessages(long partitionId, String userName, long runNumber, ResultSetCallback<ERSMessage> callback) throws SQLException {
		Run r = new Run(runNumber, 0, partitionId);
		fillConfigData(r);
		SelectMessages(
			new MessageCriteria(
				Long.MIN_VALUE, 
				Long.MAX_VALUE, 
				new Partition(null, partitionId), 
				userName, 
				r), 
			callback);
	}
	
	public void SelectMessages(MessageCriteria criteria, ResultSetCallback<ERSMessage> callback) throws SQLException {
		String columnNames = "PART_ID, PARTITION_NAME, USER_NAME,SESSION_ID,MSG_ID,MACHINE_NAME,APPLICATION_NAME,ISSUED_WHEN,SEVERITY,MSG_TEXT,PARAM,CHAINED_MSGS,LOGGED_WHEN,OPT_PARAM,QUALIFIERS";
		String sqlQuery = "SELECT "
				+ columnNames
				+ " FROM ATLAS_LOG_MESSG.LOG_MESSAGES "
				+ " INNER JOIN ATLAS_LOG_MESSG.PARTITION_IDS ON (ATLAS_LOG_MESSG.LOG_MESSAGES.PART_ID = ATLAS_LOG_MESSG.PARTITION_IDS.PARTITION_ID) " 
				+ " WHERE PART_ID = ? AND USER_NAME = ? AND RUN_NUMBER = ? AND (ISSUED_WHEN BETWEEN ? AND ?) ORDER BY ISSUED_WHEN ASC";
		ResultSet rs = null;
		PreparedStatement stmt = null;

		// FIXME: Filter Criteria
		
		try {
			// Create the statement and execute the query
			stmt = connection.prepareStatement(sqlQuery,
					ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			stmt.setLong(1, criteria.getPartition().getId());
			stmt.setString(2, criteria.getUserName());
			stmt.setLong(3, criteria.getRun().getRunNumber());
			stmt.setLong(4, criteria.getBeginTime());
			stmt.setLong(5, criteria.getEndTime());
			rs = stmt.executeQuery();
			log.info("SQL query executed") ;
			int count = 0 ;
			while (rs.next()) {
				count++ ;
				ERSMessage row = new ERSMessage(rs.getLong("PART_ID"), 
						rs.getString("PARTITION_NAME"),
						rs.getString("USER_NAME"), 
						rs.getLong("SESSION_ID"),
						rs.getString("MSG_ID"), 
						rs.getString("MACHINE_NAME"),
						rs.getString("APPLICATION_NAME"),
						rs.getLong("ISSUED_WHEN"), 
						rs.getString("SEVERITY"),
						rs.getString("MSG_TEXT"), 
						rs.getString("PARAM"),
						rs.getString("CHAINED_MSGS"),
						rs.getString("LOGGED_WHEN"), 
						rs.getString("OPT_PARAM"),
						rs.getString("QUALIFIERS"));
				callback.resultSetCallback(row);
			}
			log.info("Retreived " + count + " messages from ERS archive") ;
		}
		catch (final java.sql.SQLException ex)
			{
			ex.printStackTrace() ;	
			throw new RuntimeException(ex) ;
			}
		finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		}
	}
	
	public List<ERSMessage> SelectMessages(long partitionId, String userName, long runNumber) throws SQLException {
		final ArrayList<ERSMessage> result = new ArrayList<ERSMessage>();
		
		SelectMessages(partitionId, userName, runNumber, new ResultSetCallback<ERSMessage>() {
			@Override
			public void resultSetCallback(ERSMessage item) {
				result.add((ERSMessage)item);
			}
		});
		
		return result;
	}
	
	public RunStats SelectRunStats(MessageCriteria crit) throws SQLException {
		return SelectRunStats(crit.getPartition(), crit.getUserName(), crit.getRun());
	}
	
	/**
	 * 
	 * The PK for the run is the tuple (part_id, user_name, run_number). It may be possible to have 
	 * two messages from two different partitions with the same run_number!
	 * 
	 * @param p
	 * @param userName
	 * @param r
	 * @return
	 * @throws SQLException
	 */
	public RunStats SelectRunStats(Partition p, String userName, Run r) throws SQLException {
		String sqlQuery = "SELECT COUNT(*) AS COUNT, MIN(ISSUED_WHEN) as MIN, MAX(ISSUED_WHEN) as MAX"
				+ " FROM ATLAS_LOG_MESSG.LOG_MESSAGES "
				+ " WHERE PART_ID = ? AND USER_NAME = ? AND RUN_NUMBER = ?";
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
//		Map<String, String> result = new HashMap<String, String>();
		RunStats result = null;
		
		try {
			// Create the statement and execute the query
			stmt = connection.prepareStatement(sqlQuery,
					ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			stmt.setLong(1, p.getId());
			stmt.setString(2, userName);
			stmt.setLong(3, r.getRunNumber());
			rs = stmt.executeQuery();

			if (rs.next()) {
				//result.add(rs.getString("user_name"));
//				result.put("Run number", Long.toString(r.getRunNumber()));
//				result.put("Message count", rs.getString("COUNT"));
//				result.put("Begin time", rs.getString("MIN"));
//				result.put("End time", rs.getString("MAX"));
//				result.put("Begin time (str)", DateUtils.format(rs.getLong("MIN")*1000));
//				result.put("End time (str)", DateUtils.format(rs.getLong("MAX")*1000));
				result = new RunStats(r.getRunNumber(), rs.getLong("MIN"), rs.getLong("MAX"), rs.getLong("COUNT"));
			}
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		}
		return result;
	}

	public Partition getPartition(String release, String partition) throws SQLException {
		for (Partition w : SelectTDAQPartition(release)) {
			if (w.getName().equals(partition)) {
				return w;
			}
		}
		log.debug("getPartition: Unknown partition " + partition + " on " + release);
		return null;
	}
	
	public Run getRun(String release, String partition, String username, long startTime, long runNumber) throws SQLException {
		Partition p = getPartition(release, partition);
		if (p == null)
			return null;

		for (Run w : SelectRuns(p.getId(), username, startTime)) {
			if (w.getRunNumber() == runNumber) {
				return w;
			}
		}
		log.debug(String.format("Unknown Run %s %s %s %d at %d", release, partition, username, runNumber, startTime));
		return null;
	}

	public MessageCriteria messageCriteriaFactory(String releaseName, String partitionName, String userName, long runNumber) throws SQLException {
		log.debug(String.format("messageCriteriaFactory %s %s %s %d", releaseName, partitionName, userName, runNumber));
		Partition p = getPartition(releaseName, partitionName);
		Run r = getRun(releaseName, partitionName, userName, 0, runNumber);
		RunStats rstat = SelectRunStats(p, userName, r);
		return new MessageCriteria(rstat.getBeginTime(), rstat.getEndTime(), p, userName, r);
	}
	
	public MessageCriteria messageCriteriaFactory(long runNumber) throws SQLException {
		String columnNames = "TDAQ_RELEASE, PART_ID, PARTITION_NAME, USER_NAME ";
		String sqlQuery = "SELECT DISTINCT "
				+ columnNames
				+ " FROM ATLAS_LOG_MESSG.LOG_MESSAGES "
				+ " INNER JOIN ATLAS_LOG_MESSG.PARTITION_IDS ON (ATLAS_LOG_MESSG.LOG_MESSAGES.PART_ID = ATLAS_LOG_MESSG.PARTITION_IDS.PARTITION_ID) " 
				+ " WHERE RUN_NUMBER = ? ORDER BY RUN_NUMBER ASC";
		ResultSet rs = null;
		PreparedStatement stmt = null;
		List<MessageCriteria> result = new ArrayList<MessageCriteria>();

		try {
			// Create the statement and execute the query
			stmt = connection.prepareStatement(sqlQuery,
					ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			stmt.setLong(1, runNumber);
			rs = stmt.executeQuery();

			while (rs.next()) {
				String userName = rs.getString("USER_NAME");
				Partition p = new Partition(rs.getString("PARTITION_NAME"), rs.getLong("PART_ID"));
				Run r = new Run(runNumber, 0, p.getId());
				fillConfigData(r);
				RunStats rstat = SelectRunStats(p, userName, r);
				MessageCriteria row = new MessageCriteria(rstat.getBeginTime(), rstat.getEndTime(), p, userName, r);
				result.add(row);
			}
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		}
		
		if (result.size() != 1) {
			if (result.size() == 0) {
				throw new RuntimeException("Unknown run number: " + runNumber);
			} else {
				throw new RuntimeException("FAILURE: run number " + runNumber + " with inconsistent data, cardinal > 1");
			}
		}

		return result.get(0);
	}
}
