package ch.cern.tdaq.cassandra.replay;

import java.io.File;
// import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.cern.tdaq.cassandra.config.Configuration;


public class ISOsirisSwigReader implements IISOsirisReader {
	static {
		System.loadLibrary("osirisj");
	}
	
	private static final Log log = LogFactory.getLog(ISOsirisSwigReader.class);
	
	private boolean started = false;
	
	private long sincedec;
	private long tilldec;
	
	private osiris.api api = null;
	private osiris.playback play = null;
	private List<osiris.series_fetcher> fetchers = new ArrayList<osiris.series_fetcher>();
	
	public ISOsirisSwigReader(String since, String till) {		
		this.sincedec = osiris.osirisj.str2ts(since);
		this.tilldec = osiris.osirisj.str2ts(till);
				
		log.info(String.format("Time window for replay: %s to %s (%s %s)", since, till, sincedec, tilldec));
		
		log.info("Initializing OSIRIS SWIG wrapper");
		
		final String serverURL = Configuration.getInstance().getProperties().getProperty("replay.pbeast.server.url", "https://atlasop.cern.ch/");
		final Short cookieSetup = Short.valueOf(Configuration.getInstance().getProperties().getProperty("replay.pbeast.coockie.setup", "2"));
		api = new osiris.api_pbeast(serverURL, cookieSetup.shortValue());
	}

	public void addPartClass(String partClassNames) {
		/* "P;C,P;C" */
		log.info("Adding to replay (addPartClass): " + partClassNames);
		
		osiris.series_fetcher_by_part_class_attr ff = new osiris.series_fetcher_by_part_class_attr(api);
		
		String[] parts = partClassNames.split(",");
		
		for (String pc : parts) {
			String[] part_and_class = pc.split(";");
			
			ff.add_part_and_class(part_and_class[0], part_and_class[1]);
			
			log.info("Added to replay: " + part_and_class[0] + ":" + part_and_class[1]);
		}
		
		/* FIXME: Don't want to get garbage collected, make something better on the C++ side */
		fetchers.add(ff);
	}

	public void addPartClassAttr(String partClassAttrNames) {
		/* "P;C;A,P;C;A" */
		log.info("Adding to replay (addPartClassAttr): " + partClassAttrNames);
		
		osiris.series_fetcher_by_part_class_attr ff = new osiris.series_fetcher_by_part_class_attr(api);
		
		String[] parts = partClassAttrNames.split(",");
		
		for (String pc : parts) {
			String[] part_class_attr = pc.split(";");
			
			ff.add_part_class_attr(part_class_attr[0], part_class_attr[1], part_class_attr[2]);
			
			log.info("Added to replay: " + part_class_attr[0] + ":" + part_class_attr[1] + ":" + part_class_attr[2]);
		}
		
		/* FIXME: Don't want to get garbage collected, make something better on the C++ side */
		fetchers.add(ff);
	}

	@Override
	public String toString() {
		return "ISOsirisSwigReader";
	}
	
	@Override
	public ISOsirisEvent getNextOsirisEvent() {
		if (!started) 
			throw new RuntimeException("not started");
		
		if (!play.hasnext())
			return null;
		
		osiris.series_wrap e = play.getnext();
		List<String> attr_names = new ArrayList<String>();
		List<String> obj_name = new ArrayList<String>();
		osiris.StringVector a = e.getAttr_name();
		
		/*
		OSIRIS DEBUG attribute name ctpcore_objects/CtpcoreBusyInfoObject/name
		OSIRIS DEBUG object name L1CT.CTP.Instantaneous.BusyFractions/ctpcore_objects[9]/-1
		OSIRIS DEBUG attribute name ctpcore_objects/CtpcoreBusyInfoObject/counter_interval
		OSIRIS DEBUG object name L1CT.CTP.Instantaneous.BusyFractions/ctpcore_objects[1]/-1
		OSIRIS DEBUG attribute name ctpout_cables/CtpoutBusyInfoCable/counter_interval
 */
		
		for (int i = 0; i < a.size(); ++i) {
			attr_names.add(a.get(i));
		}
		
		osiris.PairStringIntVector vpii = new osiris.PairStringIntVector();
		osiris.osirisj.parse_nested_obj_name(e.getAttr_name(), e.getObj_name(), vpii);
		
		for (int i = 0; i < vpii.size(); ++i) {
			obj_name.add(vpii.get(i).getFirst());
			obj_name.add(Integer.toString(vpii.get(i).getSecond()));
		}
		
		ISOsirisEvent ret = new ISOsirisEvent(play.get_current_created(),
				play.get_current_last_updated(), 
				e.getPart_name(), 
				e.getClass_name(), 
				attr_names, 
				null, 
				obj_name, 
				e.getSeries().get_current_value_str());
		
		play.advance();
		
// ers.Logger.info("ISOsirisEvent:\n" + ret);

		return ret;
	}

	@Override
	public boolean isStarted() {
		return started;
	}

	@Override
	public void start() throws Exception {
		if (started) 
			throw new Exception("already started");
		
		started = true;

		play = new osiris.playback(api, sincedec, tilldec);

		for (osiris.series_fetcher x : fetchers) {
			play.inject_fetcher(x);
		}
		
		play.begin();
	}
}
