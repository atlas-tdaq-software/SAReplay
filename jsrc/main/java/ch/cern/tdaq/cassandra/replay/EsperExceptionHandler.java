package ch.cern.tdaq.cassandra.replay;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.espertech.esper.common.client.hook.exception.ExceptionHandler;
import com.espertech.esper.common.client.hook.exception.ExceptionHandlerContext;
import com.espertech.esper.common.client.hook.exception.ExceptionHandlerFactory;
import com.espertech.esper.common.client.hook.exception.ExceptionHandlerFactoryContext;

public class EsperExceptionHandler implements ExceptionHandlerFactory, ExceptionHandler {

	private static final Log log = LogFactory.getLog(EsperExceptionHandler.class);
	
	@Override
	public void handle(ExceptionHandlerContext context) {
		log.error("EsperExceptionHandler for statement '" + context.getStatementName() + "', EPL: " + context.getEpl(), context.getThrowable());
	}

	@Override
	public ExceptionHandler getHandler(ExceptionHandlerFactoryContext context) {
		return this;
	}
	
}
