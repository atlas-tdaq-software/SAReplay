package ch.cern.tdaq.cassandra.replay;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;

/** An ERSEvent acquired from the Oracle ERS Database.
 * 
 * @see ERSLogService
 * @author asantos
 *
 */
public class ERSMessage {
	public static Logger log = Logger.getLogger(ERSMessage.class); 

	/*
	 * String columnNames = "PART_ID,
	 * USER_NAME,SESSION_ID,MSG_ID,MACHINE_NAME,APPLICATION_NAME,
	 * ISSUED_WHEN,SEVERITY
	 * ,MSG_TEXT,PARAM,CHAINED_MSGS,LOGGED_WHEN,OPT_PARAM,QUALIFIERS";
	 */
	private long partitionId;
	private String partitionName;
	private String userName;
	private long sessionId;
	private String messageId;
	private String machineName;
	private String applicationName;
	private long issuedWhen;
	private String severity; // FIXME
	private String msgText;
	private String paramStr;
	private String chainedMsgsStr;
	private String loggedWhen; // FIXME
	private String optParamStr;
	private String qualifiersStr;
	private List<ERSMessage> chainedMsgs;
	
	// Fields to unserialize the "Chained Message" text.
	private static final Set<String> PARSER_FIELDS_chained = new HashSet<String>(Arrays.asList(
			new String[]{"Param", "Time", "Txt", "App", "Opt", "Host", "ID", "Sev", "Qual"}));

	// Fields to unserialize the "params" text.
	private static final Set<String> PARSER_FIELDS_params = new HashSet<String>(Arrays.asList(
			new String[]{"PACKAGE_NAME", "FILE_NAME", "FUNCTION_NAME", "LINE_NUMBER", "DATE_TIME"}));
	
	public ERSMessage(long partitionId, String partitionName, String userName, long sessionId,
			String messageId, String machineName, String applicationName,
			long issuedWhen, String severity, String msgText, String paramStr,
			String chainedMsgsStr, String loggedWhen, String optParamStr,
			String qualifiersStr) {
		super();
		this.partitionId = partitionId;
		this.partitionName = partitionName;
		this.userName = userName;
		this.sessionId = sessionId;
		this.messageId = messageId;
		this.machineName = machineName;
		this.applicationName = applicationName;
		this.issuedWhen = issuedWhen;
		this.severity = severity;
		this.msgText = msgText;
		this.paramStr = paramStr;
		this.chainedMsgsStr = chainedMsgsStr;
		this.loggedWhen = loggedWhen;
		this.optParamStr = optParamStr;
		this.qualifiersStr = qualifiersStr;
	}

	public static String prettyPrintMap(Map<String, String> map, int indentLevel) {
		StringBuilder sb = new StringBuilder();
		sb.append("[\n");
		for (Entry<String, String> e : map.entrySet()) {
			for (int k=0; k<indentLevel; ++k)
				sb.append("\t");
			sb.append(e.getKey());
			sb.append(" = \"");
			sb.append(e.getValue());
			sb.append("\"\n");
		}
		sb.append("]\n");
		return sb.toString();
	}

	public static String prettyPrintMap(List< Map<String, String> > list, int indentLevel) {
		StringBuilder sb = new StringBuilder();
		sb.append("[\n");
		for (Map<String, String> m : list) {
			sb.append(prettyPrintMap(m, indentLevel));
		}
		sb.append("]\n");
		return sb.toString();
	}

	@Override
	public String toString() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("partitionId", new Long(getPartitionId()).toString());
		map.put("partitionName", getPartitionName());
		map.put("userName", getUserName());
		map.put("sessionId", new Long(getSessionId()).toString());
		map.put("messageId", getMessageId());
		map.put("machineName", getMachineName());
		map.put("applicationName", getApplicationName());
		map.put("issuedWhen", new Long(getIssuedWhen()).toString());
		map.put("severity", getSeverity());
		map.put("msgText", getMsgText());
		map.put("paramStr", getParametersStr());
		map.put("parameters", prettyPrintMap(getParameters(), 2));
		map.put("chainedMsgsStr", getChainedMsgsStr());
		map.put("chainedMsgs", (getChainedMsgs() != null) ? "\n" + getChainedMsgs().toString() : "");
		map.put("chainedMsgsMap", prettyPrintMap(parseChainedMessageStr(getChainedMsgsStr()), 2));
		map.put("loggedWhen", getLoggedWhen());
		map.put("optParamStr", getOptParamStr());
		map.put("optionalParameters", prettyPrintMap(getOptionalParameters(), 2));
		map.put("qualifiers", getQualifiersStr());
		return "Message " + prettyPrintMap(map, 1);
	}
	
	private static Map<String, String> parseField(final String part, Iterable<String> fieldNamesList, final String keySuffix, char valueSuffix) {
		Map<String, String> current = new HashMap<String, String>();
		ArrayList<Integer> posList = new ArrayList<Integer>();
		
		for (String fieldName : fieldNamesList) {
			int pos = part.indexOf(fieldName + keySuffix);
			if (pos >= 0) {
				posList.add(pos);
			}
		}
		
		Integer[] posArr = posList.toArray(new Integer[posList.size()]);
		Arrays.sort(posArr);
		
		for (int i = 0; i < posArr.length; ++i) {
			int start = posArr[i], end = (i < (posArr.length - 1)) ? posArr[i+1] : part.length();
			String rawField = part.substring(start, end);
			int pos = rawField.indexOf(':');
			String key = rawField.substring(0, pos);
			String val = rawField.substring(pos + 2, 
				(rawField.charAt(rawField.length()-1) == valueSuffix) ? (rawField.length()-1) : rawField.length());
			
			current.put(key, val);
		}
		
		return current;
	}
	
	public static List< Map<String, String> > parseChainedMessageStr(final String msg) {
		ArrayList< Map<String, String> > result = new ArrayList< Map<String, String> >();

		// Always returns a Map<> instance, never null.
		
		if (msg == null || msg.isEmpty() || msg.equals("null"))
			return new ArrayList< Map<String, String> >();

		// Example:
		// "MsgStart: ID:[X]Host:[X]App:[X]Time:[X]Sev:[X]Txt:[X]Param:[X]Opt:[X]Qual:[X]EndMsg "
		
		//
		// The format is: "MsgStart field_name1:[field_data1]field_name2:[field_data2]...field_nameN:[field_dataN]EndMsg "
		// More than one chained messages are present by repeating the pattern: "MsgStart ... ]MsgEnd "+ 
		// 
		// The reference parser of the chained messages String is here, on the 
		// MessageListTableMouseClicked method:
		//
		//  https://svnweb.cern.ch/trac/atlastdaq/browser/DAQ/online/ls/trunk/jsrc/src/GUI/MessageListTableJPanel.java
		//
		// The program that logs the messages on the Oracle DB is this one:
		//
		//  https://svnweb.cern.ch/trac/atlastdaq/browser/DAQ/online/ls/trunk/src/bin/logReceiver.cc
		// 
		// and the specific method that generated the chainedMsg field is
		// LogBuffer::m_buildChainedMessage, available here:
		//  
		//  https://svnweb.cern.ch/trac/atlastdaq/browser/DAQ/online/ls/trunk/src/lib/LogBuffer.cc#L227
		//
		
		// Simple parser for the chained message format, just in case we add some new fields in the 
		// future.
		
		String[] pieces = msg.split("]EndMsg ");
		for (String part : pieces) {
			result.add(parseField(part, PARSER_FIELDS_chained, ":[", ']'));
		}
				
		// Post validation, just to err in the side of caution (TM).
		// Check that all the expected fields are present.
		
		/*
		 * A sample MessageMap:
		 * 
			Param = "PACKAGE_NAME: is. FILE_NAME: ../src/infodictionary.cc. FUNCTION_NAME: void ISInfoDictionary::update(const string&, const ISInfo&, bool, bool, bool, int) const. LINE_NUMBER: 226. DATE_TIME: 1393352085."
			Time = "1393352085"
			Txt = "CORBA system exception "TRANSIENT(1096024066=TRANSIENT_ConnectFailed)" has been raised"
			App = "RootController"
			Opt = "exception: TRANSIENT(1096024066=TRANSIENT_ConnectFailed). "
			Host = "pc-tdq-onl-77"
			ID = "ipc::CorbaSystemException"
			Qual = "is "
			Sev = "ERROR"
		 */
				
		for (Map<String, String> m : result) {
			/*
			 * https://svnweb.cern.ch/trac/atlastdaq/browser/DAQ/online/ls/trunk/jsrc/src/GUI/MessageListTableJPanel.java#L519
			// Back compatibility, before tdaq-02-00-03 (patches) messges 
			// did not include the qualifiers field.
			 */
			if (!m.containsKey("Qual")) {
				m.put("Qual", "");
			}
			
			Set<String> keySet = m.keySet();
			if (!keySet.containsAll(PARSER_FIELDS_chained)) {
				//throw new RuntimeException("Parsing Error, not all the required fields are present");
				log.error("Parsing Error on parseChainedMessageStr, not all the required fields are present: " + msg);
			}
		}
		
		return result;
	}
	
	public Map<String, String> parseParametersStr(String msg) {
		// Example:
		// param = "PACKAGE_NAME: is. FILE_NAME: ../src/infodictionary.cc. FUNCTION_NAME: void ISInfoDictionary::update(const string&, const ISInfo&, bool, bool, bool, int) const. LINE_NUMBER: 226. DATE_TIME: 1393352085."
		Map<String, String> m = parseField(msg, PARSER_FIELDS_params, ": ", '.');
		Set<String> keySet = m.keySet();
		if (!keySet.containsAll(PARSER_FIELDS_params)) {
			//throw new RuntimeException("Parsing Error, not all the required fields are present");
			log.error("Parsing Error on parseParametersStr, not all the required fields are present: " + msg);
		}
		return m;
	}

	/** Strict parser for the optional parameters field.
	 * 
	 * @param msg
	 * @param keySuffix
	 * @param valSuffix
	 * @return
	 */
	private Map<String, String> parseOptionalParametersStr_Ver1(final String msg, final String keySuffix, final String valSuffix) {
		Map<String, String> result = new HashMap<String, String>();

		/*
		 * We search the key separator first, and then follow the value separator. If no
		 * separator can be found on the remainder of the string we have a formating error, just abort
		 * with no more effort.
		 * 
		 * The main reason I've found this breaks is because the valSuffix is used inside the value
		 * of the field, ie, "message: this is broken. use json instead. "
		 */
		
		int start = 0;
		while (start < msg.length()) {
			int pos1 = msg.indexOf(keySuffix, start);
			if (pos1 < 0) {
				log.debug("Parsing Error on parseOptionalParametersStr_Ver1, could not cut the string to pieces: " + msg);
				return null;
			}
			int pos2 = msg.indexOf(valSuffix, pos1);
			if (pos2 < 0) {
				log.debug("Parsing Error on parseOptionalParametersStr_Ver1, could not cut the string to pieces: " + msg);
				return null;
			}
			String key = msg.substring(start, pos1);
			String val = msg.substring(pos1 + valSuffix.length(), pos2);
			result.put(key, val);
			start = pos2 + valSuffix.length();
		}
		
		return result;
	}

	/** Friendly parser for broken strings, we do the best to reconstruct the fields.
	 * 
	 * @param msg
	 * @param keySuffix
	 * @param valSuffix
	 * @return
	 */
	private Map<String, String> parseOptionalParametersStr_Ver2(final String msg, final String keySuffix, final String valSuffix) {
		Map<String, String> result = new HashMap<String, String>();
		List<Integer> posList = new ArrayList<Integer>();
		int start = 0;
		
		/*
		 * Divide the string by using the key separator as delimiter for each piece, and then check
		 * the presence of the value separator. We hope the string is still parseable though.
		 */
		while (start < msg.length()) {
			int pos = msg.indexOf(keySuffix, start);
			if (pos >= 0) {
				posList.add(pos);
				start = pos + keySuffix.length();
			} else {
				break;
			}
		}
		
		for (int i = 0; i < posList.size(); ++i) {
			String key, val;
			if (i == 0) {
				key = msg.substring(0, posList.get(i));
			} else {
				int pos2 = msg.lastIndexOf(valSuffix, posList.get(i));
				if (pos2 < 0) {
					log.debug("Parsing Error on parseOptionalParametersStr_Ver2, could not cut the string to pieces: " + msg);
					return null;
				}
				key = msg.substring(pos2 + valSuffix.length(), posList.get(i));
			}
			if (i == (posList.size() - 1)) {
				val = msg.substring(posList.get(i) + keySuffix.length());
			} else {
				int pos2 = msg.lastIndexOf(valSuffix, posList.get(i + 1));
				if (pos2 < 0) {
					log.debug("Parsing Error on parseOptionalParametersStr_Ver2, could not cut the string to pieces: " + msg);
					return null;
				}
				val = msg.substring(posList.get(i) + keySuffix.length(), pos2);
			}
			result.put(key, val);
		}
		return result;
	}
	
	public Map<String, String> parseOptionalParametersStr(final String msg) {
		String keySuffix = ": ";
		String valSuffix = ". ";
		
		Map<String, String> result;

		if (msg == null || msg.isEmpty() || msg.equals("null"))
			return new HashMap<String, String>();
		
		/*
		 * Try to parse the Optional Parameters field, using two different approaches:
		 * First, assume the string is parsable. 
		 * If for some reason the string parser wont work, do the second, which is to divide the
		 * terms by the keySuffix and use the leftmost valSuffix.
		 */
		result = parseOptionalParametersStr_Ver1(msg, keySuffix, valSuffix);
		if (result != null)
			return result;
		
		result = parseOptionalParametersStr_Ver2(msg, keySuffix, valSuffix);
		if (result != null)
			return result;
		
		log.error("Parsing Error on parseOptionalParametersStr, could not cut the string to pieces: " + msg);
		
		return new HashMap<String, String>();
	}
	
	public String[] parseQualifiersStr(final String msg) {
		List<String> result = new ArrayList<String>();

		if (msg == null || msg.isEmpty() || msg.equals("null"))
			return result.toArray(new String[0]);
		
		for (String e : msg.split(" ")) {
			if (e.length() > 0) {
				result.add(e);
			}
		}
		return result.toArray(new String[0]);
	}
	
	public ERSMessage mapToMessage(Map<String, String> m) {
		// FIXME: The 'Time' field is used twice; once for 'issuedWhen', and once for 'loggedWhen'.
		return new ERSMessage(getPartitionId(), getPartitionName(), getUserName(),
				getSessionId(), m.get("ID"), m.get("Host"),
				m.get("App"), Long.parseLong(m.get("Time")),
				m.get("Sev"), m.get("Txt"), m.get("Param"), "",
				m.get("Time"), m.get("Opt"), m.get("Qual"));
	}
	
	public long getPartitionId() {
		return partitionId;
	}

	public void setPartitionId(long partitionId) {
		this.partitionId = partitionId;
	}
	
	public String getPartitionName() {
		return partitionName;
	}
	
	public void setPartitionName(String partitionName) {
		this.partitionName = partitionName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public long getSessionId() {
		return sessionId;
	}

	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public long getIssuedWhen() {
		return issuedWhen;
	}
	
	public String getIssuedWhenFormatted() {
		if (getIssuedWhen() < 473428800000L) /* 1-Jan-1985 12:00:00 */
			return ch.cern.tdaq.cassandra.utils.DateUtils.format(getIssuedWhen() * 1000);
		else
			return ch.cern.tdaq.cassandra.utils.DateUtils.format(getIssuedWhen());
	}

	public void setIssuedWhen(long issuedWhen) {
		this.issuedWhen = issuedWhen;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getMsgText() {
		return msgText;
	}

	public void setMsgText(String msgText) {
		this.msgText = msgText;
	}

	public String getParametersStr() {
		return paramStr;
	}

	public void setParametersStr(String paramStr) {
		this.paramStr = paramStr;
	}

	public String getChainedMsgsStr() {
		return chainedMsgsStr;
	}

	public Map<String, String> getParameters() {
		// FIXME: Cache this result somewhere.
		return parseParametersStr(getParametersStr());
	}
	
	public void setChainedMsgsStr(String chainedMsgsStr) {
		this.chainedMsgsStr = chainedMsgsStr;
	}

	public String getLoggedWhen() {
		return loggedWhen;
	}

	public void setLoggedWhen(String loggedWhen) {
		this.loggedWhen = loggedWhen;
	}

	public String getOptParamStr() {
		return optParamStr;
	}

	public void setOptParamStr(String optParam) {
		this.optParamStr = optParam;
	}
	
	public Map<String, String> getOptionalParameters() {
		return parseOptionalParametersStr(getOptParamStr());
	}

	public String getQualifiersStr() {
		return qualifiersStr;
	}

	public void setQualifiersStr(String qualifiers) {
		this.qualifiersStr = qualifiers;
	}
	
	public String[] getQualifiers() {
		return parseQualifiersStr(getQualifiersStr());
	}
	
	public List<ERSMessage> getChainedMsgs() {
		if (chainedMsgs == null) {
			chainedMsgs = new ArrayList<ERSMessage>();
			List< Map<String, String> > L = parseChainedMessageStr(getChainedMsgsStr());
			if (L.size() > 0) {
				for (Map<String, String> map : L) {
					chainedMsgs.add(mapToMessage(map));
				}
			}
		}
		return chainedMsgs;
	}

	public void setChainedMsgs(List<ERSMessage> chainedMsgs) {
		this.chainedMsgs = chainedMsgs;
	}
}
