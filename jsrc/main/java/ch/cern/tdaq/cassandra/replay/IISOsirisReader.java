package ch.cern.tdaq.cassandra.replay;

/** Simple interface to read an IS event from the backend. Useful for testing purposes, only used by Osiris ftm.
 * 
 * @author asantos
 *
 */
public interface IISOsirisReader {

	public ISOsirisEvent getNextOsirisEvent();
	
	public boolean isStarted();
	
	public void start() throws Exception;
	
}
