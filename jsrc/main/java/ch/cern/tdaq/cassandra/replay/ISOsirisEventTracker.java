package ch.cern.tdaq.cassandra.replay;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeMap;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import daq.EsperUtils.ISEvent;
import daq.EsperUtils.TypedObject;
import daq.EsperUtils.ISWrapObject;

/** Keep track of object instances and repeating IS events.
 * 
 * @author asantos
 *
 */
public class ISOsirisEventTracker implements IISReader {
	
	/** For lack of a better name...
	 * 
	 * @author asantos
	 *
	 */
	private static class QueueItem implements Comparable<QueueItem> {
		public ISOsirisEvent osirisEvent;
		public long currentTime = 0;
		public long endTime = 0;
		public long stepTime = (long)(15 * 60 * 1e6); // Defaults time to step.
		public boolean end = false; // When end=true, once removed the element should not be added again.
		
		public QueueItem(ISOsirisEvent e) {
			this.osirisEvent = e;
			currentTime = e.getTimeStampCreated();
			endTime = e.getTimeStampLastUpdated();
		}
		
		@Override
		public int compareTo(QueueItem arg0) {
			return (int)(currentTime - arg0.currentTime);
		}
		
		/** Advance the time by stepTime. Returns false when this item should be removed from the queue.
		 * 
		 */
		public void advance() {
			/*
			 * We have to cases here: when ((current + step) > end), and ((current + step) == end).
			 * Both cases are the same for us, we flag the end so when end=true this is the last.
			 * By doing this we allow at least to events: 1. created, 2. lastUpdated.
			 */
			currentTime += stepTime;
			if (currentTime >= endTime) {
				currentTime = endTime;
				end = true;
			}
		}
	}
	
	public static String getObjectMapKey(String partitionName, String serverName, String objectName) {
		return partitionName + "\t" + serverName + "\t" + objectName;
	}
	
	private static final Log log = LogFactory.getLog(ISOsirisEventTracker.class);

	private IISOsirisReader reader;
	private ISOsirisEvent lastOsirisEvent = null;
	private ISEvent lastISEvent = null;
	
	// Object -> Attribute -> Value
	private Map<String, Map<String, TypedObject>> objectsMap = new HashMap<String, Map<String,TypedObject>>();
	
	private Map<String, String> objectsClassMap = new HashMap<String, String>();

	private PriorityQueue<QueueItem> queue = new PriorityQueue<QueueItem>();
	
	public ISOsirisEventTracker(IISOsirisReader reader) {
		this.reader = reader;
	}

	/*

object in IS

	L1CT.CTP.Instantaneous.BusyFractions ( 24/5/24 15:29:06.356457 )
	Attribute	Value
	run_number	476384
	lumiblock_number	358
	dead_time_settings	4 True 15 370 True True False 42 384 True True False 7 351 True True False 14 260 True True False 15 3600 True True False
	ctpcore_objects	[ True 0.015067907981574535 False 0.0 0.0 CTP-RSLT , True 0.0 False 0.0 0.0 CTP-DRND , True 0.0 False 0.0 0.0 CTP-MON , True 0.015067907981574535 False 0.0 0.0 BCKPL , True 0.0 False 0.0 0.0 CTP-ROIB-FIFO , True 0.0 False 0.0 0.0 CTP-DAQ-FIFO , True 0.0 False 0.0 0.0 to CTP , True 0.0 False 0.0 0.0 DAQ CTP , True 0.0 False 0.0 0.0 CTP-OnDemand , True 0.04824661836028099 False 0.0 0.0 Total0 , True 0.02451152540743351 False 0.0 0.0 Total1 , True 0.03351372480392456 False 0.0 0.0 Preventive0 , True 0.009443533606827259 False 0.0 0.0 Preventive1 , True 0.024591324850916862 False 0.0 0.0 Complex0 , True 0.0 False 0.0 0.0 Complex1 , True 0.009443533606827259 False 0.0 0.0 Simple ]
	ctpmi_objects	[ True 0.01490794587880373 False 0.0 0.0 BCKPL , True 0.0004471832071430981 False 0.0 0.0 ECR , True 0.0 False 0.0 0.0 Veto0 , False 0.0 False 0.0 0.0 Veto1 , False 0.0 False 0.0 0.0 VME , True 0.0 False 0.0 0.0 POW ]
	ctpout_cables	[ True 0.0014774500159546733 False 0.0 0.0 IBL 12 0 True , False 0.0 False 0.0 0.0 BCM 12 1 False , True 0.0004015000013168901 False 0.0 0.0 Pixel 12 2 True , True 0.0 False 0.0 0.0 SCT 12 3 True , True 0.01310016680508852 False 0.0 0.0 TRT 12 4 True , True 0.0 False 0.0 0.0 LAr EMB 13 0 True , True 0.0 False 0.0 0.0 LAr EC 13 1 True , True 0.0 False 0.0 0.0 LAr DT 13 2 True , True 0.0 False 0.0 0.0 Tile LB 13 3 True , True 0.0 False 0.0 0.0 Tile EB 13 4 True , True 0.0 False 0.0 0.0 L1Calo 14 0 True , True 0.0 False 0.0 0.0 MUCTPI 14 1 True , True 5.641666575684212e-05 False 0.0 0.0 RPC 14 2 True , True 0.0 False 0.0 0.0 TGC 14 3 True , False 0.0 False 0.0 0.0 BIS 7/8 14 4 False , True 3.90833338315133e-06 False 0.0 0.0 MDT B 15 0 True , True 4.174999958195258e-06 False 0.0 0.0 MDT EC 15 1 True , False 0.0 False 0.0 0.0 NSW-C 15 2 False , False 0.0 False 0.0 0.0 ZDC 15 3 False , False 0.0 False 0.0 0.0 LUCID 15 4 False , False 0.0 False 0.0 0.0 NSW-A 17 0 False , False 0.0 False 0.0 0.0 - 17 1 False , True 3.333333253863202e-08 False 0.0 0.0 AFP 17 2 True , False 0.0 False 0.0 0.0 LHCf 17 3 False , True 0.0 False 0.0 0.0 FFTV 17 4 True ]
	l1calo_busies	[ True 0.0 False 0.0 0.0 to L1Calo-0 , True 0.0 False 0.0 0.0 to L1Calo-1 , False 0.0 False 0.0 0.0 to L1Topo ]
	muctpi_busy	True 0.0 False 0.0 0.0 to MUCTPI
	roib_status	to ROIB

	 */
	
	 /* 
One attribute update received from pbeast

	// vertor attribute of type CtpoutBusyInfoCable[]
	 ISOsirisEvent [timeStampCreated=1715164515136938, timeStampLastUpdated=1715164515136938, partitionName=ATLAS, className=CtpBusyInfo,
	 attributeName=[ctpout_cables/CtpoutBusyInfoCable/counter_overflow],
	 objectName=[L1CT.CTP.Instantaneous.BusyFractions/ctpout_cables[13], -1],
	 rawValue=single:0:["0"]]

	 // single attribute 
	 ISOsirisEvent [timeStampCreated=1715164515136938, timeStampLastUpdated=1715164515136938, partitionName=ATLAS, className=CtpBusyInfo,
	 attributeName=[lumiblock_number],
	 objectName=[L1CT.CTP.Instantaneous.BusyFractions, -1],
	 rawValue=single:6:["678"]]

	 ISOsirisEvent [timeStampCreated=1715164515136938, timeStampLastUpdated=1715164515136938, partitionName=ATLAS, className=CtpBusyInfo,
	 attributeName=[dead_time_settings/CtpcoreDeadTimeSettings/complex0/CtpcoreComplexDeadTimeSetting/enabled],
	 objectName=[L1CT.CTP.Instantaneous.BusyFractions/dead_time_settings/complex0, -1],
	 rawValue=single:0:["1

	 ISOsirisEvent [timeStampCreated=1717404301573306, timeStampLastUpdated=1717404301573306, partitionName=ATLAS, 
	 className=ApplicationTestInfo,
	 attributeName=[componentsResult/TestInfo/testStatus],
	 objectName=[Setup.L1Ctp_Segment.CtpBusyMonitoringAppClient/componentsResult[0], -1],
	 rawValue=single:14:["PASSED"]]

	 */
	/*

		ISEvent [        - operation=UPDATE,
        - partition=ATLAS,
        - server=L1CT,
        - name=L1CT.CTP.Instantaneous.BusyFractions,
        - type=CtpBusyInfo,
        - is_time=1715164515136938,
        - creation_time=1716547107421,
        - attributes=lumiblock_number=(Integer)678, run_number=(Integer)475234, ]

wrongly reconstructed IS


	ISEvent [        - operation=UPDATE,
        - partition=ATLAS,
        - server=L1CT,
        - name=L1CT.CTP.Instantaneous.BusyFractions/dead_time_settings/slidingwindow,
        - type=CtpBusyInfo,
        - is_time=1715164515136938,
        - creation_time=1716547107421,
        - attributes=dead_time_settings/CtpcoreDeadTimeSettings/slidingwindow/CtpcoreSlidingWindowDeadTimeSetting/enabled=(Boolean)true,
dead_time_settings/CtpcoreDeadTimeSettings/slidingwindow/CtpcoreSlidingWindowDeadTimeSetting/size=(Integer)15,
dead_time_settings/CtpcoreDeadTimeSettings/slidingwindow/CtpcoreSlidingWindowDeadTimeSetting/veto0_enabled=(Boolean)true,
dead_time_settings/CtpcoreDeadTimeSettings/slidingwindow/CtpcoreSlidingWindowDeadTimeSetting/veto1_enabled=(Boolean)false,
dead_time_settings/CtpcoreDeadTimeSettings/slidingwindow/CtpcoreSlidingWindowDeadTimeSetting/window=(Integer)3600, ]

	ISEvent [        - operation=UPDATE,
        - partition=ATLAS,
        - server=L1CT,
        - name=L1CT.CTP.Instantaneous.BusyFractions/ctpout_cables[21],
        - type=CtpBusyInfo,
        - is_time=1715164515136938,
        - creation_time=1716547107426,
        - attributes=ctpout_cables/CtpoutBusyInfoCable/cable_number=(Integer)1,
ctpout_cables/CtpoutBusyInfoCable/counter_interval=(Float)0.0,
ctpout_cables/CtpoutBusyInfoCable/counter_overflow=(Boolean)false,
ctpout_cables/CtpoutBusyInfoCable/counter_value=(Float)0.0,
ctpout_cables/CtpoutBusyInfoCable/enabled=(Boolean)false,
ctpout_cables/CtpoutBusyInfoCable/fraction=(Float)0.0,
ctpout_cables/CtpoutBusyInfoCable/in_partition=(Boolean)false,
ctpout_cables/CtpoutBusyInfoCable/name=(String)-,
ctpout_cables/CtpoutBusyInfoCable/slot=(Integer)17, ]

 */

	private ISEvent buildISEventFrom(ISOsirisEvent e, long eventTime) {
		/*
1404908728440744        1404908728440744        ATLAS   Result  ["status"]      ["DQM.L1CaloRatesPar_Module_tau_0b40000b", "-1"]        single:5:["0"]
1404908728440979        1404956312483750        ATLAS   Result  ["objects"]     ["DQM.L1CaloRatesPar_Module_tau_0b40000c", "-1"]        array:14:0:[]
1404908728443337        1404912979785186        ATLAS   Result  ["tags", "Tag", "name"] ["DQM.L1CaloRates_EM", "-1", "tags", "0"]       single:14:["NumDisabled"]
1404908728443337        1404912979785186        ATLAS   Result  ["tags", "Tag", "name"] ["DQM.L1CaloRates_EM", "-1", "tags", "1"]       single:14:["NumExcluded"]
		 */
		/*
		 * We follow here the same schema as IS. The "Root Object Name" is the root 
		 * object in the IS data tree.
		 * 
		 * IS works by having a server name, then a dot, then an object name. Each object
		 * is represented by this compound name. Attributes and nested attributes are published
		 * from this root object.
		 * 
		 * For example, having the "SERVER.SomeObject" object with a complex structure and
		 * an attribute gets mofidied, the entire "SERVER.SomeObject" object gets
		 * published to IS, even if just a single attribute of a nested type got altered.
		 * On the listener side the entire IS root object gets received. This behaviour is
		 * what we need to replicate while reading data from P-BEAST.
		 * 
		 * The objectsMap Map maps a [Partition, ServerName, RootObjectName] triple to a TypedObject.
		 * 
		 * From P-BEAST, we're receiving updates from individual attributes on the object structure. 
		 * We need to reconstruct the object attributes from each of the individual events.
		 * Given a P-BEAST event, first find the root object name, and then tranverse through 
		 * the object tree to update this specific attribute.
		 * 
		 * For simple-typed attributes is easy because we have all the information we need. Given
		 * a Partition, Attribute name and Object Name we update the value accordingly.
		 * But for nested typed attributes we have a path of attributes to go through.
		 *
		 * Sample Data
		 * ===========
		 * 
		 * Ts                      Te                      P       C       A                O                                                      V
		 * 
		 * 1404908728440744        1404908728440744        ATLAS   Result  ["status"]      ["DQM.L1CaloRatesPar_Module_tau_0b40000b", "-1"]        single:5:["0"]
		 * 1404908728440979        1404956312483750        ATLAS   Result  ["objects"]     ["DQM.L1CaloRatesPar_Module_tau_0b40000c", "-1"]        array:14:0:[]
		 * 1404908728443337        1404912979785186        ATLAS   Result  ["tags", "Tag", "name"] ["DQM.L1CaloRates_EM", "-1", "tags", "0"]       single:14:["NumDisabled"]
		 * 1404908728443337        1404912979785186        ATLAS   Result  ["tags", "Tag", "name"] ["DQM.L1CaloRates_EM", "-1", "tags", "1"]       single:14:["NumExcluded"]
		 * 
		 * Arrays
		 * ======
		 * 
		 * Arrays are an issue. We have no way of knowing beforehand the size of arrays of 
		 * nested types. We only receive the index updated and from that we have to assume
		 * this would be the entire array. P-BEAST gives us the warranty that
		 * we'll eventually get all the events, meaning that for an array of size 10 we'll 
		 * eventually get all the 10 values. Index updates arrive in arbitrary order. 
		 * 
		 * Second and more important issue with arrays, if we have three or more levels of nested types, 
		 * we'd need to update several indices on several arrays. Example: 
		 * 
		 *   Object name: "objname/attr1[0]/attr2[0]".
		 * 
		 * This is how P-BEAST store and handle arrays of arrays. OSIRIS tool parses and transform the 
		 * previous string as:
		 * 
		 *   ["objname", "-1", "attr1", "0", "attr2", "0"]
		 * 
		 * Object Names
		 * ============
		 * 
		 * Object names are another issue here. Some IS object contain a slash "/" in their 
		 * name and the slash is also used on P-BEAST as an special separator for nested types 
		 * attributes updates.
		 *
		 * An example of a nested attribute with a Root Object Name containing a slash is the "Graph" IS class.
		 * 
		 * OSIRIS tool parses and determines the nested component on an object name.
		 * 
		 * 
		 */
		// FIXME: We're missing the DELETE edge.
		ISEvent.OP isop = ISEvent.OP.UPDATE;
		String partitionName = e.getPartitionName();
		String className = e.getClassName();
		List<String> attrName = e.getAttributeName();
		List<String> objName = e.getObjectName();
		
		String objNameParts0Dot[] = objName.get(0).split("\\.");
		String serverName = objNameParts0Dot[0];
		
		String rootObjectName = objName.get(0);
		String k = getObjectMapKey(partitionName, serverName, rootObjectName);
		
		if (!objectsMap.containsKey(k)) {
			objectsMap.put(k, new HashMap<String, TypedObject>());
			isop = ISEvent.OP.CREATE;
		}
		
		if (!objectsClassMap.containsKey(k)) {
			objectsClassMap.put(k, e.getClassName());
		}
		
		Map<String, TypedObject> root = objectsMap.get(k);
		String objValParts[] = e.getRawValue().split(":", 3);
		
		if (objValParts.length != 3) {
			throw new RuntimeException("Invalid value format");
		}
		
		if (attrName.size() < 1 || (attrName.size() % 2) != 1) {
			throw new RuntimeException("Invalid attribute format");
		}
		
		boolean isArrayValue = objValParts[0].equals("array");
		int pbeastType = Integer.parseInt(objValParts[1]);
		String vs = objValParts[2];
		Object v = null;

		if (!isArrayValue) {
			List<String> vsarr = QuotedStringParser.unquoteAsList(vs);
			if (vsarr.size() != 1) {
				throw new RuntimeException("Invalid value format: " + vs);
			}
			// Object[] varr = (Object[]) PbeastUtils.convertArray(pbeastType, vsarr);
			// v = varr[0];
			v = PbeastUtils.convertSingle(pbeastType, vsarr.get(0));
		} else {
			String[] lenAndData = vs.split(":", 2);
			List<String> vsarr = QuotedStringParser.unquoteAsList(lenAndData[1]);
			if (vsarr.size() != Integer.parseInt(lenAndData[0])) {
				throw new RuntimeException("Invalid value format: " + vs);
			}
			//Object[] varr = PbeastUtils.convertArray(pbeastType, vsarr);
			//v = varr;
			v = PbeastUtils.convertArray(pbeastType, vsarr);
		}
		
		if (attrName.size() == 1) {
			root.put(attrName.get(0), new TypedObject(v));
		} else {
			int w = 2, i;
			for (i = 0; i < attrName.size() - 2; i += 2, w += 2) {
				/* assert attrName.get(i).equals(objName.get(w)); */
				String a = attrName.get(i);
				int idxValue = Integer.parseInt(objName.get(w + 1));
				boolean isArrayNested = (idxValue >= 0);
				
				if (!root.containsKey(a)) {
					if (isArrayNested) {
						ISWrapObject[] wraparr = new ISWrapObject[idxValue + 1];
						
						for (int z = 0; z < wraparr.length; ++z) {
							wraparr[z] = new ISWrapObject();
							wraparr[z].setName(attrName.get(i + 1));
							wraparr[z].setDescription(attrName.get(i + 1));
						}
						
						root.put(a, new TypedObject(wraparr));
					} else {
						ISWrapObject wrap = new ISWrapObject();
						wrap.setName(attrName.get(i + 1));
						wrap.setDescription(attrName.get(i + 1));
						root.put(a, new TypedObject(wrap));
					}
				}
				
				if (isArrayNested) {
					ISWrapObject[] wraparr = root.get(a).getWrapObjectArray();
					if (idxValue >= wraparr.length) {
						int oldLen = wraparr.length;
						wraparr = Arrays.copyOf(wraparr, idxValue + 1);
						for (int z = 0; z < wraparr.length; ++z) {
							if (wraparr[z] == null) {
								wraparr[z] = new ISWrapObject();
								wraparr[z].setName(attrName.get(i + 1));
								wraparr[z].setDescription(attrName.get(i + 1));
							}
						}
						root.put(a, new TypedObject(wraparr));
					}
					root = wraparr[idxValue].getAttributes();
				} else {
					root = root.get(a).getWrapObject().getAttributes();
				}
			}
			
			root.put(attrName.get(i), new TypedObject(v));
		}
		
		return buildISEvent(isop, partitionName, serverName,
				rootObjectName, className, eventTime);
	}
		
	public static TypedObject deepCopy(TypedObject A) {
		if (A.getValue() == null) {
			return null;
		} else if (A.getValue() instanceof ISWrapObject[]) {
			ISWrapObject v[] = (ISWrapObject[])A.getValue();
			ISWrapObject w[] = new ISWrapObject[v.length];
			for (int i = 0; i < v.length; ++i) {
				ISWrapObject n = new ISWrapObject();
				n.setAttributes(deepCopy(v[i].getAttributes()));
				w[i] = n;
			}
			return new TypedObject(w);
		} else if (A.getValue() instanceof ISWrapObject) {
			ISWrapObject w = new ISWrapObject();
			w.setAttributes(deepCopy(A.getWrapObject().getAttributes()));
			return new TypedObject(w);
		} else if (A.getValue() instanceof int[]) {
			return new TypedObject( Arrays.copyOf((int[])A.getValue(), ((int[])A.getValue()).length));
		} else if (A.getValue() instanceof short[]) {
			return new TypedObject( Arrays.copyOf((short[])A.getValue(), ((short[])A.getValue()).length));
		} else if (A.getValue() instanceof long[]) {
			return new TypedObject( Arrays.copyOf((long[])A.getValue(), ((long[])A.getValue()).length));
		} else if (A.getValue() instanceof float[]) {
			return new TypedObject( Arrays.copyOf((float[])A.getValue(), ((float[])A.getValue()).length));
		} else if (A.getValue() instanceof double[]) {
			return new TypedObject( Arrays.copyOf((double[])A.getValue(), ((double[])A.getValue()).length));
		} else if (A.getValue() instanceof boolean[]) {
			return new TypedObject( Arrays.copyOf((boolean[])A.getValue(), ((boolean[])A.getValue()).length));
		} else if (A.getValue() instanceof String[]) {
			return new TypedObject( Arrays.copyOf((String[])A.getValue(), ((String[])A.getValue()).length));
		}
		return new TypedObject(A.getValue());
	}
	
	public static Map<String, TypedObject> deepCopy(Map<String, TypedObject> A) {
		TreeMap<String, TypedObject> R = new TreeMap<String, TypedObject>();
		
		for (String k : A.keySet()) {
			R.put(k, deepCopy(A.get(k)));
		}
		
		return R;
	}
	
	public ISEvent buildISEvent(ISEvent.OP isop, String partitionName, String serverName, 
			String objectName, String className, long eventTime) {
		String k = getObjectMapKey(partitionName, serverName, objectName);
		Map<String, TypedObject> attrs = objectsMap.get(k);
		
		if (attrs == null) {
			attrs = new TreeMap<String, TypedObject>();
		} else {
			attrs = deepCopy(attrs);
		}
		
		if (className == null) {
			className = objectsClassMap.get(k);
		}
		
		return new ISEvent(isop, 
				serverName, objectName, className, eventTime, attrs, partitionName);		
	}
	
	private ISOsirisEvent peekNextOsirisEvent() {
		if (lastOsirisEvent == null) {
			lastOsirisEvent = reader.getNextOsirisEvent();
		}
		
		return lastOsirisEvent;
	}
	
	private ISEvent internalGetNextISEvent() {
		ISOsirisEvent oe = peekNextOsirisEvent();
		
		// I've changed syntax here so its better readable (for me).
		
		if (oe == null && queue.size() == 0) 
		{
			// The end.
			return null;
		} 
		else if (oe != null && queue.size() == 0) 
		{
			// Just add the next one.
			queue.add(new QueueItem(oe));
			lastOsirisEvent = null;
		}
		else if (oe == null && queue.size() != 0) 
		{
			// Nothing to do here.
		}
		else if (oe != null && queue.size() != 0) 
		{
			// Add the peeked OSIRIS element.
			if (oe.getTimeStampCreated() < queue.peek().currentTime) {
				queue.add(new QueueItem(oe));
				lastOsirisEvent = null;
			}
		}
		
		QueueItem minQElem = queue.poll();
		ISEvent nextISEv = buildISEventFrom(minQElem.osirisEvent, minQElem.currentTime);
		if (!minQElem.end) {
			minQElem.advance();
			queue.add(minQElem);
		}
		
		return nextISEv;
	}
	
	public ISEvent peekNextISEvent() {
		if (lastISEvent == null) {
			lastISEvent = internalGetNextISEvent();
		}
		return lastISEvent;
	}
	
	public void nextISEvent() {
		lastISEvent = null;
	}
	
	@Override
	public ISEvent getEventByName(String partitionName, String infoName) {
		// infoName is "server.object".
		String[] parts = infoName.split(Pattern.quote("."), 2);
		if (parts.length != 2)
			throw new RuntimeException("getEventByName invalid infoName: " + infoName);
		// FIXME: Store the object last update time somewhere...
		return buildISEvent(ISEvent.OP.UPDATE, partitionName, parts[0], parts[1], null, 0);
	}
	
	@Override
	public TypedObject getInfoByIndex(String partition, String infoName, int parameterIndex) {
		// FIXME: Hooowww??!!
		throw new RuntimeException("Unimplemented: getInfoByIndex");
	}
	
	@Override
	public TypedObject getInfoByName(String partitionName, String infoName, String paramName) {
		ISEvent ie = getEventByName(partitionName, infoName);
		Map<String, TypedObject> m = ie.getAttributes();
		return m.get(paramName);
	}
	
}
