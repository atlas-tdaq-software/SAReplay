package ch.cern.tdaq.cassandra.replay;

import daq.EsperUtils.ISEvent;


/**
 * 
 * @author asantos
 *
 */
public class EventSeriesIS extends EventSeries {

	/** Returns the timing from an ISEvent as microseconds.
	 * 
	 * @param isEvent
	 * @return
	 */
	public static long getTimingAsMusec(ISEvent isEvent) {
		return isEvent.getTimeMicro();
	}

	private ISOsirisEventTracker tracker;
	
	public EventSeriesIS(ISOsirisEventTracker tracker) {
		this.tracker = tracker;
	}
	
	@Override
	public boolean isEmpty() {
		return tracker.peekNextISEvent() == null;
	}

	@Override
	public long getNextTimestamp() {
		return getTimingAsMusec(tracker.peekNextISEvent());
	}

	@Override
	public Object getNextEvent() {
		return tracker.peekNextISEvent();
	}
	
	@Override
	public void removeNextEvent() {
		tracker.nextISEvent();
	}
	
	@Override
	public void hit() {
		ISReader.setInstance(tracker);
	}
}
