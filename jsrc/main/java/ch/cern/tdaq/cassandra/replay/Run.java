package ch.cern.tdaq.cassandra.replay;

import java.util.Date;

/** Simple Run class for Querying the Oracle DB.
 * 
 * @author asantos
 *
 */
public class Run {
	private long runNumber;
	private long timestamp;
	private long partitionId;
	private String configData;

	public Run(long runNumber, long timestamp, long partitionId) {
		this.runNumber = runNumber;
		this.timestamp = timestamp;
		this.partitionId = partitionId;
	}

	public String getConfigData() {
		return configData;
	}
	
	public void setConfigData(String configData) {
		this.configData = configData;
	}
	
	public long getRunNumber() {
		return runNumber;
	}

	public void setRunNumber(long runNumber) {
		this.runNumber = runNumber;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public long getPartitionId() {
		return partitionId;
	}

	public void setPartitionId(long partitionId) {
		this.partitionId = partitionId;
	}

	public Date getTimestampAsDate() {
		return new Date(getTimestamp() * 1000);
	}

	@Override
	public String toString() {
		return "Run [runNumber=" + runNumber + ", timestamp=" + timestamp
				+ "(" + getTimestampAsDate() + "), partitionId=" + partitionId + ", configData=" + getConfigData() + "]";
	}
}
