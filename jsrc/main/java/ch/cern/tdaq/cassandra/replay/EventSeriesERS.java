package ch.cern.tdaq.cassandra.replay;

import java.util.Queue;

import ch.cern.tdaq.cassandra.reader.ConfigReader;
import daq.EsperUtils.ERSEvent;

/**
 * 
 * @author asantos
 *
 */
public class EventSeriesERS extends EventSeries {

	private Queue<ERSEvent> events;
	private String configDataFile;
	private MessageCriteria crit;
	private config.Configuration db;
	
	/** Returns the timing from an ERSEvent as microseconds.
	 * 
	 * @param ersEvent
	 * @return
	 */
	public static long getTimingAsMusec(ERSEvent ersEvent) {
		return ersEvent.getLongIssuedDate() * 1000000;
	}
	
	public EventSeriesERS(Queue<ERSEvent> events, String configDataFile, MessageCriteria w, config.Configuration db) {
		this.events = events;
		this.configDataFile = configDataFile;
		this.crit = w;
		this.db = db;
	}
	
	@Override
	public boolean isEmpty() {
		return events.isEmpty();
	}

	@Override
	public long getNextTimestamp() {
		return getTimingAsMusec(events.element());
	}
	
	@Override
	public Object getNextEvent() {
		return events.element();
	}
	
	@Override
	public void removeNextEvent() {
		events.remove();
	}
	
	@Override
	public void hit() {
		//if (db != null) {
		//	ConfigReader.injectStaticConfiguration(crit.getPartition().getName(), db);
		//}
	}
	
	public String getConfigDataFile() {
		return configDataFile;
	}
}
