package ch.cern.tdaq.cassandra.replay;

import java.util.Date;

/** Simple RunStats class for Querying the Oracle DB.
 * 
 * @author asantos
 *
 */
public class RunStats {
	private long runNumber;
	private long beginTime;
	private long endTime;
	private long messageCount;

	public RunStats() {

	}

	public RunStats(long runNumber, long beginTime, long endTime,
			long messageCount) {
		super();
		this.runNumber = runNumber;
		this.beginTime = beginTime;
		this.endTime = endTime;
		this.messageCount = messageCount;
	}

	public long getMessageCount() {
		return messageCount;
	}

	public void setMessageCount(long messageCount) {
		this.messageCount = messageCount;
	}

	public long getRunNumber() {
		return runNumber;
	}

	public void setRunNumber(long runNumber) {
		this.runNumber = runNumber;
	}

	public long getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(long beginTime) {
		this.beginTime = beginTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public String getBeginTimeStr() {
		return GMTDateUtils.format(getBeginTime() * 1000);
	}

	public String getEndTimeStr() {
		return GMTDateUtils.format(getEndTime() * 1000);
	}

	public String getBeginTimeGmtStr() {
		return GMTDateUtils.formatGMT(getBeginTime() * 1000);
	}

	public String getEndTimeGmtStr() {
		return GMTDateUtils.formatGMT(getEndTime() * 1000);
	}

	public Date getBeginTimeDate() {
		return new Date(getBeginTime() * 1000);
	}

	public Date getEndTimeDate() {
		return new Date(getEndTime() * 1000);
	}

}
