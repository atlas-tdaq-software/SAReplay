package ch.cern.tdaq.cassandra.replay.tests;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.*;

import ch.cern.tdaq.cassandra.replay.QuotedStringParser;

public class StringQuoteTest {

	@Test(expected=RuntimeException.class)
	public void testUnqoteEmpty1() {
		QuotedStringParser.unquoteAsList("");
	}

	@Test(expected=RuntimeException.class)
	public void testUnqoteEmpty2() {
		QuotedStringParser.unquoteAsList("][");
	}
	
	@Test
	public void testUnqoteEmpty3() {
		List<String> result = QuotedStringParser.unquoteAsList("[]");
		assertEquals(result.size(), 0);
	}

	@Test(expected=RuntimeException.class)
	public void testUnqoteBasic1() {
		QuotedStringParser.unquoteAsList("[1]");
	}

	@Test(expected=RuntimeException.class)
	public void testUnqoteBasic2() {
		QuotedStringParser.unquoteAsList("[\"1]");
	}
	
	@Test
	public void testUnqoteBasic3() {
		List<String> result = QuotedStringParser.unquoteAsList("[\"a\"]");
		assertEquals(result.size(), 1);
		assertEquals(result.get(0), "a");
	}

	@Test
	public void testUnqoteBasic4() {
		List<String> result = QuotedStringParser.unquoteAsList("[\"\"]");
		assertEquals(result.size(), 1);
		assertEquals(result.get(0), "");
	}

	@Test
	public void testUnqoteBasic5() {
		List<String> result = QuotedStringParser.unquoteAsList("[\"\",\"\"]");
		assertEquals(result.size(), 2);
		assertEquals(result.get(0), "");
		result = QuotedStringParser.unquoteAsList("[\"\", \"\"]");
		assertEquals(result.size(), 2);
		assertEquals(result.get(0), "");
	}
	
	@Test(expected=RuntimeException.class)
	public void testUnqoteBasic6() {
		QuotedStringParser.unquoteAsList("[\"\\a\"]");
	}

	@Test
	public void testUnqoteBasic7() {
		List<String> result = QuotedStringParser.unquoteAsList("[\"\\n\"]");
		assertEquals(result.size(), 1);
		assertEquals(result.get(0), "\n");
	}

	@Test(expected=RuntimeException.class)
	public void testUnqoteBasic8() {
		QuotedStringParser.unquoteAsList("[\"a\", q \"a\"]");
	}
	
	@Test(expected=RuntimeException.class)
	public void testUnqoteBasic9() {
		QuotedStringParser.unquoteAsList("[\"a\" \"a\"]");
	}

	@Test
	public void testUnqoteQuotes() {
		List<String> result = QuotedStringParser.unquoteAsList("[\"\\\"\"]");
		assertEquals(result.size(), 1);
		assertEquals(result.get(0), "\"");
		result = QuotedStringParser.unquoteAsList("[\"\\\\\"]");
		assertEquals(result.size(), 1);
		assertEquals(result.get(0), "\\");
		result = QuotedStringParser.unquoteAsList("[\"a\\\\a\\\\a\"]");
		assertEquals(result.size(), 1);
		assertEquals(result.get(0), "a\\a\\a");
	}
}
