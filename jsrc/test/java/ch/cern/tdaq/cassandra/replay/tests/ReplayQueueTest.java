package ch.cern.tdaq.cassandra.replay.tests;

import static org.junit.Assert.*;

import org.junit.*;

import ch.cern.tdaq.cassandra.replay.IISOsirisReader;
import ch.cern.tdaq.cassandra.replay.ISOsirisEventTracker;
import daq.EsperUtils.ISEvent;

public class ReplayQueueTest {
	
	@Test
	public void testEventTracker() {
		/*
		 * Some simple unit test to check the interleaving of the events of the 
		 * ISOsirisEventTracker class.
		 * 
		 */
		IISOsirisReader mockedReader = new DummyISOsirisReader();
		ISOsirisEventTracker tracker = new ISOsirisEventTracker(mockedReader);
		int counter = 0;
		String partNames[] = new String[]{"ATLAS1", "ATLAS2"};
		long nextts[] = new long[]{0, 10};
		
		for (;; counter++) {
			ISEvent ev = tracker.peekNextISEvent();
			if (ev == null)
				break;
			
			assertEquals(ev.getPartitionName(), partNames[counter % partNames.length]);
			assertEquals(ev.getTimeMicro(), nextts[counter % partNames.length]);
			
			nextts[counter % partNames.length] += 6e8;
			
			//System.out.println(ev.toString() + " " + ev.getTimeMicro());
			tracker.nextISEvent();
		}
		
		assertEquals(counter, 4);
	}
}
