package ch.cern.tdaq.cassandra.replay.tests;

import java.util.ArrayList;
import java.util.List;

import ch.cern.tdaq.cassandra.replay.IISOsirisReader;
import ch.cern.tdaq.cassandra.replay.ISOsirisEvent;

public class DummyISOsirisReader implements IISOsirisReader {

	private int idx = 0;
	private List<ISOsirisEvent> list = new ArrayList<ISOsirisEvent>();

	public DummyISOsirisReader() {
        List<String> ppp1List = new ArrayList<>();
        ppp1List.add("ppp1");
        List<String> testObject1List = new ArrayList<>();
        testObject1List.add("testObject1");
        List<String> ppp2List = new ArrayList<>();
        ppp2List.add("ppp2");
        List<String> testObject2List = new ArrayList<>();
        testObject2List.add("testObject1");

		list.add(new ISOsirisEvent(0, (long)(1e6 * 60 * 10), "ATLAS1", 
				"QQQ1", ppp1List, "test1", testObject1List, "single:0:[\"0\"]"));
		list.add(new ISOsirisEvent(10, (long)(1e6 * 60 * 10) + 10, "ATLAS2", 
				"QQQ2", ppp2List, "test2", testObject2List, "single:0:[\"0\"]"));
	}
	
	@Override
	public ISOsirisEvent getNextOsirisEvent() {
		if (idx >= list.size())
			return null;
		return list.get(idx++);
	}

    @Override
    public boolean isStarted() {
        return false;
    }

    @Override
	public void start() throws Exception {

	}
}
