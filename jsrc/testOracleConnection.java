import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import daq.coral.DBInfoExtractor;

class testOracleConnection 
{

public static void main(String[] args) {

try {

        DBInfoExtractor dbInfo = DBInfoExtractor.createFromLogicalName("LOG_MANAGER");
	
	Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
	DriverManager.setLoginTimeout(10);

System.out.println("ConnectionString: " + dbInfo.getConnectionString()) ;
System.out.println("User: " + dbInfo.getUser()) ;
System.out.println("Passwd: " + dbInfo.getPassword()) ;
	
	Connection conn = DriverManager.getConnection(dbInfo.getConnectionString(), dbInfo.getUser(), dbInfo.getPassword());
	
}
catch (final Exception e) {
            e.printStackTrace();
	System.out.println("Failed") ; 
	    System.exit(-1) ;
}

System.out.println("OK") ;
System.exit(0) ;
}
}
