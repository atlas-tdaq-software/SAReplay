#!/usr/bin/env python

import os
import sys
import StringIO
import numpy as np
import matplotlib.pyplot as plt
import pprint

all_data = {}
first_row = None

OUTDIR = "report"

if not os.path.exists(OUTDIR):
    os.makedirs(OUTDIR)

with open(sys.argv[1]) as f:
    for i, line in enumerate(f):
        if i == 0:
            first_row = line
        else:
            StatementName = line.split('\t')[1]
            if StatementName not in all_data:
                all_data[StatementName] = []
            all_data[StatementName].append(line)

#pprint.pprint(sorted(names.items(), key=lambda x: -x[1]))

for StatementName in all_data:
    print(StatementName)
    directive_data = StringIO.StringIO('\n'.join([first_row] + all_data[StatementName]))

    data = np.genfromtxt(directive_data, delimiter='\t', skip_header=0,
                         skip_footer=10, names=True)

    #print(data)
    # semilogy
    # plot

    plt.cla()
    plt.clf()

    plt.subplot(2, 1, 1)
    plt.semilogy(data['Timestamp'], data['CpuTime'], color='r', label='the data')
    plt.title(StatementName)
    plt.ylabel('CpuTime')

    plt.subplot(2, 1, 2)
    plt.semilogy(data['Timestamp'], data['NumInput'], color='r', label='the data')
    plt.title(StatementName)
    plt.ylabel('NumInput')

    #plt.show()
    plt.savefig(OUTDIR + "/" + StatementName + ".png", dpi=140)

