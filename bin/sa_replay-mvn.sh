#!/bin/bash

if [ ! -f $TDAQ_INST_PATH/VERSION ]
then
  echo "Error: You need to source a TDAQ version"
  exit 1
fi

if [ ! -d $1 ]
then
  echo "Error: Invalid SA settings directory"
  exit 1
fi

if [ ! -f $1/etc/cassandra.properties ]
then
  echo "Missing cassandra.properties file on $1/etc/"
  exit 1
fi

export TDAQ_APPLICATION_NAME="ShifterAssistant"

AAL_BASE_DIR=$(readlink -e $(dirname $0))
AAL_USER_DIR=$(readlink -e $1)
#AAL_MAIN_JAR="$AAL_BASE_DIR/../target/cassandrareplay-0.0.2.jar:$AAL_BASE_DIR/../target/dependency/commons-dbcp-1.4.jar:$AAL_BASE_DIR/../target/dependency/esper-4.11.0.jar:$AAL_BASE_DIR/../target/dependency/cassandra.jar"
AAL_MAIN_JAR="$AAL_BASE_DIR/../target/cassandrareplay-0.0.2.jar:$AAL_BASE_DIR/../target/dependency/commons-dbcp-1.4.jar:$AAL_BASE_DIR/../target/dependency/cassandra.jar"
AAL_CP="$AAL_USER_DIR/etc:$AAL_MAIN_JAR:${TDAQ_CLASSPATH}:${CLASSPATH}"
AAL_CLASSNAME=ch.cern.tdaq.cassandra.AALMain
#AAL_CLASSNAME=ch.cern.tdaq.cassandra.replay.AALReplayCmd 

cd $AAL_USER_DIR

echo "pwd: $(pwd)"
#echo "cp: $AAL_CP"

# This is a flag to allow the OKS/JNI packages to work on Java, otherwise it will throw a Segfault.
export TDAQ_ERS_NO_SIGNAL_HANDLERS=1

java -Xms1024m -Xmx2048m \
    -Duser.dir=$AAL_USER_DIR \
    -cp $AAL_CP $AAL_CLASSNAME $2 $3 $4 $5 $6 $7 # list-is-events tdaq-05-03-00 ATLAS vandelli 227137 

 
