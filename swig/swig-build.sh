#!/bin/bash

SWIG="$LCG_INST_PATH/$TDAQ_LCG_RELEASE/swig/4.0.2/$CMTCONFIG/bin/swig"
export SWIG_LIB="$LCG_INST_PATH/$TDAQ_LCG_RELEASE/swig/4.0.2/$CMTCONFIG/share/swig/4.0.2"

#SWIG=$(which swig)

cd $(dirname $0)

JAVA_SOURCES_DIR="../jsrc/main/java/osiris"
JAVA_CXX_DIR="java"
JAVA_CXX_FILE="osiris_swig_java_wrap.cxx"


mkdir -p $JAVA_CXX_DIR
mkdir -p $JAVA_SOURCES_DIR

rm -f $JAVA_CXX_DIR/$JAVA_CXX_FILE
rm -f $JAVA_SOURCES_DIR/*.java

$SWIG -v -c++ -Wall -I$PWD/.. -java -module osirisj -outdir $JAVA_SOURCES_DIR -package osiris -o $JAVA_CXX_DIR/$JAVA_CXX_FILE osiris_swig.i 

