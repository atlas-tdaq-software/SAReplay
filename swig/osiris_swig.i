
%module osirisext

%{

#include <cstddef>

#include "SAReplay/osiris_api.h"
#include "SAReplay/osiris_util.h"
#include "SAReplay/osiris_api_pbeast.h"
#include "SAReplay/osiris_replay.h" 

%}

/* STANDARD CONTAINERS */

%include "typemaps.i"
%include "stdint.i"
%include "cpointer.i"

%include "std_pair.i"
%include "std_string.i"
%include "std_vector.i"
%include "std_map.i"

/* SHARED POINTERS */

%include <boost_shared_ptr.i>
%shared_ptr(osiris::series)

/* STL CONTAINERS */

namespace std {
   %template(IntVector) vector<int>;
   %template(DoubleVector) vector<double>;
   %template(StringVector) vector<string>;
   %template(PairStringInt) pair<string, int>;
   %template(PairStringIntVector) vector<pair<string, int> >;
  //  %template(StringSeriesMap) std::map<std::string, osiris::series_ptr>;
}

/* STANDARD TYPES */

%pointer_functions(short, shortp);
%pointer_functions(int, intp);
%pointer_functions(float, floatp);
%pointer_functions(size_t, size_tp);

/* OPERATOR NAMING */

%rename(less_than) operator<;

/* EXCEPTION HANDLING */

%include "exception.i"

%exception {
  try {
    $action
  } catch (const std::exception& e) {
    SWIG_exception(SWIG_RuntimeError, e.what());
  }
}

/* PBEAST */

namespace pbeast {
  enum DataType {
    BOOL   = 0,
    S8     = 1,
    U8     = 2,
    S16    = 3,
    U16    = 4,
    S32    = 5,
    U32    = 6,
    S64    = 7,
    U64    = 8,
    FLOAT  = 9,
    DOUBLE = 10,
    ENUM   = 11,
    DATE   = 12,
    TIME   = 13,
    STRING = 14,
    OBJECT = 15,
    VOID   = 16
  };

  uint64_t mk_ts(uint32_t secs);
  
  uint64_t mk_ts(uint32_t secs, uint32_t mksecs);

  inline uint32_t ts2secs(uint64_t ts);

  inline uint32_t ts2mksecs(uint64_t ts);

}

%include "SAReplay/osiris_util.h"

%include "SAReplay/osiris_api.h"
%include "SAReplay/osiris_api_pbeast.h"

%exception osiris::playback::hasnext;
%exception osiris::playback::getnext;

%include "SAReplay/osiris_replay.h"

