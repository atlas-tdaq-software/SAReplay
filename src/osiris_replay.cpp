

#include <algorithm>
#include <iostream>
#include <set>
#include <map>
#include <unordered_map>
#include <string>
#include <memory>
#include <list>
#include <fstream>
#include <functional>
#include <chrono>

#include "SAReplay/osiris_util.h"
#include "SAReplay/osiris_api.h"
#include "SAReplay/osiris_replay.h"

#include <pbeast/exceptions.h>

namespace osiris {

namespace {
    bool heap_sort_comparator(const series_wrap& a, const series_wrap& b) {
        // The heap created by make_heap/pop_heap STL functions is max-heap.
        return !(a < b);
    }
}

playback::playback(::osiris::api& oapi, int64_t since, int64_t till) 
 : count_(0), oapi_(oapi), since_(since), till_(till), since_curr_(0), till_curr_(0), D_(0) 
{

}

void playback::inject_fetcher(series_fetcher* f) {
    all_fetchers.push_back(f);
}

void playback::begin() {
    count_ = 0;
    since_curr_ = since_;
    till_curr_ = since_;
    D_ = pbeast::mk_ts(60 * 60); /* 1 hr, FIXME */

    next_pass();
}

void playback::next_pass() {
std::cout << "#### DEBUG ##### Next pass" << std::endl ;

    std::size_t i;
    till_curr_ = std::min(since_curr_ + D_, till_);

    std::vector<osiris_work> work_series;

    for (auto p : all_fetchers) {
        p->fetch_series(work_series, since_curr_, till_curr_);
    }

    for (auto ws : work_series) {
        
//        for ( const auto at: ws.attr_name_ ) {
//            std::cerr << "next_pass work_series attr_name_: " << at << std::endl ;
//        }

        std::string attr_name_pb{join(ws.attr_name_, "/")};

        try {
            std::map<std::string, series_ptr> variant_data;

            oapi_.read_attribute_re_variant(ws.part_name_,
                ws.class_name_,
                attr_name_pb,
                ws.obj_name_regexp_,
                ws.since_,
                ws.till_,
                variant_data);

            for (const auto& vd : variant_data) {
                all_series.emplace_back(
                    ws.part_name_,
                    ws.class_name_,
                    ws.attr_name_,
                    vd.first,
                    vd.second);
            }
        } catch (ers::Issue& e) {
            std::string msg = "Cannot process data for partition \"" + ws.part_name_ +
                    "\", class \"" +  ws.class_name_ + "\" and attribute(s) \"" + attr_name_pb +
                    "\" because some error occurred during data retrieval: ";
            e.wrap_message(msg, "");
            ers::error(e);
            throw;
        }
    }

    if (osiris::settings::verbose()) {
        ERS_LOG("Building heap for " << all_series.size()
            << " series from " << since_curr_ 
            << " to " << till_curr_);
    }

    // Remove empty series
    for (i=0; i < all_series.size(); ) {
        if (!all_series[i].series->remaining()) {
            if (i != (all_series.size() - 1)) {
                std::swap(all_series[i], all_series[all_series.size() - 1]);
            }
            auto& x = all_series[all_series.size() - 1];
            if (osiris::settings::verbose()) {
                ERS_LOG("Series was empty: "
                    << x.part_name << " " 
                    << x.class_name << " " 
                    << quote_vec(x.attr_name) << " "
                    << x.obj_name << " ");
            }
            all_series.pop_back();
        } else {
            i++;
        }
    }

    // O(N)
    std::make_heap(all_series.begin(), all_series.end(), heap_sort_comparator);
std::cout << "#### DEBUG ##### Heap ready" << std::endl ;

    if (osiris::settings::verbose()) {
        ERS_LOG("Replaying...");
    }
}

void playback::advance() {
    std::pop_heap(all_series.begin(), all_series.end(), heap_sort_comparator);
    auto& x = all_series[all_series.size() - 1]; // 

    x.series->next();
    if (x.series->remaining() > 0) {
        std::push_heap(all_series.begin(), all_series.end(), heap_sort_comparator);
    } else {
        all_series.pop_back(); // remove 
    }

    ++count_;

    while ((!all_series.size())) {
        since_curr_ += D_;
        if (since_curr_ < till_) {
            next_pass();
        } else {
            break;
        }
    }
}

void playback::replay(replay_callback cb, void* user_data) {
    auto start = system_clock::now();

    // std::chrono::duration<double> elapsed_seconds = end-start;
    std::time_t end_time = system_clock::to_time_t(system_clock::now());
    std::cout << "#### DEBUG #### Begin started " << std::ctime(&end_time) << std::endl ;
    begin();
    end_time = system_clock::to_time_t(system_clock::now());
    std::cout << "#### DEBUG #### Begin finished " << std::ctime(&end_time) << std::endl ;
    while (hasnext())  {
        //std::cout << x.series->get_current_created() << "\t" << x.part_name << "\t" << x.class_name << "\t" << x.attr_name << "\t" << x.obj_name << "\t" << x.series->get_current_value_str() << std::endl;
        
        if (!cb(getnext(), user_data))
            break;

        advance();
    }

    end_time = system_clock::to_time_t(system_clock::now());
    std::cout << "#### DEBUG #### Replay finished " << std::ctime(&end_time) << std::endl ;

    ERS_LOG("Read from P-BEAST " << count_ << " events");
}

/******************************************************************************/

series_fetcher_by_part_class_attr::series_fetcher_by_part_class_attr(api& oapi)
 : osirisapi(oapi)
{
}

std::vector<std::string> extract_attribute(const std::string& attr_long) {
    size_t pos = 0, next = 0;
    std::string token;
    std::vector<std::string> ret ;
    while ( (next = attr_long.find('/', pos)) != std::string::npos) {
        ret.emplace_back(attr_long.substr(pos, next-pos));
        pos = next+1 ;
    }
    ret.emplace_back(attr_long.substr(pos, attr_long.length()-pos)) ;
    return ret ;
}

void series_fetcher_by_part_class_attr::fetch_series(std::vector<osiris_work>& series_vec, uint64_t since, uint64_t till) {
    for (auto& x : obj_desc_vec) { // part,class,attr
        const std::string& part_name = x[0];
        const std::string& class_name = x[1];
        const std::string& attr_name = x[2];
        
        series_vec.emplace_back(part_name,
                                class_name,
                                extract_attribute(attr_name),
                                ".*",
                                since,
                                till);

    }
}

void series_fetcher_by_part_class_attr::add_part_class_attr(const std::string& part_name, const std::string& class_name, const std::string& attr_name) {
    typedef std::set<std::reference_wrapper<const std::string>, std::less<const std::string>> StringRefSet;

    try {
        const std::vector<std::string>& allAttributes = osirisapi.list_attributes(part_name, class_name);
        StringRefSet sortedAttributes(allAttributes.cbegin(), allAttributes.cend());

        std::function<StringRefSet& (StringRefSet&)> findTheAttibutes = [&sortedAttributes, &attr_name] (StringRefSet& out) -> StringRefSet&
        {
            for(const auto& a : sortedAttributes) {
                if(a.get().find(attr_name) == 0) {
                    out.insert(a);
                }
            }

            return out;
        };

        if(StringRefSet attrs;
           (sortedAttributes.find(attr_name) != sortedAttributes.end()) ||
           (findTheAttibutes(attrs).size() == 0))
        {
            // Attribute found, that is a "normal" attribute or a nested *already expanded* attribute
            // It can also be an "unknowk" attributel; in that case it is added anyway and then details will be given by the server when queried
            ERS_LOG("Attribute \"" << attr_name << "\" for class \"" << class_name << "\" and partition \"" << part_name << "\" added to the replay");
            obj_desc_vec.emplace_back(std::vector<std::string>{part_name, class_name, attr_name});
        } else {
            // The attribute has not been found, check if it is a *not expanded* nested attribute
            for(const std::string& a : attrs) {
                ERS_LOG("Attribute \"" << a << "\" for class \"" << class_name << "\" and partition \"" << part_name << "\" added to the replay");
                obj_desc_vec.emplace_back(std::vector<std::string>{part_name, class_name, a});
            }
        }
    }
    catch(ers::Issue& ex) {
        std::string pre = "Cannot access attribute \"" + attr_name + "\" for partition \"" + part_name + "\" and class \"" + class_name + "\": ";
        ex.wrap_message(pre, "");
        throw;
    }
}

void series_fetcher_by_part_class_attr::add_part_and_class(const std::string& part_name, const std::string& class_name) {
    try {
        const std::vector<std::string>& attributes = osirisapi.list_attributes(part_name, class_name);
        for(const std::string& a : attributes) {
            std::cerr << "add_part_and_class attribute: " << a << std::endl ;
            obj_desc_vec.emplace_back(std::vector<std::string>{part_name, class_name, a});
        }
    }
    catch(ers::Issue& ex) {
        std::string pre = "Cannot access attributes for partition \"" + part_name + "\" and class \"" + class_name + "\" because some error occurred while retrieving data: ";
        ex.wrap_message(pre, "");
        throw;
    }
}

}
