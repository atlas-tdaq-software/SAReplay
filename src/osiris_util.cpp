

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>
#include <boost/date_time/posix_time/time_parsers.hpp>
#include "boost/date_time/local_time/local_time.hpp"

#include "SAReplay/osiris_util.h"


namespace osiris {

bool settings::s_verbose = false;


// FIXME: Stolen from pbeast/bin/read.cpp, missing timezone.
int64_t str2ts(const std::string& in)
{
    static boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));

    std::string s(in);
    std::replace(s.begin(), s.end(), 'T', ' ');
    boost::posix_time::ptime t;

    try
    {
        t = boost::posix_time::time_from_string(s);
    }
    catch (std::exception& e)
    {
        std::ostringstream text;
        text << "cannot parse " << in << " = \'" << in << "\': \"" << e.what() << '\"';
        throw std::runtime_error(text.str().c_str());
    }

    return (t - epoch).total_microseconds();
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::stringstream ss(s);
    std::vector<std::string> vec;
    std::string e;
    while (std::getline(ss, e, delim)) {
        vec.push_back(e);
    }
    return vec;
}

std::string join(const std::vector<std::string>& v, std::string delim) 
{
    std::string result;
    for (auto& x : v) {
        if (result.size())
          result += delim;
        result += x;
    }
    return result;
}

std::string quote_str(const std::string& s) {
    std::string res;

    res = "\"";

    for (std::size_t i=0; i<s.size(); ++i) {
        switch (s[i]) {
            case '\\':
            case '"':
                res += '\\';
                res += s[i];
                break;

            case '\n':
                res += '\\';
                res += 'n';
                break;
            
            case '\t':
                res += '\\';
                res += 't';
                break;

            default:
                res += s[i];
        }
    }
    res += "\"";

    return res;
}

std::string quote_vec(const std::vector<std::string>& v) {
    std::string res;
    for (auto& x : v) {
        if (res.size())
            res += ", ";
        res += quote_str(x);
    }
    return res;
}

}
