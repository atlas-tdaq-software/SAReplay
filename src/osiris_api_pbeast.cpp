#include "SAReplay/osiris_api_pbeast.h"
#include "SAReplay/osiris_api.h"
#include "SAReplay/osiris_util.h"

#include <pbeast/query.h>
#include <ers/ers.h>

#include <sstream>
#include <functional>
#include <iostream>
#include <limits>
#include <vector>
#include <string>
#include <deque>
#include <map>
#include <type_traits>

#include <boost/make_shared.hpp>

namespace osiris {

namespace {
    template<typename T, bool IS_ARRAY>
    void convert(const std::shared_ptr<pbeast::QueryDataBase>& d, std::map<std::string, series_ptr>& variant_data) {
        pbeast::DataType type = d->type();

        typedef typename std::conditional<(IS_ARRAY == false),
                                          pbeast::QueryData<pbeast::DataPoint<T>>,
                                          pbeast::QueryData<pbeast::DataPoint<std::vector<T>>>
        >::type DataType;

        typedef typename std::conditional<(IS_ARRAY == false),
                                          pbeast::SeriesData<T>,
                                          pbeast::SeriesVectorData<T>
        >::type SeriesType;

        typedef typename std::conditional<(IS_ARRAY == false),
                                          series_single<T>,
                                          series_array<T>
        >::type InternalSeries;

        DataType* data = static_cast<DataType*>(d.get());

        for(const auto& item : data->get_data()) {
            std::vector<SeriesType> seriesData;
            seriesData.reserve(item.second.size());

            for(const auto& x : item.second) {
                // TODO: currently, null values are not properly handled in the Java code
                uint64_t ts = x.ts();
                if(x.is_null() == false) {
                    seriesData.emplace_back(ts, ts, x.value());
                }
            }

            variant_data.emplace(item.first, boost::make_shared<InternalSeries>(type, std::move(seriesData)));
        }
    }

    template<typename T>
    void convert(const std::shared_ptr<pbeast::QueryDataBase>& d, std::map<std::string, series_ptr>& variant_data) {
        if(d->is_array() == false) {
            convert<T, false>(d, variant_data);
        } else {
            convert<T, true>(d, variant_data);
        }
    }

    pbeast::ServerProxy::CookieSetup convert(unsigned short opt) {
        switch(opt) {
            case 0:
                return pbeast::ServerProxy::CookieSetup::AutoUpdateKerberos;
                break;
            case 1:
                return pbeast::ServerProxy::CookieSetup::AutoUpdateUserCertificates;
                break;
            case 2:
                return pbeast::ServerProxy::CookieSetup::None;
                break;
            default:
                return pbeast::ServerProxy::CookieSetup::NotInitited;
        }
    }
}

api_pbeast::api_pbeast(const std::string& serverURL, unsigned short cs) :
        m_pbeast_serv(new pbeast::ServerProxy(serverURL, convert(cs)))
{
}

api_pbeast::~api_pbeast() {
}

std::string api_pbeast::backend_name() {
    return "PBeast API";
}

std::vector<std::string> api_pbeast::list_partitions() {
    return m_pbeast_serv->get_partitions();
}

std::vector<std::string> api_pbeast::list_classes(const std::string& part_name) {
    return m_pbeast_serv->get_classes(part_name);
}

bool series_data_overlaps(const pbeast::SeriesDataBase& v, uint64_t since, uint64_t till) {
    return v.get_ts_created() <= till && v.get_ts_last_updated() >= since;
}

std::vector<std::string> api_pbeast::list_attributes(const std::string& part_name, const std::string& class_name) {
    std::vector<std::string> attrs = m_pbeast_serv->get_attributes(part_name, class_name) ;
    std::cout << "DEBUG class: " << class_name ;
    for ( const auto& attr: attrs ) {
        std::cout << " attribute: " << attr  ;
    }
    std::cout << std::endl ;
    return m_pbeast_serv->get_attributes(part_name, class_name) ;
}

void api_pbeast::list_objects(const std::string& part_name,
                             const std::string& class_name,
                             const std::string& attr_name,
                             uint64_t since,
                             uint64_t till,
                             std::set<std::string>& objects)
{
    const std::vector<std::string>& objects_vec = m_pbeast_serv->get_objects(part_name,
                                                                             class_name,
                                                                             attr_name,
                                                                             "",
                                                                             since,
                                                                             till);
    objects.insert(objects_vec.begin(), objects_vec.end());
}

void api_pbeast::list_objects(const std::string& part_name,
                             const std::string& class_name,
                             const std::string& attr_name,
                             uint64_t since,
                             uint64_t till,
                             std::vector<std::string>& objects)
{
    const std::vector<std::string>& objects_vec = m_pbeast_serv->get_objects(part_name,
                                                                             class_name,
                                                                             attr_name,
                                                                             "",
                                                                             since,
                                                                             till);
    objects.reserve(objects.size() + objects_vec.size());
    for(const std::string& v : objects_vec) {
        objects.emplace_back(v);
    }
}

void api_pbeast::read_attribute_re_variant(const std::string& part_name,
                                          const std::string& class_name,
                                          const std::string& attr_name,
                                          const std::string& obj_name_regexp,
                                          uint64_t since,
                                          uint64_t till,
                                          std::map<std::string, series_ptr>& variant_data)
{
    ERS_LOG("Retrieving " << part_name << "." << class_name << "." << attr_name << "." << obj_name_regexp << " since " << since << " to " << till);
    
    const std::vector<std::shared_ptr<pbeast::QueryDataBase>>& data =
            m_pbeast_serv->get_data(part_name,
                                    class_name,
                                    attr_name,
                                    obj_name_regexp,
                                    true,
                                    since,
                                    till,
                                    0,
                                    0,
                                    true,
                                    0,
                                    pbeast::FillGaps::Near,
                                    pbeast::FunctionConfig(),
                                    pbeast::ServerProxy::ZipCatalog | pbeast::ServerProxy::ZipData,
                                    false);
    ERS_DEBUG(0, "Got " << data.size() << " pbeast rows");

    for(const auto& x : data) {
        switch(x->type()) {
            case pbeast::BOOL:
                convert<bool>(x, variant_data);
                break;
            case pbeast::S8:
                convert<int8_t>(x, variant_data);
                break;
            case pbeast::U8:
                convert<uint8_t>(x, variant_data);
                break;
            case pbeast::S16:
                convert<int16_t>(x, variant_data);
                break;
            case pbeast::U16:
                convert<uint16_t>(x, variant_data);
                break;
            case pbeast::ENUM:
            case pbeast::S32:
                convert<int32_t>(x, variant_data);
                break;
            case pbeast::DATE:
            case pbeast::U32:
                convert<uint32_t>(x, variant_data);
                break;
            case pbeast::S64:
                convert<int64_t>(x, variant_data);
                break;
            case pbeast::TIME:
            case pbeast::U64:
                convert<uint64_t>(x, variant_data);
                break;
            case pbeast::FLOAT:
                convert<float>(x, variant_data);
                break;
            case pbeast::DOUBLE:
                convert<double>(x, variant_data);
                break;
            case pbeast::STRING:
                convert<std::string>(x, variant_data);
                break;
            case pbeast::OBJECT:
            case pbeast::VOID:
                // Do nothing
                ERS_LOG("OBJECT and VOID types are not supported, skipping...");
                break;
        }
    }
}

}
