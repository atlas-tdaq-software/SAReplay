#include "SAReplay/osiris_api.h"
#include "SAReplay/osiris_util.h"

#include <sstream>
#include <functional>
#include <iostream>
#include <limits>
#include <vector>
#include <string>
#include <deque>

namespace osiris {

void convert_obj_names_to_regexp(const std::vector<std::string>& obj_names, std::string& obj_name_regexp) {

    for (const auto& x : obj_names) {
        if (obj_name_regexp.size() > 0) {
            obj_name_regexp += "|";
        }
        std::string y;
        for (auto c : x) {
            // ^ . $ | ( ) [ ] * + ? \ /
            switch (c) {
                case '^':
                case '.':
                case '$':
                case '|':
                case '(':
                case ')':
                case '[':
                case ']':
                case '*':
                case '+':
                case '?':
                case '\\':
                case '/':
                    y += '\\';
                default:
                    y += c;
            }
        }

        obj_name_regexp += y; // FIXME: ESCAPE TEXT
    }

}

void parse_nested_obj_name(
    const std::vector<std::string>& attr_names, 
    const std::string& obj_name, 
    std::vector<std::pair<std::string, int>> & out) 
{
    /* For a description of this method read the comments on the .h file.
     * FIXME: We should be splitting the string from back to front.
     */

    if (attr_names.size() == 0)
        throw std::runtime_error("empty attr_names");

    if (attr_names.size() == 1) {
        out.emplace_back(obj_name, -1);
    } else {
        size_t start = 0, i;
        std::string ss = "/" + attr_names[0];
        start = obj_name.find(ss, start);

        if (start == std::string::npos)
            throw std::runtime_error("1 missing attribute attr_name=" + attr_names[0] + " on obj_name=" + obj_name);

        out.emplace_back(obj_name.substr(0, start), -1);

        /* "hola/pepe[1]" */
        /*  012345678901  */

        for (i = 0; i < (attr_names.size() - 2); i += 2) {
            auto& x = attr_names[i];
            ss = "/" + x;
            if (start >= obj_name.size() || obj_name.compare(start, ss.size(), ss) != 0)
                throw std::runtime_error("2 missing attribute attr_name=" + x + " on obj_name=" + obj_name);

            auto w = start + ss.size();
            bool is_array = (w < obj_name.size()) && (obj_name[w] == '[');
            int array_idx;
            if (is_array) {
                auto z = obj_name.find("]", w);
                if (z == std::string::npos)
                    throw std::runtime_error("wrong string format for attr_name=" + x + " on obj_name=" + obj_name);
                array_idx = std::stoi(obj_name.substr(w + 1, z - w - 1));
                start = z + 1;
            } else {
                array_idx = -1;
                start = w;
            }
            out.emplace_back(x, array_idx);
        }
        if (start != obj_name.size())
            throw std::runtime_error("wrong string format on obj_name=" + obj_name + " start=" + std::to_string(start));
    }
}

template<>
std::string series_single<int8_t>::get_current_value_str() const {
    std::stringstream ss ;

    ss << "single:" << get_pbeast_type() << ":[\"" << static_cast<int32_t>(data_[p_].get_value()) << "\"]";

    return ss.str();
}

template<>
std::string series_array<int8_t>::get_current_value_str() const {
    std::stringstream ss;
    int i = 0;

    ss << "array:" << get_pbeast_type() << ":" << data_[p_].get_value().size() << ":[";
    for (const auto& w : data_[p_].get_value()) {
        if (i > 0)
            ss  << ", ";
        ss << "\"" << static_cast<int32_t>(w) << "\"";
        i++;
    }
    ss << "]";
    return ss.str();
}

template<>
std::string series_single<uint8_t>::get_current_value_str() const {
    std::stringstream ss ;

    ss << "single:" << get_pbeast_type() << ":[\"" << static_cast<uint32_t>(data_[p_].get_value()) << "\"]";

    return ss.str();
}

template<>
std::string series_array<uint8_t>::get_current_value_str() const {
    std::stringstream ss;
    int i = 0;

    ss << "array:" << get_pbeast_type() << ":" << data_[p_].get_value().size() << ":[";
    for (const auto& w : data_[p_].get_value()) {
        if (i > 0)
            ss  << ", ";
        ss << "\"" << static_cast<uint32_t>(w) << "\"" ;
        i++;
    }
    ss << "]";
    return ss.str();
}


template<>
std::string series_single<double>::get_current_value_str() const {
    std::stringstream ss ;

    ss << "single:" << OSIRIS_DOUBLE_AS_LONG << ":[";
    {
        union {
            double d;
            int64_t i;
        } n;
        n.d = n.i = 0;
        /* FIXME: ENDIANESS ISSUE HERE ! */
        n.d = data_[p_].get_value();
        ss << "\"" << n.i << "\"";
    }
    ss << "]";

    return ss.str();
}

template<>
std::string series_single<float>::get_current_value_str() const {
    std::stringstream ss ;

    ss << "single:" << OSIRIS_FLOAT_AS_INT << ":[";
    {
        union {
            float f;
            int32_t i;
        } n;
        n.f = n.i = 0;
        /* FIXME: ENDIANESS ISSUE HERE ! */
        n.f = data_[p_].get_value();
        ss << "\"" << n.i << "\"" ;
    }
    ss << "]";

    return ss.str();
}

template<>
std::string series_array<double>::get_current_value_str() const {
    std::stringstream ss;
    int i = 0;

    ss << "array:" << OSIRIS_DOUBLE_AS_LONG << ":" << data_[p_].get_value().size() << ":[";
    for (const auto& w : data_[p_].get_value()) {
        if (i > 0)
            ss  << ", ";

        {
            union {
                double d;
                int64_t i;
            } n;
            n.d = n.i = 0;
            /* FIXME: ENDIANESS ISSUE HERE ! */
            n.d = w;
            ss << "\"" << n.i << "\"" ;
        }
        
        i++;
    }
    ss << "]";
    return ss.str();
}

template<>
std::string series_array<float>::get_current_value_str() const {
    std::stringstream ss;
    int i = 0;

    ss << "array:" << OSIRIS_FLOAT_AS_INT << ":" << data_[p_].get_value().size() << ":[";
    for (const auto& w : data_[p_].get_value()) {
        if (i > 0)
            ss  << ", ";

        {
            union {
                float f;
                int32_t i;
            } n;
            n.f = n.i = 0;
            /* FIXME: ENDIANESS ISSUE HERE ! */
            n.f = w;
            ss << "\"" << n.i << "\"";
        }
        
        i++;
    }
    ss << "]";
    return ss.str();
}

api::~api() {

}

series_ptr api::read_attribute_variant(
    const std::string& part_name, 
    const std::string& class_name, 
    const std::string& attr_name, 
    const std::string& obj_name, 
    uint64_t since, 
    uint64_t till) 
{
    std::vector<std::string> obj_names;
    std::string obj_name_regexp;
    std::map<std::string, series_ptr> variant_data;

    obj_names.push_back(obj_name);
    
    convert_obj_names_to_regexp(obj_names, obj_name_regexp);
    read_attribute_re_variant(part_name, class_name, attr_name, obj_name_regexp, since, till, variant_data);

    auto it = variant_data.find(obj_name);
    if (it != variant_data.end()) {
        return it->second;
    }

    // NOTE: since the series is empty, the type should not really matter
    return build_empty_series(pbeast::BOOL, false);
}

void api::read_attribute_variant(
    const std::string& part_name, 
    const std::string& class_name, 
    const std::string& attr_name, 
    const std::vector<std::string>& obj_names, 
    uint64_t since, 
    uint64_t till, 
    std::map<std::string, series_ptr>& variant_data) 
{
    std::string obj_name_regexp;
    convert_obj_names_to_regexp(obj_names, obj_name_regexp);
    read_attribute_re_variant(part_name, class_name, attr_name, obj_name_regexp, since, till, variant_data);
}

template <typename T>
series_ptr get_empty_series(bool is_array, pbeast::DataType data_type)
{
    if (is_array)
        return series_ptr(new series_array<T>(data_type, std::move(std::vector<pbeast::SeriesVectorData<T>>())));
    return series_ptr(new series_single<T>(data_type, std::move(std::vector<pbeast::SeriesData<T>>())));
}

series_ptr api::build_empty_series(pbeast::DataType type, bool is_array) {
    switch(type)
    {
      case pbeast::BOOL:    return get_empty_series<bool>(is_array, type);
      case pbeast::S8:      return get_empty_series<int8_t>(is_array, type);
      case pbeast::U8:      return get_empty_series<uint8_t>(is_array, type);
      case pbeast::S16:     return get_empty_series<int16_t>(is_array, type);
      case pbeast::U16:     return get_empty_series<uint16_t>(is_array, type);
      case pbeast::ENUM:
      case pbeast::S32:     return get_empty_series<int32_t>(is_array, type);
      case pbeast::DATE:
      case pbeast::U32:     return get_empty_series<uint32_t>(is_array, type);
      case pbeast::S64:     return get_empty_series<int64_t>(is_array, type);
      case pbeast::TIME:
      case pbeast::U64:     return get_empty_series<uint64_t>(is_array, type);
      case pbeast::FLOAT:   return get_empty_series<float>(is_array, type);
      case pbeast::DOUBLE:  return get_empty_series<double>(is_array, type);
      case pbeast::STRING:  return get_empty_series<std::string>(is_array, type);
      case pbeast::VOID:    return get_empty_series<pbeast::Void>(is_array, type);
      default:
        {
          std::ostringstream text;
          text << "data type " << pbeast::as_string(static_cast<pbeast::DataType>(type)) << " is not supported";
          throw std::runtime_error(text.str().c_str());
        }
    }
}

// FIXME: I'm specializing this function for pbeast::Void because there is
// an ambiguous declaration for the ostream& << operator.
template <>
std::string series_single<pbeast::Void>::get_current_value_str() const {
    return "pbeast::Void";
}

template <>
std::string series_array<pbeast::Void>::get_current_value_str() const {
    return "pbeast::Void";
}

} // namespace osiris
