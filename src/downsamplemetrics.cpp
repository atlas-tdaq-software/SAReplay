
/*

Author: asantos

Downsample Esper metrics file very fast, this program is used by the
web application sareplayweb to generate simple graphics of the metrics.

*/

#include <iostream>
#include <map>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cinttypes>
#include <stdint.h>

#define MAXLINECHARS 8192
#define METRIC_LINE_FORMAT "%" SCNd64 "\t%s\t%" SCNd64 "\t%" SCNd64 "\t%" SCNd64 "\t%" SCNd64 "\t%" SCNd64 "\n"

struct data {
    data() : counter_total(0), counter_current(0) {}

    void reset_V() {
        for (int i = 0; i < 5; ++i) V[i] = 0;
    }

    int64_t counter_total;
    int64_t counter_current;
    int64_t V[5];
};

void downsample(const char* file_in_name, const char* file_out_name, int limit_per_stmt) {
    FILE* file_in;
    FILE* file_out;
    char line[MAXLINECHARS], first_line[MAXLINECHARS], stmt_name[MAXLINECHARS];
    int64_t in_ts, in_vals[5];
    std::map<std::string, data> indata;

    /*
     * I'm using fopen instead of std::fstream since fprintf and fscanf is
     * much faster than "fin >>" and "fout <<".
     */
    file_in = fopen(file_in_name, "r");

    if (!file_in) {
        perror("Error opening file_in_name\n");
        exit(EXIT_FAILURE);
    }

    file_out = fopen(file_out_name, "w");

    if (!file_in) {
        fclose(file_in);
        perror("Error opening file_out_name\n");
        exit(EXIT_FAILURE);
    }

    fgets(first_line, MAXLINECHARS, file_in);
#if 0
    puts(first_line);
#endif
    
    while (!feof(file_in)) {
        fgets(line, MAXLINECHARS, file_in);
        sscanf(line, "%" SCNd64 "\t%s\t", &in_ts, stmt_name);

        auto it = indata.find(stmt_name);
        if (it == indata.end()) {
            indata[stmt_name] = data();
#if 0
            puts(stmt_name);
#endif
        } else {
            it->second.counter_total += 1;
        }
    }

    rewind(file_in);
    fgets(first_line, MAXLINECHARS, file_in);
    fputs(first_line, file_out);

    while (!feof(file_in)) {
        fgets(line, MAXLINECHARS, file_in);
        sscanf(line, METRIC_LINE_FORMAT, 
            &in_ts, stmt_name, &in_vals[0], &in_vals[1], 
            &in_vals[2], &in_vals[3], &in_vals[4]);

        data & e = indata[stmt_name];
        int64_t C = std::max((int64_t)1, e.counter_total / limit_per_stmt);

        e.counter_current += 1;

        for (int i = 0; i < 5; ++i) {
            e.V[i] += in_vals[i];
        }

        if (e.counter_current >= C) {
            fprintf(file_out, METRIC_LINE_FORMAT, 
                in_ts, stmt_name, in_vals[0], in_vals[1], 
                in_vals[2], in_vals[3], in_vals[4]);
            e.counter_current = 0;
            e.reset_V();
        }
    }

    fclose(file_in);
    fclose(file_out);
}

int main(int argc, char** argv) {
    if (argc < 4) {
        printf("Usage: %s file_in_name file_out_name limit_per_stmt\n", argv[0]);
        return 1;
    }

    downsample(argv[1], argv[2], std::atoi(argv[3]));

    return 0;
}
