Workflow for new Directives on ATLAS Shifter Assistant

Please check the documentation at:

  https://twiki.cern.ch/twiki/bin/viewauth/Atlas/SaReplayProject

For any questions contact:

  Alejandro Santos (asantos@cern.ch)

