#!/bin/bash

cd $(dirname $0)

USERNAME=$(echo -n $(klist -a | grep "Default principal" | cut -d":" -f2 | cut -d"@" -f1))

for x in $(svn ls svn+ssh://asantos@svn.cern.ch/reps/atlastdaq/SA/) 
do 
  svn export svn+ssh://$USERNAME@svn.cern.ch/reps/atlastdaq/SA/$x/trunk/etc/directives $x
done

