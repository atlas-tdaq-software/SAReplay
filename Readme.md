Shifter Assistant Replay
========================

Development
-----------

Currently there is no setup that works outside of P1. This means that you will need to
package and deploy new rpms on the test instance in P1 to try out changes.

### Frontend

The Shifter Assistant Replay is a JavaScript heavy application. All
rendering is done on the client side, the backend just serves the initial page
and API. To organize all JavaScript files [webpack][8] is used. We also use
[Babel][9] as a ES6 to ES5 compiler to take advantage of ES6 but ensure backwards
compatibility with old browsers.

To install all JavaScript dependencies we need [npm][3] installed. After installing
it we can run inside `sareplay/frontend/` directory:

```bash
$ npm install
```

This command will fetch and install all JavaScript dependencies listed in the
package.json file.

During development I prefer having the files compiling in the background as they
are changed. We can archive this by running:

```bash
$ npm run dev
```

This means that a new file `sareplayweb/static/js/app.js` is generated. This way
you can also catch some syntax errors, as the compiler will complain about not correct
JavaScript.

The architecture is similar to the one in the Shifter Assistant Web Interface project and
you can get more details about it there:
https://gitlab.cern.ch/atlas-tdaq-software/SAWebInterface/blob/master/README.md


Deployment
----------

The Shifter Assistant Replay is deployed in 2 parts. One parts is deployed as part
of the TDAQ release and the web interface is deployed as a rpm. A [docker container][13]
can be used to easily build the rpm. If it is not possible to use docker the rpm can
also be built manually.

> If you decide to use docker for building the RPM or development, clone the project outside of `/afs`.
> There is a bug that prevents docker accessing files on AFS:
> https://github.com/docker/docker/issues/17019

### Docker rpm build

To build an RPM using docker just clone this repository and run `build-rpm.sh`.
```bash
$ git clone ssh://git@gitlab.cern.ch:7999/atlas-tdaq-software/SAReplay.git
$ cd SAReplay/sareplayweb
$ ./build-rpm.sh
```
When it finishes a new .rpm file will be created in CDW. This file can be installed in production.
NB: (a) you must be in docker group on the localhost, (b) your CWD should not be on afs 

### Manual rpm build

An [RPM spec][2] file is provided in the repository. To package the application an appropriate RPM
build environment needs to be set up. More information about the setup can be found [here][2].

To build the RPM run:
```bash
$ rpmbuild -ba sareplayweb.spec
```

The generated RPM will be located under:
RPMBUILD/RPMS/noarch/

[2]: https://fedoraproject.org/wiki/How_to_create_an_RPM_package
[3]: https://www.npmjs.com/
[8]: http://webpack.github.io/
[9]: https://babeljs.io/
[13]: https://gitlab.cern.ch/bekoloba/atlas-rpm-builder
