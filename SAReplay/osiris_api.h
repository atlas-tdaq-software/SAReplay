#ifndef OSIRIS_API_H
#define OSIRIS_API_H

#include <iostream>
#include <string>
#include <set>
#include <cstdint>
#include <memory>
#include <sstream>
#include <algorithm>

#include <boost/shared_ptr.hpp>

#include <pbeast/data-type.h>
#include <pbeast/series-data.h>

#include "osiris_util.h"

namespace osiris {

enum osiris_data_types {
    OSIRIS_DOUBLE_AS_LONG = 256, OSIRIS_FLOAT_AS_INT = 257
};

/** Given a list of strings concatenates the string with an "or" for regular expression matching.
 *
 */
void convert_obj_names_to_regexp(const std::vector<std::string>& obj_names, std::string& obj_name_regexp);

/**
 * This magic function parses an object name string and returns nested type information from it.
 * Example: given the object name string: 
 *
 *   "DQM.L1CaloRatesPar_pp2_ppm13_021d0c02/tags[4]" 
 *
 * We want to split the pieces: "DQM.L1CaloRatesPar_pp2_ppm13_021d0c02", "tags", "[4]".
 * where "DQM.L1CaloRatesPar_pp2_ppm13_021d0c02" is the root object name, "tags" is the name of 
 * the nested attribute name, and "4" is the array index on the array, since "tags" is an array.
 *
 * Since an object name may contain a "/" on it, we can not just take the object name string,
 * we need more information to positively parse the individual parts. We use the attribute 
 * name, which gives us all the information we need.
 *
 * Example: given the attribute name path: ["tags", "Tag", "name"] find the "tags" string
 * inside the obj_name string, and use that position to mark the first split. Then right
 * after the attr name may follow either a '[' or a '/'. If it's a '[' then the attribute
 * is an array. 
 *
 */
void parse_nested_obj_name(const std::vector<std::string>& attr_names,
                           const std::string& obj_name,
                           std::vector<std::pair<std::string, int> > & out);

class series {
    public:
        series(pbeast::DataType type, bool is_array) :
                type_(type), is_array_(is_array)
        {
        }
        virtual ~series() {
        }

#ifndef SWIG
        series(const series&) = delete;
        series & operator=(const series&) = delete;
#endif

        virtual size_t size() const = 0;
        virtual size_t remaining() const = 0;
        virtual void next() = 0;
        virtual int64_t get_current_created() const = 0;
        virtual int64_t get_current_last_updated() const = 0;
        virtual std::string get_current_value_str() const = 0;

        pbeast::DataType get_pbeast_type() const {
            return type_;
        }

        bool get_is_array() const {
            return is_array_;
        }

    private:
        pbeast::DataType type_;
        bool is_array_;
};

typedef boost::shared_ptr<series> series_ptr;

template<typename T>
class series_single: public series {
    public:

#ifndef SWIG
        series_single(pbeast::DataType type, std::vector<pbeast::SeriesData<T> >&& data) :
                series(type, false), data_(data), p_(0)
        {
        }
#else
        series_single(pbeast::DataType type, std::vector<pbeast::SeriesData<T> >& data)
        : series(type, false), data_(data), p_(0) {}
#endif

        virtual size_t size() const override {
            return data_.size();
        }

        virtual int64_t get_current_created() const override {
            return data_[p_].get_ts_created();
        }

        virtual int64_t get_current_last_updated() const override {
            return data_[p_].get_ts_last_updated();
        }

        virtual std::string get_current_value_str() const override {
            /*
             * We're generating an array here. It's easier to quote the string
             * and easier to reuse the same code on the Java reader.
             */
            std::stringstream ss, tmpss ;

            ss << "single:" << get_pbeast_type() << ":";
            tmpss << data_[p_].get_value();
            ss << "[" << quote_str(tmpss.str()) << "]";

            return ss.str();
        }

        virtual size_t remaining() const override {
            return data_.size() - p_;
        }

        virtual void next() override {
            ++p_;
        }

    private:
        std::vector<pbeast::SeriesData<T> > data_;
        size_t p_;
};

template<typename T>
class series_array: public series {
    public:

#ifndef SWIG
        series_array(pbeast::DataType type, std::vector<pbeast::SeriesVectorData<T> >&& data) :
                series(type, true), data_(data), p_(0)
        {
        }
#else
        series_array(pbeast::DataType type, std::vector<pbeast::SeriesVectorData<T> >& data)
        : series(type, true), data_(data), p_(0) {}
#endif

        virtual size_t size() const override {
            return data_.size();
        }

        virtual int64_t get_current_created() const override {
            return data_[p_].get_ts_created();
        }

        virtual int64_t get_current_last_updated() const override {
            return data_[p_].get_ts_last_updated();
        }

        virtual std::string get_current_value_str() const override {
            std::stringstream ss;
            int i = 0;

            ss << "array:" << get_pbeast_type() << ":" << data_[p_].get_value().size() << ":[";
            for(const auto& w : data_[p_].get_value()) {
                std::stringstream tmpss;
                tmpss << w;
                if(i > 0)
                    ss << ", ";
                ss << quote_str(tmpss.str());
                i++;
            }
            ss << "]";
            return ss.str();
        }

        virtual size_t remaining() const override {
            return data_.size() - p_;
        }

        virtual void next() override {
            ++p_;
        }

    private:
        std::vector<pbeast::SeriesVectorData<T> > data_;
        size_t p_;
};

class api {
    public:

        api() {
        }
        virtual ~api();

#ifndef SWIG
        // Non-copyable
        api(const api&) = delete;
        api & operator=(const api&) = delete;
#endif 

        virtual std::string backend_name() = 0;

        virtual std::vector<std::string> list_partitions() = 0;

        virtual std::vector<std::string> list_classes(const std::string& part_name) = 0;

        virtual std::vector<std::string> list_attributes(const std::string& part_name,
                                                         const std::string& class_name) = 0;

        virtual void list_objects(const std::string& part_name,
                                  const std::string& class_name,
                                  const std::string& attr_name,
                                  uint64_t since,
                                  uint64_t until,
                                  std::set<std::string>& objects) = 0;

        virtual void list_objects(const std::string& part_name,
                                  const std::string& class_name,
                                  const std::string& attr_name,
                                  uint64_t since,
                                  uint64_t until,
                                  std::vector<std::string>& objects) = 0;

        virtual series_ptr read_attribute_variant(const std::string& part_name,
                                                  const std::string& class_name,
                                                  const std::string& attr_name,
                                                  const std::string& obj_name,
                                                  uint64_t since,
                                                  uint64_t till);

        virtual void read_attribute_variant(const std::string& part_name,
                                            const std::string& class_name,
                                            const std::string& attr_name,
                                            const std::vector<std::string>& obj_names,
                                            uint64_t since,
                                            uint64_t till,
                                            std::map<std::string, series_ptr>& variant_data);

        virtual void read_attribute_re_variant(const std::string& part_name,
                                               const std::string& class_name,
                                               const std::string& attr_name,
                                               const std::string& obj_name_regexp,
                                               uint64_t since,
                                               uint64_t till,
                                               std::map<std::string, series_ptr>& variant_data) = 0;

        static series_ptr build_empty_series(pbeast::DataType type, bool is_array);
};

}

#endif
