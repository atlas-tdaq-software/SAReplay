#ifndef OSIRIS_REPLAY_H
#define OSIRIS_REPLAY_H

#include <string>
#include <cstdint>

#include "SAReplay/osiris_api.h"

namespace osiris {

class api;
class series;

struct osiris_work {
        osiris_work(const std::string& part_name,
                    const std::string& class_name,
                    const std::vector<std::string>& attr_name,
                    const std::string& obj_name_regexp,
                    uint64_t since,
                    uint64_t till) :
                part_name_(part_name), class_name_(class_name), attr_name_(attr_name), obj_name_regexp_(obj_name_regexp), since_(since), till_(till)
        {

        }

        std::string part_name_;
        std::string class_name_;
        std::vector<std::string> attr_name_;
        std::string obj_name_regexp_;
        uint64_t since_;
        uint64_t till_;
        //std::map<std::string, series_ptr> variant_data_;
};

struct series_wrap {
        series_wrap(std::string p, std::string c, std::vector<std::string> a, std::string o, series_ptr ptr) :
                part_name(p), class_name(c), attr_name(a), obj_name(o), series(ptr)
        {
        }

        bool operator<(const series_wrap& s) const {
            return (series->get_current_created()) < (s.series->get_current_created());
        }

        std::string part_name;
        std::string class_name;
        std::vector<std::string> attr_name;
        std::string obj_name;
        series_ptr series;
};

class series_fetcher {
    public:
        series_fetcher() {
        }
        virtual ~series_fetcher() {
        }

#ifndef SWIG
        // Non-copyable
        series_fetcher(const series_fetcher&) = delete;
        series_fetcher & operator=(const series_fetcher&) = delete;
#endif

        virtual void fetch_series(std::vector<osiris_work>& series_vec, uint64_t since, uint64_t till) = 0;
};

class series_fetcher_by_part_class_attr: public series_fetcher {
    public:
        series_fetcher_by_part_class_attr(api& oapi);

        virtual void fetch_series(std::vector<osiris_work>& series_vec, uint64_t since, uint64_t till) override;

        void add_part_class_attr(const std::string& part_name,
                                 const std::string& class_name,
                                 const std::string& attr_name);
        void add_part_and_class(const std::string& part_name,
                                const std::string& class_name);

    private:
        api& osirisapi;
        std::vector<std::vector<std::string> > obj_desc_vec;
};

typedef bool (*replay_callback)(const series_wrap& sw, void* user_data);

class playback {
    public:
        playback(api& oapi, int64_t since, int64_t till);

#ifndef SWIG
        // Non-copyable
        playback(const playback&) = delete;
        playback & operator=(const playback&) = delete;
#endif

        void inject_fetcher(series_fetcher* f);

        void replay(replay_callback cb, void* user_data);

        void begin();

        inline bool hasnext() {
            return (all_series.size() > 0); // || (since_curr_ < till_);
        }

        inline series_wrap& getnext() {
            return all_series[0];
        }

        void advance();

        inline int64_t get_current_created() {
            return hasnext() ? std::min(till_curr_, std::max(since_curr_, getnext().series->get_current_created())) : 0;
        }

        inline int64_t get_current_last_updated() {
            return hasnext() ?
                    std::min(till_curr_, std::max(since_curr_, getnext().series->get_current_last_updated())) :
                    0;
        }

    private:
        void next_pass();

        std::vector<series_fetcher*> all_fetchers;
        std::vector<series_wrap> all_series;
        std::size_t count_;

        api& oapi_;

        int64_t since_;
        int64_t till_;
        int64_t since_curr_;
        int64_t till_curr_;
        int64_t D_;
};

} // namespace osiris

#endif // OSIRIS_REPLAY_H
