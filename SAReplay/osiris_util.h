

#ifndef OSIRIS_UTIL_H
#define OSIRIS_UTIL_H

#include <cstdint>
#include <string>
#include <vector>


namespace osiris {

struct settings {
    static inline bool verbose() {
        return s_verbose;
    }

    static inline void set_verbose(bool v) {
        s_verbose = v;
    }

private:
    static bool s_verbose;
};

int64_t str2ts(const std::string& in);

std::vector<std::string> split(const std::string &s, char delim);
std::string join(const std::vector<std::string>& v, std::string delim);

std::string quote_str(const std::string& s);

std::string quote_vec(const std::vector<std::string>& v);

}

#endif
