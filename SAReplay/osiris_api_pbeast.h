#ifndef OSIRIS_API_CORBA_H
#define OSIRIS_API_CORBA_H

#include "SAReplay/osiris_api.h"

#include <memory>

namespace pbeast {
class ServerProxy;
}

namespace osiris {

class api_pbeast: public api {
    public:

        api_pbeast(const std::string& serverURL, unsigned short cookieSetup);
        virtual ~api_pbeast();

#ifndef SWIG
        // Non-copyable
        api_pbeast(const api_pbeast&) = delete;
        api_pbeast & operator=(const api_pbeast&) = delete;
#endif

        virtual std::string backend_name();

        virtual std::vector<std::string> list_partitions() override;

        virtual std::vector<std::string> list_classes(const std::string& part_name) override;

        virtual std::vector<std::string> list_attributes(const std::string& part_name, const std::string& class_name)
                override;

        virtual void list_objects(const std::string& part_name,
                                  const std::string& class_name,
                                  const std::string& attr_name,
                                  uint64_t since,
                                  uint64_t till,
                                  std::set<std::string>& objects) override;

        virtual void list_objects(const std::string& part_name,
                                  const std::string& class_name,
                                  const std::string& attr_name,
                                  uint64_t since,
                                  uint64_t until,
                                  std::vector<std::string>& objects);

        virtual void read_attribute_re_variant(const std::string& part_name,
                                               const std::string& class_name,
                                               const std::string& attr_name,
                                               const std::string& obj_name_regexp,
                                               uint64_t since,
                                               uint64_t till,
                                               std::map<std::string, series_ptr>& variant_data) override;

    private:
        std::unique_ptr<pbeast::ServerProxy> m_pbeast_serv;
};

}
#endif
