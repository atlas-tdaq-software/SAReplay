# -*- coding: utf-8 -*-
import os
import logging

from django.shortcuts import render, render_to_response
from django.shortcuts import get_object_or_404
from django import forms
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.template import RequestContext
from django.core.urlresolvers import reverse_lazy
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import csrf_exempt

from django.contrib.auth.decorators import login_required

from sacore.models import ReplayExecution, IncludedFile, RunNumber, STATUS_CHOICES
from sareplayweb import settings
from sacore.utils import get_daq_directives_files

from datetime import datetime
from dateutil.parser import parse

import os, glob, gzip, zlib, bz2, json, sqlite3, subprocess

log = logging.getLogger(__name__)

TIMEOUT_CHOICES_LIST = [
    ('1', '1 minute'),
    ('2', '2 minutes'),
    ('5', '5 minutes'),
    ('15', '15 minutes'),
    ('30', '30 minutes'),
    ('60', '60 minutes'),
    ('120', '2 hours'),
    ('240', '4 hours'),
    ('480', '8 hours'),
    ('720', '12 hours'),
    ]

LOG4J_CHOICES_LIST = [
    ('DEBUG', 'DEBUG'),
    ('INFO', 'INFO'),
    ]

DAQ_DIRECTIVES_FILES = get_daq_directives_files()
DAQ_DIRECTIVES_CHOICES = [(i, os.path.basename(x)) for i, x in enumerate(DAQ_DIRECTIVES_FILES)]

class NewExecutionForm(forms.Form):
    title = forms.CharField(max_length=100, initial="")
    run_numbers = forms.CharField(max_length=20000, initial="")
    part_class = forms.CharField(max_length=20000, required=False, initial="")
    part_class_attr = forms.CharField(max_length=20000, required=False, initial="")
    daq_directives = forms.MultipleChoiceField(required=False,
                                               choices=DAQ_DIRECTIVES_CHOICES,
                                               initial=[],
                                               widget=forms.CheckboxSelectMultiple)

@login_required
@csrf_exempt
def index(request):
    if request.method == 'POST':
        form = NewExecutionForm(request.POST, request.FILES)
        if form.is_valid():
            n = ReplayExecution()
            n.title = form.cleaned_data['title']
            n.run_numbers = form.cleaned_data['run_numbers']
            n.osiris_part_class = form.cleaned_data['part_class']
            n.osiris_part_class_attr = ''
            n.daq_directives = json.dumps([DAQ_DIRECTIVES_FILES[int(x)]
                                                for x in form.cleaned_data['daq_directives']])
            n.skip_errors = True
            n.generate_perf_plots = False
            n.timeout = 240
            n.author = request.user.username
            n.log4j_level = 'INFO'
            n.save()

            for idx, fname in enumerate(sorted(request.FILES)):
                f = request.FILES[fname]
                rf = IncludedFile()
                rf.ex = n
                rf.set_filedata(f.read())
                rf.filename = f.name
                rf.kind = 1
                rf.orderpos = 1000 + idx
                rf.save()


            # Start worker process
            x = subprocess.Popen('/opt/sareplayweb/beginwork.sh', shell=True)

            return HttpResponseRedirect(reverse_lazy('home'))
    else:
        ctx = {'DAQ_DIRECTIVES_CHOICES': DAQ_DIRECTIVES_CHOICES}
        return render(request, 'index.html', ctx)


def executions(request):
    exec_list = ReplayExecution.objects.all().order_by('-start_date')
    executions = {}
    executions['data'] = [{
        'id': x.pk,
        'name': x.title,
        'timestamp': x.start_date.strftime("%d.%m.%Y %H:%M"),
        'status': dict(STATUS_CHOICES)[x.status],
        'status_code': x.status,
        'progress': 0,
        'log': '',
        'user': x.author,
        'replay_template': {
            'name': x.title,
            'runNumbers': x.run_numbers,
            'pBeastData': x.osiris_part_class,
            'defaultDirectives': x.daq_directives
        }
    } for x in exec_list] or None
    
    return HttpResponse(json.dumps(executions), content_type="application/json")


def directives(request):
    directives_file = open('/det/tdaq/ShifterAssistant/runtime/oks/daq-directives.data.xml', 'rb')
    response = HttpResponse(content=directives_file)
    response['Content-Type'] = 'application/xml'
    response['Content-Disposition'] = 'attachment; filename="daq-directives.data.xml"'
    return response


def total_seconds(td):
    # Keep backward compatibility with Python 2.6 which doesn't have
    # this method
    if hasattr(td, 'total_seconds'):
        return td.total_seconds()
    else:
        return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6


def runnumber(request, run_id):
    run_number = RunNumber.objects.using('rn').get(runnumber=run_id)
    epoch = datetime.utcfromtimestamp(0)
    start_time = parse(run_number.startat)
    # parse() converts from UTC (returned from RN DB) to local time for rendering in the web page
    # next line is to get Unix seconds from epoch
    from_time = int(total_seconds(start_time - epoch))

    to_time = 0
    # If the duration doesn't exist use the next run's start as this run's end
    if run_number.duration is None:
        nex_run_id = int(run_id) + 1
        next_run_number = RunNumber.objects.using('rn').get(runnumber=nex_run_id)
        start_time = parse(run_number.startat)
        to_time = int(total_seconds(start_time - epoch))
    else:
        to_time = from_time + run_number.duration


    result = {
        'from' : from_time,
        'to' : to_time
    }
    return HttpResponse(json.dumps(result), content_type="application/json")


@login_required
def metricimg(request, ex_id, file_name):
    ex = get_object_or_404(ReplayExecution, pk=ex_id)

    reportspath = os.path.join(ex.output_dir, 'report', '*.*')
    metricsfiles = [os.path.basename(x) for x in glob.glob(reportspath)]

    if file_name in metricsfiles:
        with open(os.path.join(ex.output_dir, 'report', file_name)) as f:
            return HttpResponse(f.read(), mimetype="image/png")
    else:
        raise Http404

@login_required
def alertsdetail(request, ex_id):
    ex = get_object_or_404(ReplayExecution, pk=ex_id)
    alerts = ex.alert_set.all().order_by('timestamp')
    paginator = Paginator(alerts, 200)

    page = request.GET.get('page')
    try:
        paged_alerts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        paged_alerts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        paged_alerts = paginator.page(paginator.num_pages)

    ctx = {'alerts': paged_alerts}
    result = render(request, 'executionalerts.html', ctx)
    return result


@login_required
def executiondetail(request, ex_id):
    ex = get_object_or_404(ReplayExecution, pk=ex_id)
    included_files = ex.includedfile_set.all().order_by('orderpos')
    included_daq_files = ' '.join([os.path.basename(x) for x in json.loads(ex.daq_directives)])

    logfilepath = os.path.join(ex.output_dir, 'log', 'aal.err')
    consolefilepath = os.path.join(ex.output_dir, 'log', 'console.log')

    logdata = None
    consoledata = None

    for x, f in [(logfilepath, open), (logfilepath+'.gz', gzip.open), (logfilepath+'.bz2', bz2.BZ2File)]:
        if os.path.exists(x):
            logdata = f(x, 'r')
            break

    for x, f in [(consolefilepath, open), (consolefilepath+'.gz', gzip.open), (consolefilepath+'.bz2', bz2.BZ2File)]:
        if os.path.exists(x):
            consoledata = f(x, 'r')
            break

    reportspath = os.path.join(ex.output_dir, 'report', '*.*')
    metricsfiles = [os.path.basename(x) for x in glob.glob(reportspath)]

    if ex.extra_output:
        extra_output = json.loads(ex.extra_output)
    else:
        extra_output = {}
    extra_output['was_errors'] = extra_output.get('was_errors', False) or (ex.status == 10)

    ctx = {'ex': ex,
        'logdata': logdata,
        'consoledata': consoledata,
        'metricsfiles': metricsfiles,
        'included_files': included_files,
        'included_daq_files': included_daq_files,
        'extra_output': extra_output}
    result = render(request, 'executiondetail.html', ctx)

    if logdata:
        logdata.close()

    if consoledata:
        consoledata.close()

    return result

def autocompletepartclass(request):
    conn = sqlite3.connect(settings.OSIRIS_DB_PATH, 30)
    suggestinos = {}
    suggestinos['data'] = [(x[0] + ';' + x[1]) for x in conn.execute('SELECT DISTINCT part_name, class_name FROM partition p INNER JOIN class c ON (p.part_id=c.part_id) INNER JOIN attribute a ON (c.class_id = a.class_id)')]
    conn.close()
    return HttpResponse(json.dumps(suggestinos), mimetype="application/javascript")

@login_required
def allexcs(request):
    exec_list = ReplayExecution.objects.all().order_by('-start_date')
    ctx = {'exec_list': exec_list}
    return render(request, 'allexcs.html', ctx)
