# -*- coding: utf-8 -*-

import os
import ldap

from django.conf import settings
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse

from django.contrib.auth.models import User, check_password
from django.contrib.auth import REDIRECT_FIELD_NAME, login as auth_login
from django.contrib.auth.forms import AuthenticationForm


def create_user_if_doesnt_exist(username):
    """Because the users are taken from LDAP/SSO we need to create a local account
    for each of them.
    NOTE: The password field is never used as we check against LDAP if logged in."""

    user = None
    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        user = User(username=username, password='')
        user.is_staff = False
        user.is_superuser = False
        user.save()
    return user


class AuthBackendSSOLDAP(object):
    """This class is used by the auth_login (django.contrib.auth.login).
    It's set as the main authentication backend in the settings file."""

    supports_object_permissions = False
    supports_anonymous_user = False
    supports_inactive_user = False

    def authenticate(self, username=None, password=None):
        # FIXME: Use atlas LDAP
        ldap_conn = ldap.initialize('ldap://atlasvdc.cern.ch')
        ldap_conn.protocol_version = 3
        ldap_conn.set_option(ldap.OPT_REFERRALS, 0)
        full_username = "%s@cern.ch" % username
        try:
            ldap_conn.simple_bind_s(full_username, password)
            return create_user_if_doesnt_exist(username)
        except ldap.INVALID_CREDENTIALS:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


def login(request, template_name='login.html'):
    if request.method == 'GET':
        # Try SSO login when the page is requested
        if 'ADFS_LOGIN' in request.META:
            username = request.META['ADFS_LOGIN']
            create_user_if_doesnt_exist(username)
            auth_login(request, username)
            return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)
        else:
            context = {
                'form': AuthenticationForm(request)
            }
            return TemplateResponse(request, template_name, context)
    else:
        # Try LDAP login
        form = AuthenticationForm(None, data=request.POST)
        if form.is_valid():
            auth_login(request, form.get_user())
            return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)
        else:
            context = {
                'form': form
            }
            return TemplateResponse(request, template_name, context)
