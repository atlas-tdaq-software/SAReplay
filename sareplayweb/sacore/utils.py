# coding: utf-8

import os

from sareplayweb import settings

def read_properties_file(file_name):
    import StringIO
    import os

    config = StringIO.StringIO()
    config.write('[dummysection]\n')
    try:
        with open(file_name) as f:
            config.write(f.read())
    except IOError:
        # No configuration file found, fake data for testing
        config.write('directives.filelist = checklists.xml, ctp.xml, hlt.xml, netalerts.xml, runctrl.xml, common.xml, daqhlt.xml, l1calo.xml, resources.xml, shiftleader.xml')
    config.seek(0, os.SEEK_SET)

    import ConfigParser
    cp = ConfigParser.ConfigParser()
    cp.readfp(config)
    # cp.getint("a", "b")
    # cp.get("sec", "val")
    return cp

def get_daq_directives_files():
    "directives.filelist"
    p = read_properties_file('/opt/sareplayweb/jars/share/lib/cassandra.properties')
    return []
