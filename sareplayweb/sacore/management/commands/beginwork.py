# -*- coding: utf-8 -*-

import collections
import datetime
import fcntl
import glob
import json
import os
import pwd
import pprint
import signal
import socket
import string
import StringIO
import subprocess
import tempfile
import threading
import time
import traceback
import shutil

# XML Parser
import xml.etree.ElementTree as ET

# MatPlotLib
import numpy as np
import matplotlib
matplotlib.use('Cairo')
import matplotlib.pyplot as plt

# Django
from django.core.management.base import BaseCommand
from django.db import IntegrityError
from django.db import transaction

import django.db

# This web app.
import sacore.models
from sareplayweb.settings import DEBUG, JAVA_BIN_PATH, EXECUTIONS_TMP_BASE_DIR, \
                                 OSIRIS_BIN_PATH, DOWNSAMPLER_BIN_PATH, \
                                 LOCAL_CLASSPATH, TDAQ_LIB_DIR, DAQ_DIRECTIVES_BASE_PATH

from sacore.utils import get_daq_directives_files

import logging
log = logging.getLogger(__name__)


VALID_CHARS = set(string.ascii_letters + string.digits + "_")

ERROR_STRINGS = ["ERROR CERNCERN", "RuntimeException", "NullPointerException", "Exception in thread", "[Fatal Error]"]

def sanitize_string(s):
    return ''.join([(x if (x in VALID_CHARS) else '_') for x in s])

def plots_maker(outdir, metrics_file):
    all_data = {}
    first_row = None

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    if not os.path.exists(metrics_file):
        return

    with open(metrics_file) as f:
        for i, line in enumerate(f):
            if i == 0:
                first_row = line
            else:
                StatementName = line.split('\t')[1]
                if StatementName not in all_data:
                    all_data[StatementName] = []
                all_data[StatementName].append(line)

    for StatementName in all_data:
        try:
            directive_data = StringIO.StringIO('\n'.join([first_row] + all_data[StatementName]))

            data = np.genfromtxt(directive_data, delimiter='\t', skip_header=0,
                                 skip_footer=10, names=True)

            #print(data)
            # semilogy
            # plot

            plt.cla()
            plt.clf()

            plt.subplot(2, 1, 1)
            plt.semilogy(data['Timestamp'], data['CpuTime'], color='r', label='the data')
            plt.title(StatementName)
            plt.ylabel('CpuTime')

            plt.subplot(2, 1, 2)
            plt.semilogy(data['Timestamp'], data['NumInput'], color='r', label='the data')
            plt.title(StatementName)
            plt.ylabel('NumInput')

            #plt.show()
            plt.savefig(outdir + "/" + sanitize_string(StatementName) + ".png", dpi=120)
        except Exception, e:
            print "ERROR plots_maker", e

class AsyncRunner(object):
    def __init__(self, command, env, endfunc, terminatefunc):
        self.command = command
        self.env = env
        self.thread = None
        self.proc = None
        self.endfunc = endfunc
        self.terminate_func = terminatefunc
        self.exit_by_timeout = False

    def threadproc(self):
        use_shell = False
        c = self.command

        self.output_line_counter = 0

        if use_shell:
            self.proc = subprocess.Popen(' '.join(c),
                                         bufsize=0,
                                         shell=True,
                                         env=self.env,
                                         stdout=subprocess.PIPE,
                                         stderr=subprocess.STDOUT)
        else:
            self.proc = subprocess.Popen(c,
                                         bufsize=0,
                                         shell=False, # shell should be False for terminate() to work
                                         env=self.env,
                                         stdout=subprocess.PIPE,
                                         stderr=subprocess.STDOUT)

        # for line in self.proc.stdout:
        for line in iter(self.proc.stdout.readline, ''):
            if self.endfunc(line):
                self.terminate()
                break

    def run(self, timeout):
        if True:
            self.thread = threading.Thread(target=self.threadproc)
            self.thread.start()

            if DEBUG:
                print("$$$ started")

            try: #FIXME: Add polling to Task here to see if we need to die.
                self.thread.join(timeout)
            except KeyboardInterrupt:
                print "Ctrl-C, terminating ..."

            if self.thread.is_alive():
                self.exit_by_timeout = True
                self.terminate()
                self.thread.join()
            if DEBUG:
                print("$$$ finish")
        else:
            self.threadproc()

    def terminate(self):
        # http://publib.boulder.ibm.com/infocenter/realtime/v1r0/index.jsp?topic=%2Fcom.ibm.rt.doc.10%2Fuser%2Fsighand.html
        if DEBUG:
            print("$$$ terminated")
        if self.terminate_func:
            self.terminate_func(self.proc)
            self.proc.send_signal(signal.SIGTERM)
            self.proc.terminate()
        else:
            self.proc.send_signal(signal.SIGTERM)
            self.proc.terminate()

class replay_finish_detector:
    def __init__(self, fout, base_dir):
        self.error = False
        self.debug = DEBUG
        self.fout = fout
        self.base_dir = base_dir

        self.exit_by_linecountlimit = False
        self.output_line_counter = 0
        self.linecountlimit = 3000000

        self.exit_by_logfilesizelimit = False
        self.logfilesizelimit = self.linecountlimit * 55

        self.exit_by_replayfinish = False

    def __call__(self, line):
        ret = False

        self.output_line_counter += 1
        if self.output_line_counter >= self.linecountlimit:
            self.exit_by_linecountlimit = True
            ret = True

        logFileName = os.path.join(self.base_dir, 'log', 'aal.err')
        if os.path.exists(logFileName) and os.stat(logFileName).st_size >= self.logfilesizelimit:
            self.exit_by_logfilesizelimit = True
            ret = True

        if self.fout:
            self.fout.write(line)

        if self.debug:
            if False:
                print "!!! - " + datetime.datetime.now().isoformat() + " - " + line

        for x in ERROR_STRINGS:
            if x in line:
                if self.debug and False:
                    print "!!! FOUND ERROR " + x + " AT " + line
                self.error = True

        if "REPLAY FINISH" in line:
            self.exit_by_replayfinish = True
            if self.debug:
                print "Found: REPLAY FINISH. Terminating... "
            ret = True

        return ret

def downsample_metrics(infname, outfname, limit):
    subprocess.check_call([DOWNSAMPLER_BIN_PATH, infname, outfname, str(limit)])


def get_process_output(*args, **kwargs):
    print "Running " + " ".join(*args)
    p = subprocess.Popen(" ".join(*args), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    c = p.communicate()
    return (p.returncode, c[0], c[1])

def oks_download_config(run_number, out_dir):
    run_number = sacore.models.RunNumber.objects.using('rn').get(runnumber=run_number)

    rninfo_config = str(run_number.configschema) + '.' + str(run_number.configdata)
    config = [str(run_number.configschema), str(run_number.configdata)]

    if len(config) != 2:
        return False

    wdir = os.getcwd()
    os.chdir(out_dir)

    data_err_list = []
    try:
        oks_schema_fname = config[0] + ".schema.xml"
        oks_data_fname = run_number.partitionname + "." + rninfo_config + ".data.xml"
        oks_archive_fname = run_number.partitionname + "." + rninfo_config + ".tar.gz"

        if os.path.exists(oks_schema_fname) and os.path.exists(oks_data_fname):
            return True

        ret_code, data, data_err = get_process_output(['/opt/sareplayweb/oks_get_data.sh', '-c', 'oracle://atonr_oks/r',
                                                       '-w', 'atlas_oks_archive', '-s', config[0], '-n', config[1],
						       '-q', 'Partition', "'(all (object-id \"ATLAS\" =))'", '-r', '1000',
                                                       '-m', oks_schema_fname, '-f', oks_data_fname])
        if ret_code != 0:
            raise Exception(data_err)

        subprocess.check_call(['tar', 'cfz', oks_archive_fname, oks_schema_fname, oks_data_fname])
        return True
    finally:
        os.chdir(wdir)

    raise Exception('could not download OKS db backups run_number=' + str(run_number) + " data_err_list=" + str(data_err_list))

def parse_sa_alert_xml(xmldata):
    e = ET.fromstring(xmldata)
    ret = e.attrib
    ret['events'] = [x.attrib for x in e.findall('events/event')]
    return ret

# def parse_sa_alert_text(textdata):
#     pass

def parse_sa_alert_file(fname):
    """
    Function to parse alerts file from Shifter Assistant.
    """
    SA_XML_OPENTAG = "<?xml "
    SA_XML_ENDTAG = "</simplealert>"

    start = 0

    with open(fname, 'r') as f:
        data = f.read()

    while start >= 0 and start < len(data):
        if data.startswith(SA_XML_OPENTAG, start):
            endpos = data.find(SA_XML_ENDTAG, start)
            if endpos == -1:
                break
            try:
                yield (parse_sa_alert_xml(data[start:(endpos + len(SA_XML_ENDTAG))]))
            except Exception, e:
                print "parse_sa_alert_xml", e
                print "data start:", start, " endpos:", endpos
                break
            start = endpos + len(SA_XML_ENDTAG)
        else:
            start = data.find(SA_XML_OPENTAG, start)

@transaction.commit_manually
def parse_sa_alers(d, ex):
    counter = 0
    if DEBUG:
        print "Parsing alerts..."
    for a in glob.glob(os.path.join(d, 'data/*.out')):
        if DEBUG:
            print "Parsing alerts file ", a
        L = parse_sa_alert_file(a)
        for b in L:
            c = sacore.models.Alert()
            c.ex = ex
            try:
                c.timestamp = int(b['timestamp'])
            except KeyError:
                c.timestamp = 0
            c.source = os.path.basename(a)[:-4]
            c.domain = b['domain'].replace(',', ' ')
            c.name = b['name']
            c.message = b['alert']
            c.action = b['action']
            c.severity = b['severity']
            c.details = json.dumps(b)
            c.save()

            # Group alerts updates in a transaction to make it go faster on the database.
            counter += 1
            if counter >= 1000:
                counter = 0
                transaction.commit()

    # Commit the last.
    transaction.commit()

    if DEBUG:
        print "Parsing done"

def remove_duplicates(L):
    """
    Keeps the original ordering
    """
    S = set()
    R = []
    for e in L:
        if e not in S:
            R.append(e)
            S.add(e)
    return R

def run_sa_process(d, timeout, endfunc):

    # Enviroment variables required by SA, copied from the sa_replay script.
    env = dict(os.environ)

    classpath = [d + "/etc"] + [
        LOCAL_CLASSPATH,
        TDAQ_LIB_DIR,
        os.environ['TDAQ_CLASSPATH'],
        os.environ['CLASSPATH']]

    classpath_str = ':'.join(remove_duplicates(':'.join(classpath).split(':')))

    # Compress classpath removing duplicates


    env['TDAQ_APPLICATION_NAME'] = "ShifterAssistant"
    env['AAL_USER_DIR'] = d
    env['AAL_CP'] = classpath_str
    env['TDAQ_ERS_NO_SIGNAL_HANDLERS'] = "1"
    env['TDAQ_IPC_TIMEOUT'] = str(15 * 60 * 1000)
    env['LD_LIBRARY_PATH'] = TDAQ_LIB_DIR + ":" + os.environ['LD_LIBRARY_PATH']

    os.chdir(d)

    #JAVA_BIN_PATH = env['TDAQ_JAVA_HOME'] + "/bin/java"

    # We have to start Java VM directly, since starting the bash scripts won't
    # allow us to kill the process, either by timeout or when the replay finishes.
    # Shifter Assistant does not currently keep a .pid file.
    cmd = [JAVA_BIN_PATH,
           "-Xms1024m",
           "-Xmx2048m",
           "-XX:-OmitStackTraceInFastThrow",
	   "-Djava.security.egd=file:/dev/urandom"
           "-Duser.dir=" + d,
           "-cp", classpath_str,
           "ch.cern.tdaq.cassandra.AALMain",
           ]

    def myterminatefunc(proc):
        print "Terminating PID " + str(proc.pid)
	try:
        	os.killpg(proc.pid, signal.SIGTERM)
	except:
		print "Failed to kill pg"

    run = AsyncRunner(cmd, env, endfunc, myterminatefunc)
    run.run(timeout)

    return {'exit_by_timeout': run.exit_by_timeout}

def parse_run_numbers(S):
    # FIXME
    return json.loads(S)

def run_execution(ex):
    myexdir = os.path.join(EXECUTIONS_TMP_BASE_DIR, pwd.getpwuid(os.getuid())[0])
    OKS_CACHE_DIR = os.path.join(myexdir, 'oks-cache')

    if not os.path.exists(myexdir):
        os.makedirs(myexdir)

    try:
        os.chmod(myexdir, 0o755)
    except:
        pass

    if not os.path.exists(OKS_CACHE_DIR):
        os.makedirs(OKS_CACHE_DIR)

    base_dir = os.path.join(myexdir, datetime.datetime.now().strftime("%Y%m%d"))
    if not os.path.exists(base_dir):
        try:
            os.makedirs(base_dir)
        except:
            pass

    try:
        os.chmod(base_dir, 0o755)
    except:
        pass

    d = tempfile.mkdtemp(prefix=(sanitize_string(ex.title.encode("utf8"))[:10]), dir=base_dir)
    try:
        os.chmod(d, 0o755)
    except:
        pass

    if DEBUG:
        print(d)
    ex.status = 1
    ex.output_dir = d
    ex.save()

    for x in ['etc', 'directives', 'data', 'log']:
        os.makedirs(os.path.join(d, x))

    # OKS cache
    os.symlink(OKS_CACHE_DIR, os.path.join(d, 'cache'))

    if ex.log4j_level:
        sadebuglevel = ex.log4j_level
    else:
        sadebuglevel  = "INFO"

    with open(os.path.join(d, 'etc', 'log4j.xml'), 'w') as f:
        f.write("""<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE log4j:configuration SYSTEM "log4j.dtd">

<log4j:configuration xmlns:log4j="http://jakarta.apache.org/log4j/" debug="false">

   <appender name="terminal" class="org.apache.log4j.ConsoleAppender">
      <param name="Threshold" value="SADEBUGLEVEL"/>
      <param name="Target" value="System.out"/>
      <param name="immediateFlush" value="true"/>
      <layout class="org.apache.log4j.PatternLayout">
         <param name="ConversionPattern" value="%d{DATE} %-5p CERNCERN [%c{1}] %m%n"/>
      </layout>
    </appender>
    <appender name="errors" class="org.apache.log4j.FileAppender">
      <param name="File" value="log/aal.err" />
      <param name="immediateFlush" value="true"/>
      <param name="Append" value="true" />
      <param name="Threshold" value="SADEBUGLEVEL"/>
      <layout class="org.apache.log4j.PatternLayout">
         <param name="ConversionPattern" value="%d{DATE} %-5p [%c{1}] %m%n"/>
      </layout>
    </appender>
   <root>
      <level value="SADEBUGLEVEL"/>
      <appender-ref ref="terminal"/>
      <appender-ref ref="errors"/>
   </root>

</log4j:configuration>""".replace("SADEBUGLEVEL", sadebuglevel))

    with open(os.path.join(d, 'etc', 'esper.xml'), 'w') as f:
        f.write("""<?xml version="1.0" encoding="UTF-8"?>
<esper-configuration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns="http://www.espertech.com/schema/esper"
  xsi:schemaLocation=
    "http://www.espertech.com/schema/esper/esper-configuration-2.0.xsd">

  <!-- Warning: don't change this! -->
  <event-type name="Message" class="daq.EsperUtils.ERSEvent"/>
  <event-type name="ISEvent" class="daq.EsperUtils.ISEvent"/>
  <event-type name="Configuration"
    class="ch.cern.tdaq.cassandra.event.ConfigurationEvent"/>
  <!-- <auto-import import-name="daq.EsperUtils.ISReader"/> -->
  <auto-import import-name="ch.cern.tdaq.cassandra.replay.ISReader"/>
  <auto-import import-name="ch.cern.tdaq.cassandra.reader.ConfigReader"/>
  <auto-import import-name="ch.cern.tdaq.cassandra.config.Utils"/>
  <auto-import import-name="java.lang.Math"/>
  <auto-import import-name="ch.cern.tdaq.cassandra.utils.*"/>
<!--  <auto-import import-name="java.util.*"/> -->


  <engine-settings>
    <defaults>
    <!--
      <threading>
        <listener-dispatch preserve-order="true" timeout-msec="1000" locking="spin"/>
        <threadpool-inbound enabled="true" num-threads="4"/>
        <threadpool-outbound enabled="true" num-threads="4" capacity="1000"/>
      </threading>
      -->

      <metrics-reporting enabled="true" jmx-engine-metrics="true" engine-interval="60000" statement-interval="60000" threading="false"/>
      <exceptionHandling>
      <handlerFactory class="ch.cern.tdaq.cassandra.replay.EsperExceptionHandler"/>
      </exceptionHandling>
    </defaults>
  </engine-settings>
</esper-configuration>    """)

    directives_file_names = []

    path_to_directives = os.path.join(d, 'directives', 'daq-directives.data.xml')
    included_files = ex.includedfile_set.all().order_by('orderpos')
    file_uploaded = False
    for i, incf in enumerate(included_files):
        file_uploaded = True
        with open(path_to_directives, 'w') as f:
            f.write(incf.filedata_uncompressed())
    
    # If nothing was uploaded use default directives file
    if not file_uploaded:
        path_to_directives = DAQ_DIRECTIVES_BASE_PATH + '/directives.data.xml'

    # Copy schema dependency
    shutil.copyfile(
        '/det/tdaq/ShifterAssistant/runtime/directives/aal.schema.xml',
        os.path.join(d, 'directives', 'aal.schema.xml')
    )
    
    with open(os.path.join(d, 'etc', 'cassandra.properties'), 'w') as f:
        f.write("""
directives.writer.mask = Db=File, Ers=null, Email=null 
server=oksconfig:{directives}
esper.configuration=etc/esper.xml
replay.mode=true
replay.osiris.path={osirispath}
replay.osiris.partclass={partclass}
replay.osiris.partclassattr={partclassattr}
""".format(**{'directives': path_to_directives,
              'partclass': ex.osiris_part_class,
              'partclassattr': ex.osiris_part_class_attr,
              'osirispath': OSIRIS_BIN_PATH}))

    run_numbers = parse_run_numbers( ex.run_numbers )

    with open(OKS_CACHE_DIR + "/lockfile", "w") as okslockfile:
        fcntl.lockf(okslockfile, fcntl.LOCK_EX)
        try:
            for r in run_numbers:
                rn = r['rn']
                try:
                    oks_download_config(int(rn), OKS_CACHE_DIR)
                except Exception, e:
                    print "error on oks_download_config: ", e
		    return
        finally:
            fcntl.lockf(okslockfile, fcntl.LOCK_UN)

    execution_timeout = ex.timeout * 60

    consolefoutpath = os.path.join(d, 'log', 'console.log')
    with open(consolefoutpath, 'w', 0) as consolefout:
       # if not ex.skip_errors:
       #     ex.status = 2
       #     ex.save()
#
#            eordetect = replay_finish_detector(consolefout, d)
#
#            # First we start SA without data.
#            run_ret = run_sa_process(d, execution_timeout, eordetect)
#
#            # An error was detected, abort the actual replay
#            if eordetect.error:
#                ex.status = 10
#                ex.save()
#                return

        ex.status = 3
        ex.save()

        eordetect = replay_finish_detector(consolefout, d)

        # Then we start SA with runs.

        with open(os.path.join(d, 'etc', 'cassandra.properties'), 'a+') as f:
            f.seek(0, os.SEEK_END)
            for i, r in enumerate(run_numbers, 1):
                rn = r['rn']

                f.write("replay.run{0}.runnumber={1}\n".format(i, rn))

                if 'rnref' in r:
                    f.write("replay.run{0}.refrn={1}\n".format(i, r['rnref']))

                if 'since' in r:
                    f.write("replay.run{0}.since={1}\n".format(i, r.get('since', '')))
                    f.write("replay.run{0}.till={1}\n".format(i, r.get('till', '')))

        run_ret = run_sa_process(d, execution_timeout, eordetect)

    extra_output = {
        'was_errors': eordetect.error,
        'exit_by_linecountlimit': eordetect.exit_by_linecountlimit,
        'exit_by_logfilesizelimit': eordetect.exit_by_logfilesizelimit,
        'exit_by_replayfinish': eordetect.exit_by_replayfinish,
        'exit_by_timeout': run_ret['exit_by_timeout']
        }

    ex.extra_output = json.dumps(extra_output)
    ex.status = 4
    ex.save()

    # Parse alerts
    parse_sa_alers(d, ex)

    # Generate performance metrics plots
    metrics_stmt_fname = os.path.join(d, 'data', 'metricsStatements.txt')
    if ex.generate_perf_plots:
        if os.path.exists(metrics_stmt_fname):
            downsample_metrics(metrics_stmt_fname, metrics_stmt_fname + ".down", 1000)
            plots_maker(os.path.join(d, 'report'), metrics_stmt_fname + ".down")

    if DEBUG:
        print ("%%% plots finished")

    logfilepath = os.path.join(d, 'log', 'aal.err')
    consolefilepath = os.path.join(d, 'log', 'console.log')

    # Compress log files
    for x in [metrics_stmt_fname, metrics_stmt_fname + ".down", logfilepath, consolefilepath]:
        if os.path.exists(x):
            subprocess.check_call(['gzip', x])

    # Compress alerts files
    for x in glob.glob(os.path.join(d, 'data/*.*')):
        if (not x.endswith(".gz")) and os.stat(x).st_size > (256 * 1024):
            subprocess.check_call(['gzip', x])

    if eordetect.error:
        ex.status = 10
    else:
        ex.status = 5
    ex.save()

class Command(BaseCommand):
    args = ''
    help = ''

    # @transaction.commit_manually
    def handle(self, *args, **options):
        if DEBUG:
            print "STARTED"
        # Once every minute the worker dies and the service restarts it.
        # This is a horrible hack so we don't need to kill the workers
        # when the service is restarted.
        for _unused in range(12):
            exec_list = sacore.models.ReplayExecution.objects.filter(
                        status=0).order_by('start_date')[:1]

            for x in exec_list:
                # FIXME: Poor man's concurrency lock. the current_ex column is unique.
                try:
                    this_task = sacore.models.Task()
                    this_task.worker_pid = os.getpid()
                    this_task.worker_hostname = socket.gethostname()
                    this_task.current_ex = x
                    this_task.status = 1
                    this_task.save()
                except IntegrityError:
                    try:
                        django.db.connection.close()
                    except Exception, e:
                        print "ERROR on django.db.connection.close()", e
                    break

                if DEBUG:
                    print(x)

                try:
                    run_execution(x)
                    if DEBUG:
                        print ("$$$ COMMIT ")
                except Exception, e:
                    x.status = 99
                    print "ERROR: ", e
                    print traceback.format_exc()

                x.save()

                this_task.status = 2
                this_task.save()

                # Restart the DB connection after each execution.
                try:
                    django.db.connection.close()
                except Exception, e:
                    print "ERROR on django.db.connection.close()", e
                return
            try:
                time.sleep(5)
            except KeyboardInterrupt:
                if DEBUG:
                    print "STOPPING"
                return
