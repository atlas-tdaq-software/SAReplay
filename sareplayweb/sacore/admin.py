import sacore.models
from django.contrib import admin

admin.site.register(sacore.models.ReplayExecution)
admin.site.register(sacore.models.Task)
