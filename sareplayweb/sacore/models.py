# -*- coding: utf-8 -*-

from django.db import models

STATUS_CHOICES = [
                  (0, u'Waiting'),
                  (1, u'Init env'),
                  (2, u'Syntax checking'),
                  (3, u'Running'),
                  (4, u'Generating plots'),
                  (5, u'Finished'),
                  (10, u'Finished - With errors'),
                  (99, u'Runner CRASH!'),
                  ]

WORKER_STATUS_CHOICES = [
                         (0, u'Waiting new task'),
                         (1, u'Running task'),
                         (2, u'Terminated'),
                         (10, u'Stopping current task'),
                         ]

FILE_KIND_CHOICES = [
                     (0, "DAQ Directive"),
                     (1, "User Directive"),]

class ReplayExecution(models.Model):
    title = models.CharField(max_length=300)
    start_date = models.DateTimeField('date published', auto_now=True)
    run_numbers = models.CharField(max_length=20000)
    osiris_part_class = models.CharField(max_length=20000, default='')
    osiris_part_class_attr = models.CharField(max_length=20000, default='')
    status = models.IntegerField(default=0, choices=STATUS_CHOICES)
    output_dir = models.CharField(max_length=300, default='')
    skip_errors = models.BooleanField()
    daq_directives = models.CharField(max_length=20000)
    generate_perf_plots = models.BooleanField()
    timeout = models.IntegerField()
    author = models.CharField(max_length=300)
    log4j_level = models.CharField(max_length=300)
    extra_output = models.TextField(null=False, default='')
    
    def __unicode__(self):
        return "ReplayExecution title=" + self.title + " start_date=" + str(self.start_date) + " status=" + str(self.status)

class IncludedFile(models.Model):
    ex = models.ForeignKey('ReplayExecution', null=False)
    filedata = models.TextField(null=False)
    filename = models.TextField(null=False)
    kind = models.IntegerField(null=False, choices=FILE_KIND_CHOICES)
    compression = models.CharField(max_length=300, null=True, default=None)
    orderpos = models.IntegerField(null=False, default=10)

    def __unicode__(self):
        return "IncludedFile filename={0} kind={1} iscompressed={2} orderpos={3}".format(self.filename, 
            str(self.get_kind_display()), str(self.iscompressed), str(self.orderpos))

    def filedata_uncompressed(self):
        if self.compression is not None:
            if self.compression == 'zlib':
                import zlib
                return zlib.decompress(self.filedata.decode("base64"))
            return None
        else:
            return self.filedata.decode("base64")

    def set_filedata(self, data):
        import zlib
        self.filedata = zlib.compress(data).encode("base64")
        self.compression = 'zlib'

class Alert(models.Model):
    ex = models.ForeignKey('ReplayExecution', null=False)
    source = models.TextField()
    domain = models.TextField()
    timestamp = models.IntegerField()
    name = models.TextField()
    message = models.TextField()
    action = models.TextField()
    severity = models.TextField()
    details = models.TextField()

    def __unicode__(self):
        return "Alert " + repr({'timestamp': self.timestamp, 'name': self.name, 
                                'message': self.message, 'action': self.action, 
                                'severity': self.severity, 'details': self.details})

    def details_parsed(self):
        import json 
        return json.loads(self.details)

    def timestamp_parsed(self):
        import datetime
        return datetime.datetime.fromtimestamp(self.timestamp / 1e3)
    
class Task(models.Model):
    """
    Identify a running task
    """
    worker_pid = models.IntegerField(null=False, default=0)
    worker_hostname = models.CharField(null=False, max_length=300)
    current_ex = models.ForeignKey('ReplayExecution', null=False, unique=True)
    status = models.IntegerField(null=False, default=0, choices=WORKER_STATUS_CHOICES)


class RunNumber(models.Model):
    """
    RunNumber
    """
    class Meta:
        managed = False        
        db_table = '"atlas_run_number"."runnumber"'

    name = models.CharField(null=False, max_length=16)
    runnumber = models.IntegerField(null=False, primary_key=True)
    startat = models.CharField(null=False, max_length=24)
    duration = models.IntegerField()
    createdby = models.CharField(null=False, max_length=16)
    host = models.CharField(null=False, max_length=256)
    partitionname = models.CharField(null=False, max_length=256)
    configschema = models.IntegerField()
    configdata = models.IntegerField()
    comments = models.CharField(null=False, max_length=2000)
