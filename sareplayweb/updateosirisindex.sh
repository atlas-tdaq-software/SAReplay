#!/bin/bash

# Cron job to update database of the Shifter Assistant Replay Web Interface.
# https://twiki.cern.ch/twiki/bin/view/Atlas/SaReplayProject

# This file is installed by the sareplayweb rpm package

# Responsible: Andrei Kazarov <Andrei.Kazarov@cern.ch>, atlas-tdaq-cc-wg@cern.ch

source /opt/sareplayweb/sourcethetdaqrelease.sh > /dev/null

SAREPLAY_OSIRIS_INDEX="/data/sareplayweb/osirisindex.sqlite"
LOG="/var/log/sareplayworker/updateosiris"

test_corba_server -p initial -c "pbeast/server" -n "pbeast-server" > /dev/null 2>&1
PBEAST_IS_UP=$?

if [ ${PBEAST_IS_UP} -eq 0 ];then
  sareplay_osiris --update-index basic -i $SAREPLAY_OSIRIS_INDEX > $LOG 2>&1
fi
