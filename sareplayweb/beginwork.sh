#!/bin/bash

source /opt/sareplayweb/sourcethetdaqrelease.sh

export MPLCONFIGDIR=/tmp

PYTHONBIN="/usr/bin/python"
LOG="/var/log/sareplayworker/beginwork"

$PYTHONBIN /opt/sareplayweb/manage.py beginwork >> $LOG 2>&1
