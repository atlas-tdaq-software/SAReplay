#!/bin/bash

# Source function library.
. /etc/init.d/functions

prog="sareplayworker"
LOCKFILE="/var/lock/subsys/$prog"

PYTHONBIN="/usr/bin/python"
SATEST_DIR="/opt/sareplayweb"

SATEST_WORKER_SERVICE="$SATEST_DIR/workerservice.py"
SATEST_WORKER_PID="/var/run/sareplayworker.pid"

start() {
        echo -n "Starting $prog worker: "
        daemon $SATEST_WORKER_SERVICE
        RETVAL=$?
        [ $RETVAL -eq 0 ] && touch $LOCKFILE
        echo

        return $RETVAL
}

stop() {
        echo -n "Shutting down $prog worker: "
        killproc -p $SATEST_WORKER_PID $prog
        RETVAL=$?
        [ $RETVAL -eq 0 ] && rm -f $LOCKFILE
        echo

        return $RETVAL
}

status() {
        echo -n "Checking $prog worker status: "
        status -p $SATEST_WORKER_PID $prog
        RETVAL=$?

        return $RETVAL
}

case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    status)
        status
        ;;
    restart)
        stop
        sleep 1
        start
        ;;
    *)
        echo "Usage: $prog {start|stop|status|restart}"
        exit 1
        ;;
esac
