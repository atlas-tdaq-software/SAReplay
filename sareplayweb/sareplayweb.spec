Name: sareplayweb
Version: 1.10
Release: 2
Summary: Shifter Assistant Replay Web interface, release tdaq-09-03-00

License:	CERN
URL:		http://www.cern.ch
Packager: 	Andrei Kazarov <Andrei.Kazarov@cern.ch>

BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildArch:	noarch

Requires:	httpd, python2-django16, python-ldap, java-1.8.0-openjdk, numpy, scipy, python-matplotlib, pygtk2, mod_wsgi, libxml2-python

Requires(post): policycoreutils-python
Requires(postun): policycoreutils-python

%description
SAReplay web interface allows you to test SA directives. Built against tdaq-09-02-01.

%prep

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}/opt/sareplayweb/sacore/
mkdir -p %{buildroot}/opt/sareplayweb/sareplayweb/
mkdir -p %{buildroot}/opt/sareplayweb/static/
mkdir -p %{buildroot}/opt/sareplayweb/templates/
mkdir -p %{buildroot}/opt/sareplayweb/jars/share/lib/

rsync -a --filter=":- .gitignore" %{_sourcedir}/sacore/ %{buildroot}/opt/sareplayweb/sacore/
rsync -a --filter=":- .gitignore" %{_sourcedir}/sareplayweb/ %{buildroot}/opt/sareplayweb/sareplayweb/
rsync -a --filter=":- .gitignore" %{_sourcedir}/static/ %{buildroot}/opt/sareplayweb/static/
rsync -a --filter=":- .gitignore" %{_sourcedir}/templates/ %{buildroot}/opt/sareplayweb/templates/
rsync -a --filter=":- .gitignore" %{_sourcedir}/jars/share/lib/ %{buildroot}/opt/sareplayweb/jars/share/lib/

# Copy everything from the root folder
cp %{_sourcedir}/beginwork.sh %{buildroot}/opt/sareplayweb/
cp %{_sourcedir}/manage.py %{buildroot}/opt/sareplayweb/
cp %{_sourcedir}/sourcethetdaqrelease.sh %{buildroot}/opt/sareplayweb/
cp %{_sourcedir}/oks_get_data.sh %{buildroot}/opt/sareplayweb/
cp %{_sourcedir}/workerservice.py %{buildroot}/opt/sareplayweb/

mkdir -p %{buildroot}/etc/httpd/conf.d/
cp %{_sourcedir}/apache/sareplay.conf %{buildroot}/etc/httpd/conf.d/

# Update the database cache every hour
mkdir -p %{buildroot}/etc/cron.scripts %{buildroot}/etc/cron.d
cp %{_sourcedir}/updateosirisindex.sh %{buildroot}/etc/cron.scripts/
cp %{_sourcedir}/updateosirisindex %{buildroot}/etc/cron.d/

# Create data folders
mkdir -p %{buildroot}/data/sareplayweb/tmp/
mkdir -p %{buildroot}/var/log/sareplayworker/
mkdir -p %{buildroot}/var/log/sareplay/

%clean
rm -rf %{buildroot}

%files
# Make all files owned by apache
%defattr(755,apache,apache,755)
%attr(644,apache,apache) /etc/cron.d/updateosirisindex
/opt/sareplayweb
/etc/httpd/conf.d/sareplay.conf
/etc/cron.scripts/updateosirisindex.sh
/data/sareplayweb/
/var/log/sareplayworker/
/var/log/sareplay/

%post
# CMTCONFIG & TDAQ_INST_PATH need to be set for manage.py to work, but doesn't need to be correct yet
CMTCONFIG="" TDAQ_INST_PATH="" /opt/sareplayweb/manage.py syncdb --noinput
# After creating a log file allow apache to access it
chown apache:apache /var/log/sareplay/debug.log
# After creating the database allow apache to access it
chown apache:apache /data/sareplayweb/sareplay.sqlite
service httpd restart
# Fix selinux context
semanage fcontext -a -t httpd_sys_content_t '/data/sareplayweb(/.*)?' 2>/dev/null || :
restorecon -R /data/sareplayweb || :

%postun
if [ $1 -eq 0 ] ; then  # final removal
semanage fcontext -d -t httpd_sys_content_t '/data/sareplayweb(/.*)?' 2>/dev/null || :
fi
