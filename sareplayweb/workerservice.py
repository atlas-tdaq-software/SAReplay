#!/usr/bin/python
# coding: utf-8


"""
service program for running sareplayweb worker on the background
author: asantos

this program can be:
  - run as root, the program dropts its privileges.
  - run as a normal user with the --no-daemon switch.
"""

import os, subprocess, threading

BASEPATH="/opt/sareplayweb"
SHELL="/bin/bash"
NUMTHREADS=2 # Number of WORKERS
OSIRIS_INTERVAL=6 * 60 * 60 # Seconds.

RUN=True

def threadproc():
    global RUN

    while RUN:
        p = subprocess.Popen(
            [SHELL, os.path.join(BASEPATH, 'beginwork.sh')],
            shell=False,
            cwd=BASEPATH,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT)

        p.wait()


def main():
    global RUN

    try:
        while RUN:
            # Run the background workers
            threads = [threading.Thread(target=threadproc) for i in range(NUMTHREADS)]
            for t in threads:
                t.start()
            for t in threads:
                t.join()
    except KeyboardInterrupt:
        RUN=False

if __name__ == '__main__':
    main()
