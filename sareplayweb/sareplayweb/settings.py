# -*- coding: utf-8 -*-
# Django settings for sareplayweb project.

# This is a workaround for a bug that happens when you mix rhel6 and epel6
# repositories (https://bugzilla.redhat.com/show_bug.cgi?id=882016)
import sys
sys.path.insert(0, '/usr/lib64/python2.7/site-packages')

import os, glob, socket, libxml2

from django.core.urlresolvers import reverse_lazy

# SA CONFIGURATION

TDAQ_PLATFORM_VER = "x86_64-centos7-gcc11-opt"

AUTHENTICATION_FILE = "/atlas/db/.adm/coral/authentication.xml"
AUTHENTICATION_ID = "oracle://atonr/rn_r"

# sareplayweb base dir
PROJECT_BASE_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

JAVA_BIN_PATH = "java"

DAQ_DIRECTIVES_BASE_PATH = "/det/tdaq/ShifterAssistant/runtime/directives/"

DATA_BASE_DIR = "/data/sareplayweb"
OSIRIS_DB_PATH = os.path.join(DATA_BASE_DIR, 'osirisindex.sqlite')
WEBAPP_DATABASE_PATH = os.path.join(DATA_BASE_DIR, 'sareplay.sqlite')
EXECUTIONS_TMP_BASE_DIR = os.path.join(DATA_BASE_DIR, 'tmp')

# it is not longer available in a wsgi process, need to hardcode it
#if not 'TDAQ_INST_PATH' in os.environ:
#    raise Exception("Environment variable TDAQ_INST_PATH not found")
TDAQ_INST_PATH = "/sw/atlas/tdaq/tdaq-09-04-00/installed"

TDAQ_LIB_DIR = os.path.join(TDAQ_INST_PATH, TDAQ_PLATFORM_VER, "lib")
OSIRIS_BIN_PATH = os.path.join(TDAQ_INST_PATH, TDAQ_PLATFORM_VER, 'bin', 'sareplay_osiris')
DOWNSAMPLER_BIN_PATH = os.path.join(TDAQ_INST_PATH, TDAQ_PLATFORM_VER, 'bin', 'sareplay_downsamplemetrics')

local_jars = '/opt/sareplayweb/jars/share/lib/'

LOCAL_CLASSPATH = ":".join([
#     local_jars + "esperUtils.jar",
      local_jars + "cassandra.jar",
#     local_jars + "osirisjext.jar",
      local_jars + "sareplay.jar"
])


# DJANGO CONFIGURATION

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Andrei Kazarov', 'Andrei.Kazarov@cern.ch'),
)

MANAGERS = ADMINS

# Parse database authentication if in P1
if os.path.isfile(AUTHENTICATION_FILE):
    auth_doc = libxml2.parseFile(AUTHENTICATION_FILE)
    auth_ctxt = auth_doc.xpathNewContext()
    rn_db_user = auth_ctxt.xpathEval("/connectionlist/connection[@name='" + AUTHENTICATION_ID +
                                     "']/parameter[@name='user']/@value")[0].content
    rn_db_password = auth_ctxt.xpathEval("/connectionlist/connection[@name='" + AUTHENTICATION_ID +
                                     "']/parameter[@name='password']/@value")[0].content
else:
    # If outside of P1 we are not able to connect to the database
    rn_db_user = None
    rn_db_password = None

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME':    WEBAPP_DATABASE_PATH, # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    },
    'rn': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'atonr',
        'USER': rn_db_user,
        'PASSWORD': rn_db_password,
        'HOST': '',
        'PORT': '', 
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.4/ref/settings/#allowed-hosts
# See also https://docs.djangoproject.com/en/3.2/releases/1.5/
# this is now a required setting

ALLOWED_HOSTS = ['.cern.ch','localhost','vm-tdq-sar-01.cern.ch']

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Zurich'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

LOGIN_URL = reverse_lazy('login')

LOGIN_REDIRECT_URL = reverse_lazy('home')

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = PROJECT_BASE_DIR + '/media'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/tdaq/sar/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    PROJECT_BASE_DIR + "/static",
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder'
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'c_w#96227)p^#5#i+#y=p(s@(0q711_hr%!8-$vb0@21rmo8%e'

SESSION_COOKIE_AGE = 60 * 60 * 24

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader'
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'sareplayweb.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'sareplayweb.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    PROJECT_BASE_DIR + "/templates",
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    'django.contrib.admindocs',
    'sacore',
)

AUTHENTICATION_BACKENDS = (
    'sacore.auth.AuthBackendSSOLDAP',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
         'file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': '/var/log/sareplay/debug.log',
        },
    },
    'loggers': {
        '': {
            'handlers': ['file'],
            'level': 'INFO',
            'propagate': True,
        }
    }
}
