# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.core.urlresolvers import reverse_lazy
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'sacore.views.index', name='home'),

    url(r'^execution/(?P<ex_id>\d+)/$', 'sacore.views.executiondetail', name='executiondetail'),

    url(r'^executions/$', 'sacore.views.executions', name='executions'),

    url(r'directives/$', 'sacore.views.directives', name='directives'),

    url(r'^alerts/(?P<ex_id>\d+)/$', 'sacore.views.alertsdetail', name='alerts'),

    url(r'^runnumber/(?P<run_id>\d+)/$', 'sacore.views.runnumber', name='runnumber'),

    url(r'^allexcs/$', 'sacore.views.allexcs', name='allexcs'),

    url(r'^login/$', 'sacore.auth.login', {'template_name': 'login.html'}, name='login'),

    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': reverse_lazy('home')}, name='logout'),

    url(r'^metricimg/(?P<ex_id>\d+)/(?P<file_name>.+)/$', 'sacore.views.metricimg', name='metricimg'),

    url(r'^autocompletepartclass/$', 'sacore.views.autocompletepartclass', name='autocompletepartclass'),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
