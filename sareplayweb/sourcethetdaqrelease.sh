#!/bin/bash

# Source the release
source /det/tdaq/scripts/setup_TDAQ.sh

# Give CORBA some space
export TDAQ_IPC_TIMEOUT=900000

# Do not use ERS handlers. This is required for Java ERS bindings.
export TDAQ_ERS_NO_SIGNAL_HANDLERS=1

# Python libraries required are not available from TDAQ Python.
PYTHONBIN="/usr/bin/python"
