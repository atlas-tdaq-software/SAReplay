
import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';

import replayApp from './reducers';
import Exections from './components/executions'


const store = createStore(
  replayApp,
  applyMiddleware(
    thunkMiddleware // lets us dispatch() functions
  )
);

export default class App extends React.Component {
    render () {
        return  <Provider store={store}>
            <Exections />
        </Provider>
    }
};
