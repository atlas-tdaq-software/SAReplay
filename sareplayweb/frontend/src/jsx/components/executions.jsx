import React from 'react';
import { connect } from 'react-redux';
import ReactSlider from 'react-slider';
import ReactTags from 'react-tag-autocomplete';

import { changeExecutionsSearch,
         fetchExecutions,
         replayExecution,
         showNewExecutionModal,
         cancelNewExecutionModal,
         newExecutionNameChanged,
         newExecutionFetchRunNumber,
         newExecutionRemoveRunNumber,
         newExecutionChangeRunNumber,
         fetchPBeastSuggestions,
         newExecutionAddPBeastData,
         newExecutionRemovePBeastData } from '../actions';


/* New execution modal */

class NewExecution extends React.Component {
  constructor(props) {
    super();
    this.state = {
      newRunNumber: ''
    }
  }
  submit(e) {
    this._form.submit()
  }
  cancel(e) {
    this.props.dispatch(cancelNewExecutionModal())
  }
  nameChanged(e) {
    this.props.dispatch(newExecutionNameChanged(e.target.value));
  }
  newRunNumberChanged(e) {
    this.setState({newRunNumber: e.target.value})
  }
  addRunNumber(e){
    if(this.state.newRunNumber) {
      this.props.dispatch(newExecutionFetchRunNumber(this.state.newRunNumber));
      this.setState({newRunNumber: ''});
    }
  }
  removeRunNumber(runNumber){
    return (e) => {
      this.props.dispatch(newExecutionRemoveRunNumber(runNumber))
    }

  }
  sliderMoved(runNumber){
    return (newRange) => {
      this.props.dispatch(newExecutionChangeRunNumber(runNumber, newRange[0], newRange[1]))
    }
  }
  addPBeastData(tag) {
    this.props.dispatch(newExecutionAddPBeastData(tag))
  }
  removePBeastData(id) {
    this.props.dispatch(newExecutionRemovePBeastData(id))
  }
  render() {
    const style = {}
    if (this.props.showModal == false) {
      style['display'] = 'none';
    }
    return <div style={style} className="new-execution-popup">
        <h2>New execution</h2>
        <br />
        <form action="" method="POST" encType="multipart/form-data"
              ref={(c) => this._form = c}>
          <label>Name</label>
          <input value={this.props.data.name}
                 onChange={this.nameChanged.bind(this)}
                 className="form-control"
                 name="title"
                 readOnly={this.props.data.nameDisabled} />
          <br />
          <label>Run number</label>
          <input value={this.state.newRunNumber}
                 onChange={this.newRunNumberChanged.bind(this)}
                 className="form-control run-number-field" />
          <button type="button" className="btn-add-run-number" onClick={this.addRunNumber.bind(this)}>
            Add
          </button>

          <br />
          { this.props.data.runNumbers.map( (data, runNumber) => {
              return (
                <div className="slider-container" key={runNumber}>
                  <div className="slider-run-number">
                    {runNumber}&nbsp;&nbsp;
                    <a className="close-button" href="#"
                      onClick={this.removeRunNumber(runNumber).bind(this)}>x</a>
                  </div>
                  <ReactSlider min={data.from.getTime()}
                              max={data.to.getTime()}
                              step={1000}
                              value={[data.fromSelected.getTime(),data.toSelected.getTime()]}
                              withBars={true}
                              onChange={this.sliderMoved(runNumber).bind(this)}>
                    <div className="handle-label arrow-box">
                        {data.fromSelected.toLocaleDateString('de-CH')}&nbsp;
                        {data.fromSelected.toLocaleTimeString('de-CH').substring(0,5)}
                    </div>
                    <div className="handle-label arrow-box">
                        {data.toSelected.toLocaleDateString('de-CH')}&nbsp;
                        {data.toSelected.toLocaleTimeString('de-CH').substring(0,5)}
                    </div>
                  </ReactSlider>
                </div>
              )
          }).toArray()
          }

          <input type="hidden" name="run_numbers" value={
            JSON.stringify(
              this.props.data.runNumbers.map( (data, runNumber) => {
              return {
                rn: runNumber,
                /* Since & till mark what data should be used for the run.
                   From & to describe when the run starts and ends. */
                since: data.fromSelected.toISOString().split('.')[0].replace('T',' '),
                till: data.toSelected.toISOString().split('.')[0].replace('T',' '),
                from: data.from.toISOString().split('.')[0].replace('T',' '),
                to: data.to.toISOString().split('.')[0].replace('T',' ')}}
              ).toArray())
          } />
          <br />
          <label>P-BEAST data</label>
          <br />
          <ReactTags tags={this.props.data.pBeastData.toArray()}
                     suggestions={this.props.data.pBeastSuggestions}
                     maxSuggestionsLength={10}
                     handleAddition={this.addPBeastData.bind(this)}
                     handleDelete={this.removePBeastData.bind(this)}
                     placeholder="Add new class"
                     autoresize={false} />
          <input type="hidden" name="part_class" value={
            this.props.data.pBeastData.map( x => x.name ).toArray().toString()
          } />
          <br />
          <a href="directives"><label>Get directives</label></a>
          <br />
          <label>New directives:</label>
          <br />
          <input type="file" name="daq_directives_dev" multiple />
          <br />
          <div>
            <a className="new-execution-button-submit" onClick={this.submit.bind(this)}> SUBMIT </a>
            <a className="new-execution-button-cancel" onClick={this.cancel.bind(this)}> CANCEL </a>
          </div>
        </form>
    </div>
  }
}

function mapStateToPropsNewExecution(state) {
  return {
    showModal: state.newExecution.showModal,
    data: state.newExecution.data,
  }
}

const NewExecutionRedux = connect(mapStateToPropsNewExecution)(NewExecution);


/* Execution list */

class Execution extends React.Component {
  render() {
    const previous_url = `${STATIC_URL}img/previous.png`;
    let statusColor = {color: '#000'}
    if (this.props.execution.status_code < 3) {
      statusColor = {color: '#CCCC23'}
    }
    if (this.props.execution.status_code >= 3 &&
        this.props.execution.status_code < 10) {
      statusColor = {color: '#15BF3D'}
    }
    if (this.props.execution.status_code >= 10) {
      statusColor = {color: '#b30000'}
    }
    return <tbody className="execution">
      <tr className="content">
        <td className="executions-name">
          <a href={`execution/${this.props.execution.id}`}>
            {this.props.execution.name}
          </a>
        </td>
        <td>{this.props.execution.timestamp}</td>
        <td style={statusColor}>{this.props.execution.status}</td>
        <td>{this.props.execution.progress}%</td>
        <td>
          <a href="#" onClick={this.props.replay}>
            <img src={previous_url} height="20" />
          </a>
        </td>
      </tr>

      <tr className="log">
        <td colSpan="5" className="executions-log">{this.props.execution.log}</td>
      </tr>
    </tbody>
  }
}

class Executions extends React.Component {
  constructor(props) {
    super()
    // Get all the executions when the component loads
    props.dispatch(fetchExecutions())
    // Get autocomplete suggestions for PBeast data
    props.dispatch(fetchPBeastSuggestions())
  }
  replay(template) {
    return (e) => {
      this.props.dispatch(replayExecution(template))
    }
  }
  showModal(e) {
    this.props.dispatch(showNewExecutionModal())
  }
  changeExecutionsSearch(e) {
    this.props.dispatch(changeExecutionsSearch(e.target.value));
  }
  render() {
    const magnifier_url = `${STATIC_URL}img/magnifier.png`;
    const search_style = {background: `url(${magnifier_url}) no-repeat 6px 6px`,
                          backgroundSize: '20px 20px'};

    // Filter executions based on search
    const executionsSearch = this.props.executionsSearch.toLowerCase()
    const filtered_executions = this.props.executions.filter( execution => {
      if (executionsSearch !== '' &&
          execution.name.toLowerCase().indexOf(executionsSearch) == -1 &&
          execution.timestamp.toLowerCase().indexOf(executionsSearch) == -1 &&
          execution.log.toLowerCase().indexOf(executionsSearch) == -1) {
        return false;
      }
      return true;
    });

    // TODO: Group executions by name and add metainformation about grouping

    return <div>
      <NewExecutionRedux />
      <br />
      <a className="new-execution-button" onClick={this.showModal.bind(this)}>
        <span>&#43;</span>NEW REPLAY
      </a>
      <div className="input-group executions-search-wrapper">
        <input style={search_style} className="form-control executions-search-input"
               placeholder="Search for replay"
               value={this.props.executionsSearch}
               onChange={this.changeExecutionsSearch.bind(this)} />
      </div>

      <div className="executions-list">
        <table className="executions-table">
          <tbody>
            <tr>
              <th>NAME</th>
              <th>TIMESTAMP</th>
              <th>STATUS</th>
              <th>PROGRESS</th>
              <th></th>
            </tr>
          </tbody>

          { filtered_executions.map( execution =>
            <Execution key={execution.id}
                       execution={execution}
                       replay={this.replay(execution.replay_template).bind(this)} /> ) }
        </table>
      </div>
    </div>;
  }
};


function mapStateToPropsExecutions(state) {
  return {
    executionsSearch: state.executionsSearch,
    executions: state.executions,
  }
}

export default connect(mapStateToPropsExecutions)(Executions);
