import	fetch	from	'isomorphic-fetch'

/* Execution list */

export const changeExecutionsSearch = (text) => ({
  type: 'CHANGE_EXECUTIONS_SEARCH',
  text
});

const requestExecutions = () => ({
  type: 'REQUEST_EXECUTIONS'
});

const receiveExecutions = (json) => ({
    type: 'RECEIVE_EXECUTIONS',
    executions: json.data
});

export const fetchExecutions = () => {
  return (dispatch) => {
    // Signal that the request is starting
    dispatch(requestExecutions());
    return fetch('executions')
      .then(response => response.json())
      .then(json =>
        dispatch(receiveExecutions(json))
      )
  }
}

export const replayExecution = (template) => {
   return (dispatch) => {
     dispatch(fillInNewExecutionTemplate(template));
     dispatch(showNewExecutionModal());
   }
};

/* New execution modal */

const fillInNewExecutionTemplate = (template) => ({
  type: 'FILL_IN_NEW_EXECUTION_TEMPLATE',
  template
}); 

export const showNewExecutionModal = () => ({
  type: 'SHOW_NEW_EXECUTION_MODAL'
});


const hideNewExecutionModal = () => ({
  type: 'HIDE_NEW_EXECUTION_MODAL'
});

const resetNewExecutionModal = () => ({
  type: 'RESET_NEW_EXECUTION_MODAL'
});

export const cancelNewExecutionModal = () => {
   return (dispatch) => {
     dispatch(hideNewExecutionModal());
     dispatch(resetNewExecutionModal());
   }
};

export const newExecutionNameChanged = (name) => ({
  type: 'NEW_EXECUTION_NAME_CHANGED',
  name
})

const newExecutionRequestRunNumber = () => ({
  type: 'NEW_EXECUTION_REQUEST_RUN_NUMBER'
})  

const newExecutionAddRunNumber = (runNumber, from, to) => ({
  type: 'NEW_EXECUTION_ADD_RUN_NUMBER',
  runNumber,
  from,
  to
}) 

export const newExecutionFetchRunNumber = (runNumber) => {
  return (dispatch) => {
    // Signal that the request is starting
    dispatch(newExecutionRequestRunNumber());
    return fetch(`runnumber/${runNumber}`)
      .then(response => response.json())
      .then(json => {
        // Convert unix seconds to milliseconds
        const from = json.from * 1000;
        const to = json.to * 1000;
        dispatch(newExecutionAddRunNumber(runNumber, from, to))
      })
  }
}

export const newExecutionRemoveRunNumber = (runNumber) => ({
  type: 'NEW_EXECUTION_REMOVE_RUN_NUMBER',
  runNumber
})

export const newExecutionChangeRunNumber = (runNumber, fromSelected, toSelected) => ({
  type: 'NEW_EXECUTION_CHANGE_RUN_NUMBER',
  runNumber,
  fromSelected,
  toSelected
})

const requestPBeastSuggestions = () => ({
  type: 'REQUEST_PBEAST_SUGGESTIONS'
});

const receivePBeastSuggestions = (json) => ({
    type: 'RECEIVE_PBEAST_SUGGESTIONS',
    pBeastSuggestions: json.data
});

export const fetchPBeastSuggestions = () => {
  return function (dispatch) {
    // Signal that the request is starting
    dispatch(requestPBeastSuggestions());
    // Make request
    return fetch('autocompletepartclass')
      .then(response => response.json())
      .then(json =>
        dispatch(receivePBeastSuggestions(json))
      )
  }
}

export const newExecutionAddPBeastData = (tag) => ({
  type: 'NEW_EXECUTION_ADD_PBEAST_DATA',
  tag
})

export const newExecutionRemovePBeastData = (id) => ({
  type: 'NEW_EXECUTION_REMOVE_PBEAST_DATA',
  id
})
