import { combineReducers } from 'redux';
import { List, OrderedMap } from 'immutable';
import _ from 'lodash';

function executions(state = List(), action) {
  switch (action.type) {
    case 'REQUEST_EXECUTIONS':
      // TODO: Can be used to set spinner
      return state;
    case 'RECEIVE_EXECUTIONS':
      return List(action.executions);
    default:
      return state;
  }
}

function executionsSearch(state = '', action) {
  switch (action.type) {
    case 'CHANGE_EXECUTIONS_SEARCH':
      return action.text;
    default:
      return state;
  }
}

function showModal(state = false, action) {
  switch (action.type) {
    case 'SHOW_NEW_EXECUTION_MODAL':
      return true;
    case 'HIDE_NEW_EXECUTION_MODAL':
      return false;
    default:
      return state;
  }
}

const replayDateToTimestamp = (dateString) => {
  const ISODateString = dateString.replace(' ', 'T');
  var jsdate = new Date(ISODateString);
  var now = new Date() ;
  jsdate.setTime(jsdate.getTime() - now.getTimezoneOffset()*60*1000);
  return jsdate;
}

function newExecutionData(state = {
  name: '',
  nameDisabled: false,
  runNumbers: OrderedMap(),
  pBeastSuggestions: [],
  pBeastData: List()
}, action) {
  switch (action.type) {
    case 'FILL_IN_NEW_EXECUTION_TEMPLATE':
      const runNumberPlain = JSON.parse(action.template.runNumbers)
      let runNumbers = OrderedMap()
      runNumberPlain.forEach((data) => {
        runNumbers = runNumbers.set(data.rn, {
          fromSelected: replayDateToTimestamp(data.since),
          toSelected: replayDateToTimestamp(data.till),
          from: replayDateToTimestamp(data.from),
          to: replayDateToTimestamp(data.to)
        })
      })
      let pBeastData = List()
      if (action.template.pBeastData) {
        action.template.pBeastData.split(',').forEach((name, index) => {
          pBeastData = pBeastData.push({id: index, name: name})
        })
      }
      
      return {...state,
              name: action.template.name,
              nameDisabled: true,
              runNumbers: runNumbers,
              pBeastData: pBeastData}

    case 'RESET_NEW_EXECUTION_MODAL':
      return {...state,
              name: '',
              nameDisabled: false,
              runNumbers: OrderedMap(),
              pBeastData: List()}

    case 'NEW_EXECUTION_NAME_CHANGED':
      return {...state,
              name: action.name};

    case 'NEW_EXECUTION_ADD_RUN_NUMBER':
        // If run number exist don't override it.
        if (state.runNumbers.has(action.runNumber)) {
          return state
        }
        return {...state,
                runNumbers: state.runNumbers.set(action.runNumber,
                                                 {from: new Date(action.from),
                                                  to: new Date(action.to),
                                                  fromSelected: new Date(action.from),
                                                  toSelected: new Date(action.to)
                                                 })}

    case 'NEW_EXECUTION_REMOVE_RUN_NUMBER':
      // Remove a run number by setting it to undefined.
      return {...state,
              runNumbers: state.runNumbers.delete(action.runNumber)}

    case 'NEW_EXECUTION_CHANGE_RUN_NUMBER':
      return {...state,
              runNumbers: state.runNumbers.set(action.runNumber,
                                               {from: state.runNumbers.get(action.runNumber).from,
                                                to: state.runNumbers.get(action.runNumber).to,
                                                fromSelected: new Date(action.fromSelected),
                                                toSelected: new Date(action.toSelected)})}

    case 'RECEIVE_PBEAST_SUGGESTIONS':
      return {...state,
              pBeastSuggestions: action.pBeastSuggestions.map(
                (value, index) => ({id: index, name: value}))}

    case 'NEW_EXECUTION_ADD_PBEAST_DATA':
      return {...state,
              pBeastData: state.pBeastData.push(action.tag)}

    case 'NEW_EXECUTION_REMOVE_PBEAST_DATA':
      return {...state,
              pBeastData: state.pBeastData.delete(action.id)}
    default:
      return state;
  }
}

const newExecution = combineReducers({
  showModal,
  data: newExecutionData
});

const replayApp = combineReducers({
  executions,
  executionsSearch,
  newExecution
});

export default replayApp;
