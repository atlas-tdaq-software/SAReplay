var path = require('path');

var BUILD_DIR = path.resolve(__dirname, '../static/js/');
var APP_DIR = path.resolve(__dirname, 'src/jsx');

var config = {
    entry: APP_DIR + '/index.jsx',
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    output: {
        path: BUILD_DIR,
        filename: 'app.js'
    },
    module: {
        loaders: [{
            test: /\.jsx?/,
            include: APP_DIR,
            loader: 'babel',
            query: {
                presets:['react', 'es2015', 'stage-2']
            }
        }]
    }
};

module.exports = config;
